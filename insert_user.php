<?php
session_start();
if(!isset($_SESSION['user_id']))
{
    session_destroy();
    header('Location: connect.php');
}
else
{
    /**
    * start program by including core.php and run include_core_files()
    */
    include "core.php";
    //including core files
    include_core_files();
    $db = new db_object();
    $current_page = "Dashboard";
    $userId = $_SESSION['user_id'];
    $userLevel = $_SESSION['level'];
    $userName = $_SESSION['name'];
    if(isset($_POST['userName']) && isset($_POST['userLvl']))
    {
        if($userLevel > 0)
        {
            if(!$db->verify_user_by_name_only($_POST['userName']))
            {
                if($db->add_new_user($_POST['userName'],$_POST['userLvl']))
                {
                    echo "User added with success.";
                }
                else
                {
                    echo "Error while adding new user. Check database connexion or contact support.";
                }
            }
            else
            {
                echo "User name already exists. Please choose another one.";
            }
        }
        else
        {
            echo "Your runlevel does not allow you to do such action.";
        }
    }
    else
    {
        session_destroy();
        header('Location: connect.php');
    }
}
?>