<?php
session_start();
if(!isset($_SESSION['user_id']))
{
    session_destroy();
    header('Location: connect.php');
}
else
{
    /**
    * start program by including core.php and run include_core_files()
    */
    include "core.php";
    //including core files
    include_core_files();
    $db = new db_object();
    $current_page = "Dashboard";
    $userId = $_SESSION['user_id'];
    $userLevel = $_SESSION['level'];
    $userName = $_SESSION['name'];
    if(isset($_POST['reportName']) && isset($_POST['frequency']) && isset($_POST['email_list']) && isset($_POST['entries']))
    {
        $report = array(
            "name" => $_POST['reportName'],
            "frequency" => $_POST['frequency'],
            "email_list" => $_POST['email_list'],
            "entries" => $_POST['entries'],
            "sorting" => $_POST['sorting'],
            "number_elem" => $_POST['number_elem']
        );
        if($userLevel > 0)
        {
            if($db->add_report($report,$userId))
            {
                echo "Report added with success.";
            }
            else
            {
                echo "Error while adding new report. Check database connexion or contact support.";
            }
        }
        else
        {
            echo "Your runlevel does not allow you to do such action.";            
        }
    }
    else
    {
        session_destroy();
        header('Location: connect.php');
    }
}

?>