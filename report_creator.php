<?php
use Dompdf\Dompdf;
require_once 'dompdf/autoload.inc.php';
include_once "core/misc_functions.php";
include "core.php";

include_core_files();
$dompdf = new Dompdf();
$db = new db_object();
if(isset($_POST['id_strsys']))
{
    $id_strsys = $_POST['id_strsys'];
    $chartPie = $_POST['chartPie'];
    $graph = $_POST['graph'];
    $graph_global = $_POST['graph-global'];
    $strsys = $db->load_strsys_byID($id_strsys);
    $types = $db->get_strsys_types();
    $entries_pools = $db->load_last_pools_entry($strsys);
    $entries_rgs = $db->load_last_rgs_entry($strsys);
    $pools = $db->get_system_pools_byID($strsys);
    $rgs = $db->get_system_rgs_byID($strsys);
    $alerts = $db->get_system_alerts_byID($strsys);
    $type_key = array_search($strsys['id_type'], array_column($types,'id_type'));
}
$used = 0;
$free = 0;
foreach($entries_pools as $pool)
{
    $used += ($pool['used_cap_gb'] * 100)/$pool['total_cap_gb'];
    $free += ($pool['free_cap_gb'] * 100)/$pool['total_cap_gb'];
}
foreach($entries_rgs as $rg)
{
    $used += (($rg['user_cap_gb'] - $rg['free_cap_gb']) * 100)/$rg['user_cap_gb'];
    $free += ($rg['free_cap_gb'] * 100)/$rg['user_cap_gb'];
}
if(isset($_POST['freq']))
    $freq = $_POST['freq'];
else
    $freq = -1;
$fraction = sizeof($entries_pools) + sizeof($entries_rgs);
$used = number_format((float)$used / $fraction, 2,'.','');
$free = number_format((float)$free / $fraction, 2,'.','');
$date = date("d/m/Y H:i:s");
$file_name = str_replace(array("/",":"," "),"",$date);
$file_path = $file_name.".html";
ob_start();
top_report($date);
//#### one page #### //
echo "<div class=\"content\">";
cover_page($strsys['name'], date("d/m/Y"),$freq);
echo "</div>";
//#### one page #### //
echo "<div class=\"content\">";
header_report($date,$strsys['name']);
strsys_info_report($strsys,$db);
echo "<div class=\"content-title\">
Pool Summary
</div>";
pools_table_gen($pools,$entries_pools,$strsys,false);
if($strsys['id_type'] == 1)
{
    echo "<div class=\"content-title\">
    RAID GROUP Summary
    </div>";
    rgs_table_gen($rgs,$entries_rgs,$strsys,false);
}
bottom_report();
echo "</div>";
//page lun
if(isset($_POST['evolution']))
    $evolution = $_POST['evolution'];
else
    $evolution = 1;
if(isset($_POST['sorting']))
    $sorting = $_POST['sorting'];
else
    $sorting = 0;
if(isset($_POST['number_elem']))
    $number_elem = $_POST['number_elem'];
else
    $number_elem = 10;
$date = date("Y-m-d");
$offset_day = $evolution * 86400;
$day_past = strtotime($date) - $offset_day;
$date_offset = date("Y-m-d", $day_past);
foreach($pools as $pool)
{
    $luns = $db->load_luns_by_pool($pool,$strsys);
    $luns_status = $db->load_luns_entries_by_date($pool,$strsys,$date);
    $luns_status_offset = $db->load_luns_entries_by_date($pool,$strsys,$date_offset);
    //#### one page ###//
    echo "<div class=\"content\">";
    header_report($date,$strsys['name']);
    echo "<div class=\"content-title\">LUNs Evolution of ".$pool['name']."</div>";
    lun_table_gen($luns, $luns_status,$luns_status_offset,$sorting,$number_elem,true,$strsys);
    bottom_report();
    echo "</div>";
}
//#### one page ### //
echo "<div class=\"content\">";
header_report($date,$strsys['name']);
echo "<div class=\"content-title\">
System Capacity
</div>";
echo "<p><img class=\"img-report chart\" src=\"$chartPie\"></p><br><br><br><br>";
if($used > 50 && $used < 70)
{
    echo "<p class=\"sys-cap-info\">System capacity usage has trespassed 50% but free space remains for more use.</p>";
}
else if($used > 70)
{
    echo "<p class=\"sys-cap-info\">System capacity usage has trespassed 70%. Consider some capacity extensions in the future to ensure your storage system performance and prevent data loss.</p>";
}
else if($used >90)
{
    echo "<p class=\"sys-cap-info\">Almost no more free space is remaining. Please consider immediate capacity extensions to avoid data loss/corruption.</p>";
}
else
{
    echo "<p class=\"sys-cap-info\" style=\"color:#ffffff;\">-</p>";
}
echo "<div class=\"content-title\">
Global storage usage
</div>";
echo "<p><img class=\"img-report graph\" src=\"$graph_global\" width=\"740\" height=\"420\"></p>";
bottom_report();
echo "</div>";
//#### one page ### //
echo "<div class=\"content\">";
header_report($date,$strsys['name']);
    echo "<div class=\"content-title\">Storage Usage Evolution</div>";
echo "<p><img class=\"img-report graph\" src=\"$graph\" width=\"740\" height=\"420\"></p><br><br><br><br><br><br><br><br>";
echo "<p class=\"sys-cap-info\">This is the system usage in GB during the latest period. Each line represents a POOL/Raid Group. Refer to the monitoring interface for more details.</p>";
bottom_report();
echo "</div>";
//####### end html document ##### //
echo "</body>";
echo "</html>";
file_put_contents($file_path,ob_get_contents());
ob_end_clean();
$html = file_get_contents($file_path);
$dompdf->loadHtml($html);
$dompdf->setPaper('a4','portrait');
$dompdf->render();
$pdf = $dompdf->output();
$pref = "report-";
if(isset($_POST['report']))
{
    $pref = $_POST['report']."-";
}
file_put_contents(__DIR__."/reports/".$pref."".$file_name."-".$id_strsys."-".$strsys['name'].".pdf",$pdf);
unset($dompdf);
$output = shell_exec("/bin/bash ".__DIR__."/rm_tmp_file.sh");
echo $pref."".$file_name."-".$id_strsys."-".$strsys['name'].".pdf";
?>