<?php
include "core.php";
//including core files
include_core_files();
$current_page = "Connect";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 5.0 Strict//EN">

<html lang="en">
    <?php main_head_gen($current_page); ?>  
    <body class="bg-light">
        <!-- ========= modal error ==================== -->
        <div class="modal fade" id="ErrorModal" tabindex="-1" role="dialog" aria-labelledby="ErrorModal" aria-hidden="false">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Server Response</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="modalContent">
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="container h-100">
            <div class="row align-items-center" style="height:24%;">
                <img src="img/Logo_client.png" class="logo-connect">
            </div>
            <div class="row align-items-center">
                <h3 class="col-sm-12 text-center">FTS Storage Monitoring</h3>
            </div>
            <div class="row align-items-center">
                <div class="col-sm-5 mx-auto">
                    <div class="card card-signin my-5">
                        <div class="card-body">
                            <h4 class="card-title text-center font-weight-light text-light">Sign In</h4>
                            <form id="connectForm">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="userName" name="userName" required placeholder="User">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="passwd" name="passwd" required placeholder="Password">
                                </div>
                                <div class="form-group text-center">
                                    <div id="Spinner" class="d-none spinner-border text-light" role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button id="buttonConnect" class="btn btn-lg btn-primary btn-block text-uppercase">CONNECT</button>
                                </div>
                            </form>

                        </div>
                    </div>

                </div>
            </div>
            <div class="row align-items-center">
                <img src="img/Logo_Focus.png" class="logo-constructor">
            </div>
            <div class="row align-items-center">
                <p class="copyright">Copyright &copy; <?php echo date("Y"); ?> Focus Technology Solutions<br>All Rights Reserved</p>
            </div>
            
        </div>
        <!-- ******************** JQUERY LIBRARY ************************** -->
        <?php
        js_libraries_gen();
        ?>
        <script>
            $(document).ready(function() {
                $("#connectForm").submit(function(e) {
                    e.preventDefault();
                    var buttonConnect = $("#buttonConnect");
                    var userName = $("#userName").val();
                    var passwd = $("#passwd").val();
                    var spinner = $("#Spinner");
                    if(userName != "" && passwd != "")
                    {
                        buttonConnect.addClass('disabled');
                        buttonConnect.attr('aria-disabled','true');
                        spinner.removeClass('d-none');
                        $.ajax({
                            url: "check_connexion.php",
                            type:'post',
                            data: {
                                'userName':userName,
                                'passwd' : passwd
                            },
                            success:function(response)
                            {
                                if(response == "0")
                                {
                                    spinner.addClass('d-none');
                                    buttonConnect.removeClass('disabled');
                                    buttonConnect.attr('aria-disabled','false');
                                    $('#modalContent').html("User name or Password are wrong. Please try again.");
                                    $("#userName").val("");
                                    $("#passwd").val("");
                                    $('#ErrorModal').modal('show');
                                    
                                }
                                else if(response == "1")
                                {
                                    buttonConnect.removeClass('disabled');
                                    buttonConnect.attr('aria-disabled','false');
                                    spinner.addClass('d-none');
                                    window.location = "index.php";
                                }
                                else
                                {
                                    buttonConnect.removeClass('disabled');
                                    buttonConnect.attr('aria-disabled','false');
                                    spinner.addClass('d-none');
                                    $('#modalContent').html(response);
                                    $("#userName").val("");
                                    $("#passwd").val("");
                                    $('#ErrorModal').modal('show');
                                }
                            }
                        });
                    }
                    else
                    {
                        $('#modalContent').html("Please fill all the fields.");
                        $('#ErrorModal').modal('show');
                    }
                });
            });
        </script>
    </body>
</html>