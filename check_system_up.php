<?php
session_start();
if(!isset($_SESSION['user_id']))
{
    session_destroy();
    header('Location: connect.php');
}
else
{
    /**
    * start program by including core.php and run include_core_files()
    */
    include "core.php";
    //including core files
    include_core_files();
    $db = new db_object();
    $current_page = "Dashboard";
    $userId = $_SESSION['user_id'];
    $userLevel = $_SESSION['level'];
    $userName = $_SESSION['name'];
    if(isset($_POST['id_strsys']))
    {
        $system = $db->load_strsys_byID($_POST['id_strsys']);
        $state = 1;
        $ping_test = ping($system['spa_ip']);
        if(!$ping_test)
        {
            $ping_test = ping($system['spb_ip']);
            if(!$ping_test)
            {
                $ping_test = ping($system['cs_ip']);
                if(!$ping_test)
                    $state = 0;
            }
        }
        echo $state;
    }
    else
    {
        session_destroy();
        header('Location: connect.php');
    }
}
    
        