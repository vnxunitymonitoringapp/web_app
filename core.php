<?php
/** 
 * function: this file is the only file to be included in application files to include core elements
 * requieres: requiere folder core
*/
ini_set("auto_detect_line_endings", true);
function include_core_files()
{
    foreach(glob("core/*.php") as $filename)
    {
        include_once $filename;
    }
}
?>
