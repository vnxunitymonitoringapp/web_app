<?php
session_start();
if(!isset($_SESSION['user_id']))
{
    session_destroy();
    header('Location: connect.php');
}
else
{
    /**
    * start program by including core.php and run include_core_files()
    */
    include "core.php";
    //including core files
    include_core_files();
    $db = new db_object();
    $current_page = "Dashboard";
    $userId = $_SESSION['user_id'];
    $userLevel = $_SESSION['level'];
    $userName = $_SESSION['name'];
    if(isset($_POST['file-to-delete']))
    {
        $root_dir = $_SERVER['DOCUMENT_ROOT'];
        $file = $root_dir."/reports/".$_POST['file-to-delete'];
        unlink($file) or die("Error while deleting the file.");
    }
}