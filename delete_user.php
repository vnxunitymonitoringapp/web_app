<?php
session_start();
if(!isset($_SESSION['user_id']))
{
    session_destroy();
    header('Location: connect.php');
}
else
{
    /**
    * start program by including core.php and run include_core_files()
    */
    include "core.php";
    //including core files
    include_core_files();
    $db = new db_object();
    $current_page = "Dashboard";
    $userId = $_SESSION['user_id'];
    $userLevel = $_SESSION['level'];
    $userName = $_SESSION['name'];
    if(isset($_POST['userID']))
    {
        if($userLevel == 2)
        {
            if($db->verify_user_by_id($_POST['userID']))
            {
                if($db->delete_user_by_id($_POST['userID']))
                    echo "1";
            }
            else
            {
                echo "Selected user does not exist in database.";
            }
        }
        else
        {
            echo "Your runlevel does not allow you to do such action.";
        }
    }
    else
    {
        session_destroy();
        header('Location: connect.php');
    }
}
?>