<?php
/**
 * start program by including core.php and run include_core_files()
 */
include "core.php";
//including core files
include_core_files();
$db = new db_object();
session_start();
if(isset($_POST['userName']) && isset($_POST['passwd']))
{
    $userName = strip_tags(trim($_POST['userName']));
    $passwd = strip_tags(trim($_POST['passwd']));
    $connected_user = $db->verify_user_by_name($userName,$passwd);
    if(!empty($connected_user))
    {
        $_SESSION['user_id'] = $connected_user[0];
        $_SESSION['name'] = $connected_user[1];
        $_SESSION['passwd'] = $connected_user[2];
        $_SESSION['level'] = $connected_user[3];
        echo "1";
    }
    else
    {
        session_destroy();
        echo "0";
    }
}
else
{
    session_destroy();
    header('Location: connect.php');
}


?>