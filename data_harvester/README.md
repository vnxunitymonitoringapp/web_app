## data_harvester component description:
This python module is responsible for collecting data from storage arrays registred in the application database. It requires python3 installed on the applience for it to run correctly. The following REAMDE file will give a general overview on the mechanism used by the module's different files.

## Scripts:

**data_harvester.py**
This scripts is the core of the whole application. It has the responsibility to retrieve information about registred storage arrays from data base, and then use the ***storops*** python module to communicate with the retrieved storages and update information about storage status, pool status and lun status. If at the beginning the scripts find a "new_strsys" file with the following format:

    mgmtip:user:passwd:type:sitelocation
It will then proceed in adding each line to the monitoring appliance. This utility happens to be handy when the monitoring has been deployed and you want to add multiple storage system at once without accessing the web interface.
***db_schema.py***
This file contains a class that describes a storage system within the monitoring appliances. It serves as a model to the data_harvester component.

    class strsys:
    attributes: 
	    id_strsys: int
		id_type: int
		name: string
		site_location: string
		serial_number: string
		model: string
		spa_ip: ip address (decimal representation)
		spb_ip: ip address (decimal representation)
		cs_ip: ip address (decimal representation)
		firmware_ver: string
		user: string
		password: string

***report_scheduler.py***
This script is invoked through the crontab functionality of the monitoring appliance with a frequency argument:

 - day: The script will retrieve from the database the daily reports and start to process them.
 - week: The script will retrieve from the database the weekly reports and start to process them.
 - month: The script will retrieve from the database the monthly reports and start to prcess them
This script need to be configured with an smtp server (account and password) in order for it to be able to send reports via emails: change the **smpt** and **smtp_port** for the server, **from_** and **mdp** for the password.

***lun_harvester.py***
This scripts functions similarly as ***data_harvester.py*** but without adding new systems to the monitoring appliance. It retrieves information about the systems registred and then updates lun relatives information: cleaning luns that have been deleted from the system and updating others.
***functions.py***
This file acts as a framework to be shared between the other core scripts to centralize the data access layer with the monitoring database. This is like an model-controller layer in a MVC architecture. Check the functions, the sql queries are straightforward.

Find here a summer up schema: https://drive.google.com/file/d/1Bxcmb31YOOs5miJCJ2u-GxXvTTTykqmi/view?usp=sharing
## Web application access
Root account: Login: root/ mdp: ftsFTS123
See documentation for use.