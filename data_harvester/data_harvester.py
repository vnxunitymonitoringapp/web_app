#!/usr/bin/python
import sys
import os
import datetime
import mysql.connector
import functions
import logging
from storops import *
from storops import VNXSystem
from storops import UnitySystem
from db_schema import strsys

#script description
#script name: data_harvester.py
#author: Belhassen Dahmen
#library: native python libraries, emc storops library for an overlay to communicate with vnx storage system
#usage: cron use
#database config
logging.basicConfig(level=logging.INFO)
separator = ":"
hostname = "localhost"
username = "root"
password = ""
db = "FTS_mon_app"
#initial config
new_strsys = "new_strsys"
new_strsys_en = os.path.isfile(new_strsys)
FTS_mon_app = mysql.connector.connect(
	host=hostname,
	user=username,
	passwd=password,
	database=db
)
strsys_list = []
#adding new sys to database
if new_strsys_en:
    prop_lists = []
    with open(new_strsys, "r") as f:
        lines = f.readlines()
        for line in lines:
            line = line.rstrip().lstrip()
            nw_str = line.split(separator)
            #vnx
            if nw_str[3] == "vnx" or nw_str[3] == "VNX":
                vnx = VNXSystem(nw_str[0],nw_str[1],nw_str[2])
                if vnx.existed:
                    cs_fd = False
                    j = 0
                    #while not cs_fd and j < len(vnx.domain):
                    #    if str(vnx.domain[j].name).find("SPA") == -1 and str(vnx.domain[j].name).find("SPB") == -1:
                    #        cs_fd = True
                    #        cs_ip = str(vnx.domain[j].ip_address)
                    #    else:
                    #        j += 1
		    if cs_fd == False:
			cs_ip = str(vnx.spa_ip)
                    #get system health
                    health = functions.get_health(nw_str[0],nw_str[1],nw_str[2])
                    id_type = functions.get_type_id("vnx",FTS_mon_app)
                    prop_list = (id_type, str(vnx.name), str(nw_str[4]), str(vnx.serial), str(vnx.model), str(vnx.spa_ip), str(vnx.spb_ip), cs_ip, str(vnx.revision),health,nw_str[1],nw_str[2])
                    prop_lists.append(prop_list)
                else:
                    print("Cannot reach storage system. Please check network and try add it later.")
            #unity
            elif nw_str[3] == "unity" or nw_str[3] == "Unity" or nw_str[3] == "UNITY":
                unity = UnitySystem(nw_str[0],nw_str[1],nw_str[2])
                if unity.existed:
                    health = functions.get_health_unity(unity)
                    id_type = functions.get_type_id("unity",FTS_mon_app)
                    mgmt_int = unity.get_mgmt_interface()
                    if len(mgmt_int) == 1:
                        mgmt_int.append(mgmt_int[0])
                        mgmt_int.append(mgmt_int[0])
                    elif len(mgmt_int) == 2:
                        mgmt_int.append(mgmt_int[0])
                    prop_list = (id_type, str(unity.name), str(nw_str[4]), str(unity.serial_number), str(unity.model), str(mgmt_int.ip_address[0]), str(mgmt_int.ip_address[1]), str(mgmt_int.ip_address[2]), str(unity.info.software_version),health,nw_str[1],nw_str[2])
                    prop_lists.append(prop_list)
            else:
                print("Cannot reach storage system. Please check network and try add it later.")
        f.close()
    count = functions.insert_strsys(prop_lists,FTS_mon_app)
    print("Added %i new system to database." % count)
    os.remove(new_strsys)
else:
    print("No new system to add.")
#retrieving from database
res_get_strsys = functions.get_strsys(FTS_mon_app)
if len(res_get_strsys) > 0:
    for strsys_elem in res_get_strsys:
        strsys_obj = strsys(strsys_elem[0],strsys_elem[1],strsys_elem[2],strsys_elem[3],strsys_elem[4],strsys_elem[5],strsys_elem[6],strsys_elem[7],strsys_elem[8],strsys_elem[9],strsys_elem[10],strsys_elem[11],strsys_elem[12])
        nif_test = functions.test_strsys_up(strsys_obj)
        if nif_test != 0:
            strsys_list.append(strsys(strsys_elem[0],strsys_elem[1],strsys_elem[2],strsys_elem[3],strsys_elem[4],strsys_elem[5],strsys_elem[6],strsys_elem[7],strsys_elem[8],strsys_elem[9],strsys_elem[10],strsys_elem[11],strsys_elem[12]))
            functions.update_reachable(FTS_mon_app,strsys_obj.id_strsys,1)
        else:
            functions.update_reachable(FTS_mon_app,strsys_obj.id_strsys,0)
            print("Cannot reach storage system ID:%s NAME %s. Please check network and try it later. " % (str(strsys_obj.id_strsys),strsys_obj.name))
    print("%i strsys were retrieved from database." % len(res_get_strsys))
else:
    print("no entries in database yet.")
#updating pools and RG
if len(strsys_list) > 0:
    for strsys_elem in strsys_list:
        print("Updating pools and RAID groups for %s system..." % strsys_elem.name)
        pools_db = functions.get_pools_by_strsys_id(str(strsys_elem.id_strsys),FTS_mon_app)
        rgs_db = functions.get_rgs_by_strsys_id(str(strsys_elem.id_strsys),FTS_mon_app)
        if strsys_elem.id_type == 1:
            health = functions.get_health(strsys_elem.spa_ip, strsys_elem.user,strsys_elem.passwd)
        elif strsys_elem.id_type == 2:
            un = UnitySystem(strsys_elem.spa_ip, strsys_elem.user, strsys_elem.passwd)
            if un.existed:
                health = functions.get_health_unity(un)
                functions.update_strsys_health(FTS_mon_app,strsys_elem.id_strsys,health)
        #vnx
        if strsys_elem.id_type == 1:
            vnx = VNXSystem( strsys_elem.spa_ip, strsys_elem.user , strsys_elem.passwd )
            if not vnx.existed:
                vnx = VNXSystem( strsys_elem.spb_ip, strsys_elem.user , strsys_elem.passwd )
            #pools
            cs_fd = False
            j = 0
            #while not cs_fd and j < len(vnx.domain):
            #    if str(vnx.domain[j].name).find("SPA") == -1 and str(vnx.domain[j].name).find("SPB") == -1 :
            #        cs_fd = True
            #        cs_ip = str(vnx.domain[j].ip_address)
            #    else:
            #        j += 1
	    #print(cs_fd)
	    if cs_fd == False:
	        cs_ip = vnx.spa_ip
	    print("HEALTH: %s" % (str(health)))
            updated_stat = strsys(strsys_elem.id_strsys,strsys_elem.id_type,vnx.name,strsys_elem.site_location,vnx.serial,vnx.model,vnx.spa_ip,vnx.spb_ip,cs_ip,str(vnx.revision),health,strsys_elem.user,strsys_elem.passwd)
            functions.update_strsys(FTS_mon_app,updated_stat,health)
            if len(pools_db) == 0:
                pools = vnx.get_pool()
                pools_vals = []
                for pool in pools:
                    val = (str(pool.pool_id) , str(pool.name),str(pool.percent_full_threshold), str(strsys_elem.id_strsys))
                    pools_vals.append(val)
                count = functions.insert_pools(pools_vals, FTS_mon_app)
                print("Add %i pools for system %s." % (count, strsys_elem.name))
            else:
                pools = vnx.get_pool()
                pools_vals = []
                i = 0
                for pool in pools:
                    pool_select = functions.get_pool_by_id(pool.pool_id,strsys_elem.id_strsys, FTS_mon_app)
                    if len(pool_select) == 0 :
                        val = (str(pool.pool_id) , str(pool.name),str(pool.percent_full_threshold), str(strsys_elem.id_strsys))
                        pools_vals.append(val)
                    else:
                        i += 1
                if len(pools_vals) > 0:
                    count = functions.insert_pools(pools_vals, FTS_mon_app)
                    print("Added %i pools to %i for system %s." % (count, i,strsys_elem.name))
            #rgs
            if len(rgs_db) == 0:
                rgs = vnx.get_rg()
                rgs_vals = []
                for rg in rgs:
                    val = (str(rg.raid_group_id) , str(rg.raid_group_type.value)[1:].replace("_",""),str(strsys_elem.id_strsys))
                    rgs_vals.append(val)
                count = functions.insert_rgs(rgs_vals, FTS_mon_app)
                print("Add %i RAID groups for system %s." % (count, strsys_elem.name))
            else:
                rgs = vnx.get_rg()
                rgs_vals = []
                i = 0
                for rg in rgs:
                    rg_select = functions.get_rg_by_id(rg.raid_group_id,strsys_elem.id_strsys, FTS_mon_app)
                    if len(rg_select) == 0 :
                        val = (str(rg.raid_group_id) , str(rg.raid_group_type.value)[1:].replace("_",""),str(strsys_elem.id_strsys))
                        rgs_vals.append(val)
                    else:
                        i += 1
                if len(rgs_vals) > 0:
                    count = functions.insert_rgs(rgs_vals, FTS_mon_app)
                    print("Added %i RAID groups to %i for system %s." % (count, i, strsys_elem.name))
        #unity
        elif strsys_elem.id_type == 2:
            nif_test = functions.test_strsys_up(strsys_elem)
            if nif_test == 1:
                unity = UnitySystem( strsys_elem.spa_ip, strsys_elem.user , strsys_elem.passwd )
            elif nif_test == 2:
                unity = UnitySystem( strsys_elem.spb_ip, strsys_elem.user , strsys_elem.passwd )
            elif nif_test == 3:
                unity = UnitySystem( strsys_elem.cs_ip, strsys_elem.user , strsys_elem.passwd )
            mgmt_int = unity.get_mgmt_interface()
            if len(mgmt_int) == 1:
                mgmt_int.append(mgmt_int[0])
                mgmt_int.append(mgmt_int[0])
            elif len(mgmt_int) == 2:
                mgmt_int.append(mgmt_int[0])
            updated_stat = strsys(strsys_elem.id_strsys,strsys_elem.id_type,str(unity.name),strsys_elem.site_location,str(unity.serial_number), str(unity.model),str(mgmt_int.ip_address[0]), str(mgmt_int.ip_address[1]), str(mgmt_int.ip_address[2]),str(unity.info.software_version),health,strsys_elem.user,strsys_elem.passwd)
            functions.update_strsys(FTS_mon_app,updated_stat,health)
            #pools
            if len(pools_db) == 0:
                pools = unity.get_pool()
                pools_vals = []
                for pool in pools:
                    pp_id = str(pool.id)[str(pool.id).find("pool_")+len("pool_"):len(str(pool.id))]
                    val = (pp_id, str(pool.name),str(pool.alert_threshold), str(strsys_elem.id_strsys))
                    pools_vals.append(val)
                count = functions.insert_pools(pools_vals, FTS_mon_app)
                print("Add %i pools for system %s." % (count, strsys_elem.name))
            else:
                pools = unity.get_pool()
                pools_vals = []
                i = 0
                for pool in pools:
                    pp_id = str(pool.id)[str(pool.id).find("pool_")+len("pool_"):len(str(pool.id))]
                    pool_select = functions.get_pool_by_id(pp_id,strsys_elem.id_strsys, FTS_mon_app)
                    if len(pool_select) == 0 :
                        val = (pp_id, str(pool.name),str(pool.alert_threshold), str(strsys_elem.id_strsys))
                        pools_vals.append(val)
                    else:
                        i += 1
                if len(pools_vals) > 0:
                    count = functions.insert_pools(pools_vals, FTS_mon_app)
                    print("Added %i pools to %i for system %s." % (count, i,strsys_elem.name))
        #updating data
        pools_db = functions.get_pools_by_strsys_id(str(strsys_elem.id_strsys),FTS_mon_app)
        rgs_db = functions.get_rgs_by_strsys_id(str(strsys_elem.id_strsys),FTS_mon_app)
        #vnx
        if strsys_elem.id_type == 1:
            vnx = VNXSystem( strsys_elem.spa_ip, strsys_elem.user , strsys_elem.passwd )
            if not vnx.existed:
                vnx = vnx = VNXSystem( strsys_elem.spb_ip, strsys_elem.user , strsys_elem.passwd )
            #pools
            status = []
            date =str(datetime.datetime.now())
            for pool in pools_db:
                pp = vnx.get_pool(pool_id = pool[0])
                status.append(functions.construct_pool_status_tuple(pp,date,strsys_elem.id_strsys))
            functions.insert_pool_status(status, FTS_mon_app)
            #rgs
            status = []
            for rg in rgs_db:
                rr = vnx.get_rg(rg_id = rg[0])
                status.append(functions.construct_rg_status_tuple(rr,date,strsys_elem.id_strsys))
            functions.insert_rg_status(status, FTS_mon_app)
        elif strsys_elem.id_type == 2:
            nif_test = functions.test_strsys_up(strsys_elem)
            if nif_test == 1:
                unity = UnitySystem( strsys_elem.spa_ip, strsys_elem.user , strsys_elem.passwd )
            elif nif_test == 2:
                unity = UnitySystem( strsys_elem.spb_ip, strsys_elem.user , strsys_elem.passwd )
            elif nif_test == 3:
                unity = UnitySystem( strsys_elem.cs_ip, strsys_elem.user , strsys_elem.passwd )
            #pools
            status = []
            date =str(datetime.datetime.now())
            for pool in pools_db:
                pp = unity.get_pool(id = "pool_"+str(pool[0]))
                status.append(functions.construct_pool_status_tuple_unity(pp[0],date,strsys_elem.id_strsys,pool[0]))
            functions.insert_pool_status(status, FTS_mon_app)
        print("Update pools and RAID groups for %s system done." % strsys_elem.name)
else:
    print("No storage system is present or reachable. Please check network and try later.")
