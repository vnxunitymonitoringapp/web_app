#!/usr/bin/python
import os
import mysql.connector
import datetime
from storops import *
from storops import VNXSystem
from storops import UnitySystem
from storops.lib.converter import block_to_gb 
from db_schema import strsys

def get_type_id(type_name, db):
    sql = "SELECT `id_type` FROM sys_types WHERE `name` = %s"
    val = (type_name,)
    cursor = db.cursor()
    cursor.execute(sql, val)
    result = cursor.fetchall()
    if cursor.rowcount == 0:
        print("No type found.")
        return -1
    else:
        return result[0][0]

def get_strsys(db):
    qry_get_strsys = "SELECT id_strsys, id_type, `name`, site_location, serial_number, model, INET_NTOA(`spa_ip`), INET_NTOA(`spb_ip`), INET_NTOA(`cs_ip`), firmware_ver, system_health, user, passwd, reachable FROM strsys" #sql query
    cur_get_strsys = db.cursor() #cursor to execute query
    cur_get_strsys.execute(qry_get_strsys) 
    res_get_strsys = cur_get_strsys.fetchall() #results in array
    cur_get_strsys.close()
    return res_get_strsys

def get_last_strsys(db):
    qry_get_last_strsys = "SELECT id_strsys, id_type, `name`, site_location, serial_number, model, INET_NTOA(`spa_ip`), INET_NTOA(`spb_ip`), INET_NTOA(`cs_ip`), firmware_ver, system_health, user, passwd, reachable FROM strsys ORDER BY id_strsys DESC LIMIT 1"
    cur_get_last_strsys = db.cursor()
    cur_get_last_strsys.execute(qry_get_last_strsys)
    res_get_last_strsys = cur_get_last_strsys.fetchall()
    cur_get_last_strsys.close()
    return res_get_last_strsys

def update_strsys_health(db, id_strsys, health):
    qry_update_strsys = "UPDATE strsys SET system_health=%s WHERE id_strsys = %s"
    val = (str(health),str(id_strsys))
    cur_update_strsys = db.cursor()
    cur_update_strsys.execute(qry_update_strsys, val)
    cur_update_strsys.close()

def update_strsys(db, strsys, health):
    qry_update_strsys = "UPDATE strsys SET `name` = %s, serial_number = %s, model = %s, spa_ip = INET_ATON(%s), spb_ip = INET_ATON(%s), cs_ip = INET_ATON(%s), firmware_ver = %s, system_health = %s WHERE id_strsys = %s"
    cur_update_strsys = db.cursor()
    val = (str(strsys.name), str(strsys.serial_number),str(strsys.model),str(strsys.spa_ip),str(strsys.spb_ip),str(strsys.cs_ip),str(strsys.firmware_ver),str(health),str(strsys.id_strsys))
    cur_update_strsys.execute(qry_update_strsys,val)
    cur_update_strsys.close()

def get_strsys_by_id(id_strsys, db):
    qry_get_strsys = "SELECT id_strsys, id_type, `name`, site_location, serial_number, model, INET_NTOA(`spa_ip`), INET_NTOA(`spb_ip`), INET_NTOA(`cs_ip`), firmware_ver, system_health, user, passwd, reachable FROM strsys WHERE id_strsys = %s" #sql query
    cur_get_strsys = db.cursor() #cursor to execute query
    val = (str(id_strsys),)
    cur_get_strsys.execute(qry_get_strsys, val) 
    res_get_strsys = cur_get_strsys.fetchall() #results in array
    cur_get_strsys.close()
    return res_get_strsys[0]

def insert_strsys(strsys_vals, db):
    qry_add_strsys = "INSERT INTO strsys (id_type, `name`, site_location, serial_number, model, spa_ip, spb_ip, cs_ip, firmware_ver, system_health, user, passwd) VALUES (%s, %s, %s, %s, %s, INET_ATON(%s),INET_ATON(%s), INET_ATON(%s),%s,%s,%s,%s)"
    cur_add_strsys = db.cursor()
    cur_add_strsys.executemany(qry_add_strsys,strsys_vals)
    db.commit()
    i = cur_add_strsys.rowcount
    cur_add_strsys.close()
    return i

def insert_pool_status(pool_stat_vals, db):
    qry_add_pool_status = "INSERT INTO pool_status(total_cap_gb,used_cap_gb,free_cap_gb,per_subscription,id_pool,id_strsys,`date`) VALUES(%s,%s,%s,%s,%s,%s,%s)"
    cur_add_pool_status = db.cursor()
    cur_add_pool_status.executemany(qry_add_pool_status,pool_stat_vals)
    db.commit()
    i = cur_add_pool_status.rowcount
    cur_add_pool_status.close()
    return i

def construct_pool_status_tuple(pp,date,id_strsys):
    total_cap_gb = pp.user_capacity_gbs
    used_cap_gb = pp.user_capacity_gbs - pp.available_capacity_gbs
    free_cap_gb = pp.available_capacity_gbs
    if pp.user_capacity_gbs > 0:
        per_subscription = round((pp.total_subscribed_capacity_gbs / pp.user_capacity_gbs) * 100,3)
    else:
        per_subscription = 0
    return (total_cap_gb,used_cap_gb,free_cap_gb,per_subscription,pp.pool_id,id_strsys,date)

def construct_pool_status_tuple_unity(pp,date,id_strsys,id_p):
    octet_to_gb = 1024 * 1024 * 1024
    total_cap_gb = pp.size_total / octet_to_gb
    used_cap_gb = pp.size_used / octet_to_gb
    free_cap_gb = pp.size_free / octet_to_gb
    subscribed_cap_gb = pp.size_subscribed / octet_to_gb
    if pp.size_subscribed > 0:
        per_subscription = round((float(subscribed_cap_gb) / total_cap_gb) * 100,3)
    else:
        per_subscription = 0
    return (total_cap_gb,used_cap_gb,free_cap_gb,per_subscription,id_p,id_strsys,date)

def construct_rg_status_tuple(rr,date,id_strsys):
    user_cap_gb = block_to_gb(rr.raw_capacity_blocks)
    free_cap_gb = rr.available_capacity_gbs
    return (user_cap_gb,free_cap_gb,rr.raid_group_id,id_strsys,date)

def insert_rg_status(rg_stat_vals, db):
    qry_add_rg_status = "INSERT INTO rg_status(user_cap_gb,free_cap_gb,id_rg,id_strsys,`date`) VALUES(%s,%s,%s,%s,%s)"
    cur_add_rg_status = db.cursor()
    cur_add_rg_status.executemany(qry_add_rg_status,rg_stat_vals)
    db.commit()
    i = cur_add_rg_status.rowcount
    cur_add_rg_status.close()
    return i

#def calc_system_health(strsystem):
#    system_health = 0
#    if strsystem.id_type == 1:
#        vnx = VNXSystem( strsystem.spa_ip, strsystem.user , strsystem.passwd )
#        if not vnx.existed:
#            vnx = vnx = VNXSystem( strsystem.spb_ip, strsystem.user , strsystem.passwd )

def get_pools_by_strsys_id(id_strsys, db):
    qry_get_pools = "SELECT * FROM pools WHERE id_strsys = %s"
    cur_get_pools = db.cursor()
    val = (str(id_strsys),)
    cur_get_pools.execute(qry_get_pools, val)
    result = cur_get_pools.fetchall()
    cur_get_pools.close()
    return result

def get_rgs_by_strsys_id(id_strsys, db):
    qry_get_rgs = "SELECT * FROM rgs WHERE id_strsys = %s"
    cur_get_rgs = db.cursor()
    val = (str(id_strsys),)
    cur_get_rgs.execute(qry_get_rgs, val)
    result = cur_get_rgs.fetchall()
    cur_get_rgs.close()
    return result

def get_pool_by_id(id_pool, id_strsys, db):
    qry_get_pool_id = "SELECT * FROM pools WHERE id_pool = %s AND id_strsys = %s"
    cur_get_pool_id = db.cursor()
    cur_get_pool_id.execute(qry_get_pool_id,(str(id_pool),str(id_strsys)))
    result = cur_get_pool_id.fetchall()
    cur_get_pool_id.close()
    return result

def get_rg_by_id(id_rg, id_strsys, db):
    qry_get_rg_id = "SELECT * FROM rgs WHERE id_rg = %s AND id_strsys = %s"
    cur_get_rg_id = db.cursor()
    cur_get_rg_id.execute(qry_get_rg_id,(str(id_rg),str(id_strsys)))
    result = cur_get_rg_id.fetchall()
    cur_get_rg_id.close()
    return result

def load_last_pools_entry(db, id_strsys):
    qry_load_last_pools_entry = "SELECT * FROM (SELECT * FROM pool_status WHERE id_pool in (SELECT id_pool FROM pools WHERE id_strsys = %s) AND id_strsys = %s ORDER BY id_pool, date DESC) as T1 GROUP BY id_pool"
    cur_load_last_pools_entry = db.cursor()
    val = (str(id_strsys),str(id_strsys))
    cur_load_last_pools_entry.execute(qry_load_last_pools_entry,val)
    result = cur_load_last_pools_entry.fetchall()
    cur_load_last_pools_entry.close()
    return result

def load_last_rgs_entry(db, id_strsys):
    qry_load_last_rgs_entry = "SELECT * FROM (SELECT * FROM rg_status WHERE id_rg in (SELECT id_rg FROM rgs WHERE id_strsys = %s) AND id_strsys = %s ORDER BY id_rg, date DESC) as T1 GROUP BY id_rg"
    cur_load_last_rgs_entry = db.cursor()
    val = (str(id_strsys),str(id_strsys))
    cur_load_last_rgs_entry.execute(qry_load_last_rgs_entry,val)
    result = cur_load_last_rgs_entry.fetchall()
    cur_load_last_rgs_entry.close()
    return result

def load_pool_entries(db,id_pool,id_strsys):
    qry_load_pool_entries = "SELECT id_status, AVG(total_cap_gb),AVG(used_cap_gb),AVG(free_cap_gb),AVG(per_subscription),id_pool, DATE(`date`) FROM pool_status WHERE id_pool = %s AND id_strsys = %s GROUP BY DATE(`date`) ASC"
    cur_load_pool_entries = db.cursor()
    val = (str(id_pool),str(id_strsys))
    cur_load_pool_entries.execute(qry_load_pool_entries,val)
    result = cur_load_pool_entries.fetchall()
    cur_load_pool_entries.close()
    return result

def load_rg_entries(db,id_rg,id_strsys):
    qry_load_rg_entries = "SELECT id_status, AVG(user_cap_gb), AVG(free_cap_gb),id_rg,DATE(`date`) FROM rg_status WHERE id_rg = %s AND id_strsys = %s GROUP BY DATE(`date`) ASC"
    cur_load_rg_entries = db.cursor()
    val = (str(id_rg),str(id_strsys))
    cur_load_rg_entries.execute(qry_load_rg_entries,val)
    result = cur_load_rg_entries.fetchall()
    cur_load_rg_entries.close()
    return result

def insert_pools(pools_vals, db):
    qry_insert_pools = "INSERT INTO pools (id_pool,`name`,threshold, id_strsys) VALUES (%s, %s, %s,%s) "
    cur_insert_pools = db.cursor()
    cur_insert_pools.executemany(qry_insert_pools,pools_vals)
    db.commit()
    i = cur_insert_pools.rowcount
    cur_insert_pools.close()
    return i

def insert_rgs(rgs_vals, db):
    qry_insert_rgs = "INSERT INTO rgs (id_rg,`type`, id_strsys) VALUES (%s, %s, %s) "
    cur_insert_rgs = db.cursor()
    cur_insert_rgs.executemany(qry_insert_rgs,rgs_vals)
    db.commit()
    i = cur_insert_rgs.rowcount
    cur_insert_rgs.close()
    return i

def get_health(ip, user, passwd):
    cmd = "/var/www/html/fts_mon_app_db/data_harvester/check_fault.sh %s %s %s" % (str(ip), str(user), str(passwd))
    nbr_faults = os.popen(cmd).read().rstrip()
    if nbr_faults == "0":
        health = 0
    elif nbr_faults == "1":
        health = 1
    else :
        health = 2
    return health

def get_health_unity(unity):
    healthText = unity.health.value.name
    if healthText == 'OK':
        return 0
    elif healthText == 'DEGRADED':
        return 1
    else:
        return 2

def get_reports_by_Freq(db,freq):
    qry_get_rep = "SELECT * FROM reports WHERE frequency = %s "
    val = (str(freq),)
    cur_get_rep = db.cursor()
    cur_get_rep.execute(qry_get_rep,val)
    results = cur_get_rep.fetchall()
    cur_get_rep.close()
    return results

def get_report_entries(db,id):
    qry_get_rep_entries = "SELECT * FROM report_entries WHERE report_id = %s"
    val = (id,)
    cur_get_rep_entries = db.cursor()
    cur_get_rep_entries.execute(qry_get_rep_entries,val)
    results = cur_get_rep_entries.fetchall()
    cur_get_rep_entries.close()
    return results

def get_dates(db, strsys_type, strsys_id):
    if strsys_type == 1:
        qry_get_dates = "SELECT DISTINCT DATE(pool_status.date) AS day FROM pool_status INNER JOIN rg_status ON pool_status.date = rg_status.date WHERE pool_status.id_strsys = %s ORDER BY pool_status.date ASC"
    elif strsys_type == 2:
        qry_get_dates = "SELECT DISTINCT DATE(pool_status.date) AS day FROM pool_status WHERE pool_status.id_strsys = %s ORDER BY pool_status.date ASC"
    cur_get_dates = db.cursor()
    val = (str(strsys_id),)
    cur_get_dates.execute(qry_get_dates, val)
    result = cur_get_dates.fetchall()
    cur_get_dates.close()
    return result

def test_strsys_up(strsys):
    print("Connection check...")
    net_if = strsys.spa_ip
    response = os.system("ping -c 1 -W 1 " + net_if+ " > /dev/null ")
    if response == 0:
        return 1
    net_if = strsys.spb_ip
    response = os.system("ping -c 1 -W 1 " + net_if+ " > /dev/null ")
    if response == 0:
        return 2
    net_if = strsys.cs_ip
    response = os.system("ping -c 1 -W 1 " + net_if+ " > /dev/null ")
    if response == 0:
        return 3
    return 0

def update_reachable(db, strsys, state):
    qry_update_reachable = "UPDATE strsys SET reachable = %s WHERE id_strsys = %s"
    cur_update_reachable = db.cursor()
    val = (str(state),str(strsys))
    cur_update_reachable.execute(qry_update_reachable,val)
    cur_update_reachable.close()

def insert_lun(db,lun_tuple):
    qry_insert_lun = "INSERT INTO luns(id_lun, id_pool, id_strsys, `name`,is_thin) VALUES (%s,%s,%s,%s,%s)"
    cur_insert_lun = db.cursor()
    cur_insert_lun.execute(qry_insert_lun,lun_tuple)
    db.commit()
    cur_insert_lun.close()

def update_lun_stat(db,lun_stat_tuple):
    qry_update_lun_stat = "INSERT INTO lun_status(id_lun,id_pool,id_strsys,total_cap_gb,user_cap_gb,consumed_cap_gb,`date`) VALUES (%s,%s,%s,%s,%s,%s,%s)"
    cur_update_lun_stat = db.cursor()
    cur_update_lun_stat.execute(qry_update_lun_stat,lun_stat_tuple)
    db.commit()
    cur_update_lun_stat.close()
    
def get_luns_by_strsys(db,strsys):
    qry_get_lun_by_strsys = "SELECT * FROM luns WHERE id_strsys = %s"
    cur_get_lun_by_strsys = db.cursor()
    val = (str(strsys.id_strsys),)
    cur_get_lun_by_strsys.execute(qry_get_lun_by_strsys,val)
    luns = cur_get_lun_by_strsys.fetchall()
    cur_get_lun_by_strsys.close()
    return luns
def get_luns_by_pool(db,strsys,pool):
    qry_get_lun_by_pool = "SELECT * FROM luns WHERE id_pool = %s AND id_strsys = %s"
    cur_get_lun_by_pool = db.cursor()
    val = (str(pool[0]),str(strsys.id_strsys))
    cur_get_lun_by_pool.execute(qry_get_lun_by_pool,val)
    luns = cur_get_lun_by_pool.fetchall()
    cur_get_lun_by_pool.close()
    return luns

def get_lun_by_id(db,lun,strsys,pool):
    qry_get_lun_by_id = "SELECT * FROM luns WHERE id_lun = %s AND id_pool = %s AND id_strsys = %s"
    cur_get_lun_by_id = db.cursor()
    val = (str(lun),str(pool),str(strsys.id_strsys))
    cur_get_lun_by_id.execute(qry_get_lun_by_id,val)
    lun = cur_get_lun_by_id.fetchall()
    cur_get_lun_by_id.close()
    return lun

def get_lun_list_unprivate(lunList):
    lunList_np = []
    for lun in lunList:
        if not(lun.is_private):
            lunList_np.append(lun)
    return lunList_np

def get_lun_pool(lun,pools):
    found = False
    i = 0
    pool_id = -1
    while not(found) and i < len(pools):
        if lun.pool_name == pools[i][1]:
            pool_id = pools[i][0]
            found = True
        else:
            i += 1
    return pool_id
def get_lun_pool_unity(pool_name,pools):
    found = False
    i = 0
    pool_id = -1
    while not(found) and i < len(pools):
        if pool_name == pools[i][1]:
            pool_id = pools[i][0]
            found = True
        else:
            i += 1
    return pool_id
def delete_lun(lun_tuple, db):
    cur_delete_lun = db.cursor()
    qry_delete_lun = "DELETE FROM luns WHERE id_lun = %s AND id_pool = %s AND id_strsys = %s"
    val = (lun_tuple[0],lun_tuple[1],lun_tuple[2])
    cur_delete_lun.execute(qry_delete_lun,val)
    db.commit()
    cur_delete_lun.close()
