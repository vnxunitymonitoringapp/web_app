const ChartjsNode = require('chartjs-node');
var chartNode = new ChartjsNode(700, 600);
var graphData1 = {
     labels: [
"2019-05-03", "2019-05-04", "2019-05-05", "2019-05-06"], datasets : [
{
label: "CDP_AIX",
borderColor: "#F265A9",
data : [ 
10136.287, 10137.01, 10137.218, 10137.654], fill:false 
}, 
{
label: "HMLG_VMWARE",
borderColor: "#127762",
data : [ 
19048.174, 19051.262, 19054.456, 19056.253], fill:false 
}, 
{
label: "HMLG_AIX",
borderColor: "#1A102A",
data : [ 
16777.605, 16777.705, 16777.788, 16777.955], fill:false 
}, 
{
label: "RPA_JOURNAL",
borderColor: "#0075B8",
data : [ 
2743.348, 2743.348, 2743.348, 2743.348], fill:false 
}, 
{label: "RAID GROUP 10",
borderColor: "#245F6F",
backgroundColor: "#245F6F",
pointBackgroundColor: "#245F6F",
data : [ 
0.002,0.002,0.002,0.002], fill:false }, 
{label: "RAID GROUP 1",
borderColor: "#E75624",
backgroundColor: "#E75624",
pointBackgroundColor: "#E75624",
data : [ 
0.003,0.003,0.003,0.003], fill:false }]};
var graphOP = {
type: 'line',
data: graphData1,
options: {    scales: {    yAxes: [{        ticks: {            beginAtZero: true        }    }]}}};
return chartNode.drawChart(graphOP)
.then(() => {
return chartNode.getImageBuffer('image/png');
})
.then(buffer => {
Array.isArray(buffer);
return chartNode.getImageStream('image/png');
})
.then(streamResult => {
return chartNode.writeImageToFile('image/png', '/var/www/html/fts_mon_app_db/data_harvester/graph.png');
})
.then(() => {
});
