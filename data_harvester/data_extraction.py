#!/usr/bin/python
import sys
import os
import datetime
import mysql.connector
import functions
import logging

separator = ","
hostname = "localhost"
username = "root"
password = ""
db = "FTS_mon_app"
#initial config
new_strsys = "data_extract"
new_strsys_en = os.path.isfile(new_strsys)
FTS_mon_app = mysql.connector.connect(
	host=hostname,
	user=username,
	passwd=password,
	database=db
)
status = []
with open(new_strsys, "r") as f:
    lines = f.readlines()
    for line in lines:
        line = line.rstrip().lstrip()
        nw_str = line.split(separator)
        status.append((nw_str[0],nw_str[1],nw_str[2],nw_str[3],nw_str[4],nw_str[5],nw_str[6]))
    functions.insert_pool_status(status, FTS_mon_app)