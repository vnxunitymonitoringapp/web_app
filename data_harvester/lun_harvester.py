#!/usr/bin/python
import sys
import os
import datetime
import mysql.connector
import functions
import logging
from storops import *
from storops import VNXSystem
from storops import UnitySystem
from db_schema import strsys

#script description
#script name: data_harvester.py
#author: Belhassen Dahmen
#library: native python libraries, emc storops library for an overlay to communicate with vnx storage system
#usage: cron use
#database config
logging.basicConfig(level=logging.INFO)
separator = ":"
hostname = "localhost"
username = "root"
password = ""
db = "FTS_mon_app"
#initial config
FTS_mon_app = mysql.connector.connect(
	host=hostname,
	user=username,
	passwd=password,
	database=db
)
strsys_list = []
dte = str(datetime.date.today())
#retrieving from database
res_get_strsys = functions.get_strsys(FTS_mon_app)
if len(res_get_strsys) > 0:
    for strsys_elem in res_get_strsys:
        strsys_obj = strsys(strsys_elem[0],strsys_elem[1],strsys_elem[2],strsys_elem[3],strsys_elem[4],strsys_elem[5],strsys_elem[6],strsys_elem[7],strsys_elem[8],strsys_elem[9],strsys_elem[10],strsys_elem[11],strsys_elem[12])
        nif_test = functions.test_strsys_up(strsys_obj)
        if nif_test != 0:
            strsys_list.append(strsys(strsys_elem[0],strsys_elem[1],strsys_elem[2],strsys_elem[3],strsys_elem[4],strsys_elem[5],strsys_elem[6],strsys_elem[7],strsys_elem[8],strsys_elem[9],strsys_elem[10],strsys_elem[11],strsys_elem[12]))
        else:
            print("Cannot reach storage system ID:%s NAME %s. Please check network and try it later. " % (str(strsys_obj.id_strsys),strsys_obj.name))
    print("%i strsys were retrieved from database." % len(strsys_list))
else:
    print("no entries in database yet.")
#updating luns
if len(strsys_list) > 0:
    for strsys_elem in strsys_list:
        print("Updating Luns for %s system..." % strsys_elem.name)
        pools_db = functions.get_pools_by_strsys_id(str(strsys_elem.id_strsys),FTS_mon_app)
        #vnx
        if strsys_elem.id_type == 1:
            rgs_db = functions.get_rgs_by_strsys_id(str(strsys_elem.id_strsys),FTS_mon_app)
            vnx = VNXSystem( strsys_elem.spa_ip, strsys_elem.user , strsys_elem.passwd )
            if not vnx.existed:
                vnx = vnx = VNXSystem( strsys_elem.spb_ip, strsys_elem.user , strsys_elem.passwd )
            luns = vnx.get_lun()
            luns = functions.get_lun_list_unprivate(luns)
            identifier_lun = []
            for lun in luns:
                identifier_lun.append((str(lun.lun_id),str(functions.get_lun_pool(lun,pools_db)),str(strsys_elem.id_strsys)))
                ll = functions.get_lun_by_id(FTS_mon_app,lun.lun_id,strsys_elem,functions.get_lun_pool(lun,pools_db))
                if len(ll) == 0:
                    if lun.is_thin_lun:
                        thin = 1
                    else:
                        thin = 0
                    lun_tuple = (str(lun.lun_id),str(functions.get_lun_pool(lun,pools_db)),str(strsys_elem.id_strsys),str(lun.name),str(thin))
                    functions.insert_lun(FTS_mon_app,lun_tuple)
                lun_stat_tuple = (str(lun.lun_id),str(functions.get_lun_pool(lun,pools_db)),str(strsys_elem.id_strsys),str(lun.total_capacity_gb),str(lun.user_capacity_gbs),str(lun.consumed_capacity_gbs),dte)
                functions.update_lun_stat(FTS_mon_app,lun_stat_tuple)
            all_luns = functions.get_luns_by_strsys(FTS_mon_app,strsys_elem)
            for lun in all_luns:
                found = False
                for idl in identifier_lun:
                    if (str(lun[0]) == str(idl[0])) and (str(lun[1]) == str(idl[1])) and (str(lun[2]) == str(idl[2])):
                        found = True
                if found == False:
                    functions.delete_lun((lun[0],lun[1],lun[2]),FTS_mon_app)
                    print("%s %s %s lun is deleted" % (lun[0],lun[1],lun[2]))


        #unity
        elif strsys_elem.id_type == 2:
            nif_test = functions.test_strsys_up(strsys_elem)
            if nif_test == 1:
                unity = UnitySystem( strsys_elem.spa_ip, strsys_elem.user , strsys_elem.passwd )
            elif nif_test == 2:
                unity = UnitySystem( strsys_elem.spb_ip, strsys_elem.user , strsys_elem.passwd )
            elif nif_test == 3:
                unity = UnitySystem( strsys_elem.cs_ip, strsys_elem.user , strsys_elem.passwd )
            else:
                print("Cannot reach storage system ID:%s NAME %s. Please check network and try it later. " % (str(strsys_elem.id_strsys),strsys_elem.name))
            luns = unity.get_lun()
            identifier_lun = []
            for lun in luns:
                lun_id = lun.id
                l_id = lun_id[lun_id.find('_')+1:len(lun_id)]
                identifier_lun.append((str(l_id),str(functions.get_lun_pool_unity(lun.pool.name,pools_db)),str(strsys_elem.id_strsys)))
                ll = functions.get_lun_by_id(FTS_mon_app,l_id,strsys_elem,functions.get_lun_pool_unity(lun.pool.name,pools_db))
                if len(ll) == 0:
                    if lun.is_thin_enabled:
                        thin = 1
                    else:
                        thin = 0
                    lun_tuple = (str(l_id),str(functions.get_lun_pool_unity(lun.pool.name,pools_db)),str(strsys_elem.id_strsys),str(lun.name),str(thin))
                    functions.insert_lun(FTS_mon_app,lun_tuple)
                total = round(float(lun.size_total) / (1024 * 1024 * 1024),3)
                user = total
                consumed = round(float(lun.size_allocated) / (1024 * 1024 * 1024),3)
                lun_stat_tuple = (str(l_id),str(functions.get_lun_pool_unity(lun.pool.name,pools_db)),str(strsys_elem.id_strsys),str(total),str(user),str(consumed),dte)
                functions.update_lun_stat(FTS_mon_app,lun_stat_tuple)
                identifier_lun.append(lun_stat_tuple)
            all_luns = functions.get_luns_by_strsys(FTS_mon_app,strsys_elem)
            for lun in all_luns:
                found = False
                for idl in identifier_lun:
                    if (str(lun[0]) == str(idl[0])) and (str(lun[1]) == str(idl[1])) and (str(lun[2]) == str(idl[2])):
                        found = True
                if found == False:
                    functions.delete_lun((lun[0],lun[1],lun[2]),FTS_mon_app)
                    print("%s %s %s lun is deleted" % (lun[0],lun[1],lun[2]))
        print("Finished updating for %s ." % (strsys_elem.name))
else:
    print("No storage system is present or reachable. Please check network and try later.")
