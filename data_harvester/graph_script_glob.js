const ChartjsNode = require('chartjs-node');
var chartNode = new ChartjsNode(700, 600);
var graphData1 = {
     labels: [
"2019-05-03", "2019-05-04", "2019-05-05", "2019-05-06"], datasets : [
{
label: "Global storage usage",
borderColor: "#007db8",
backgroundColor: "#007db8",
pointBackgroundColor: "#007db8",
data : [ 
49778.974, 49782.885, 49786.37, 49788.77, ],fill:false}]};
var graphOP = {
type: 'line',
data: graphData1,
options: {    scales: {    yAxes: [{        ticks: {            beginAtZero: true        }    }]}}};
return chartNode.drawChart(graphOP)
.then(() => {
return chartNode.getImageBuffer('image/png');
})
.then(buffer => {
Array.isArray(buffer);
return chartNode.getImageStream('image/png');
})
.then(streamResult => {
return chartNode.writeImageToFile('image/png', '/var/www/html/fts_mon_app_db/data_harvester/graph_glob.png');
})
.then(() => {
});
