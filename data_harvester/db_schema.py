#!/usr/bin/python
class strsys:
    def __init__(self, id_strsys, id_type, name, site_location, serial_number, model, spa_ip, spb_ip, cs_ip, firmware_ver,system_health,user,passwd):
        self.id_strsys = id_strsys
        self.id_type = id_type
        self.name = name
        self.site_location = site_location
        self.serial_number = serial_number
        self.model = model
        self.spa_ip = spa_ip
        self.spb_ip = spb_ip
        self.cs_ip = cs_ip
        self.firmware_ver = firmware_ver
        self.user = user
        self.passwd = passwd  

