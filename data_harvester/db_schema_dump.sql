-- MySQL dump 10.13  Distrib 5.6.43, for Linux (x86_64)
--
-- Host: localhost    Database: FTS_mon_app
-- ------------------------------------------------------
-- Server version	5.6.43

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alerts`
--

DROP TABLE IF EXISTS `alerts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alerts` (
  `id_alert` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(1) NOT NULL,
  `description` text,
  `code` varchar(20) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `id_strsys` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_alert`),
  KEY `id_strsys` (`id_strsys`),
  CONSTRAINT `alerts_ibfk_1` FOREIGN KEY (`id_strsys`) REFERENCES `strsys` (`id_strsys`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lun_status`
--

DROP TABLE IF EXISTS `lun_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lun_status` (
  `id_status` int(11) NOT NULL AUTO_INCREMENT,
  `id_lun` int(11) NOT NULL,
  `id_pool` int(11) NOT NULL,
  `id_strsys` int(11) NOT NULL,
  `total_cap_gb` float DEFAULT NULL,
  `user_cap_gb` float DEFAULT NULL,
  `consumed_cap_gb` float DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id_status`),
  KEY `id_lun` (`id_lun`,`id_pool`,`id_strsys`),
  CONSTRAINT `lun_status_ibfk_1` FOREIGN KEY (`id_lun`, `id_pool`, `id_strsys`) REFERENCES `luns` (`id_lun`, `id_pool`, `id_strsys`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=78499 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `luns`
--

DROP TABLE IF EXISTS `luns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `luns` (
  `id_lun` int(11) NOT NULL,
  `id_pool` int(11) NOT NULL,
  `id_strsys` int(11) NOT NULL,
  `name` text,
  `is_thin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_lun`,`id_pool`,`id_strsys`),
  KEY `id_pool` (`id_pool`),
  KEY `id_strsys` (`id_strsys`),
  CONSTRAINT `luns_ibfk_2` FOREIGN KEY (`id_strsys`) REFERENCES `strsys` (`id_strsys`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pool_status`
--

DROP TABLE IF EXISTS `pool_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pool_status` (
  `id_status` int(11) NOT NULL AUTO_INCREMENT,
  `total_cap_gb` float DEFAULT NULL,
  `used_cap_gb` float DEFAULT NULL,
  `free_cap_gb` float DEFAULT NULL,
  `per_subscription` float DEFAULT NULL,
  `id_pool` int(11) NOT NULL,
  `id_strsys` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_status`),
  KEY `pool_status_ibfk_1` (`id_pool`,`id_strsys`),
  CONSTRAINT `pool_status_ibfk_1` FOREIGN KEY (`id_pool`, `id_strsys`) REFERENCES `pools` (`id_pool`, `id_strsys`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33650 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pools`
--

DROP TABLE IF EXISTS `pools`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pools` (
  `id_pool` int(11) NOT NULL,
  `name` text,
  `threshold` float NOT NULL DEFAULT '0',
  `id_strsys` int(11) NOT NULL,
  PRIMARY KEY (`id_pool`,`id_strsys`),
  KEY `id_strsys` (`id_strsys`),
  CONSTRAINT `pools_ibfk_1` FOREIGN KEY (`id_strsys`) REFERENCES `strsys` (`id_strsys`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `report_entries`
--

DROP TABLE IF EXISTS `report_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_entries` (
  `entry_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_strsys` int(11) NOT NULL,
  `report_id` int(11) NOT NULL,
  PRIMARY KEY (`entry_id`),
  KEY `report_id` (`report_id`),
  KEY `report_entries_ibfk_1` (`id_strsys`),
  CONSTRAINT `report_entries_ibfk_1` FOREIGN KEY (`id_strsys`) REFERENCES `strsys` (`id_strsys`) ON DELETE CASCADE,
  CONSTRAINT `report_entries_ibfk_2` FOREIGN KEY (`report_id`) REFERENCES `reports` (`report_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reports`
--

DROP TABLE IF EXISTS `reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reports` (
  `report_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `email_list` text,
  `frequency` int(1) NOT NULL DEFAULT '0',
  `date` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `sorting` int(10) NOT NULL DEFAULT '2',
  `number_elem` int(10) NOT NULL DEFAULT '10',
  PRIMARY KEY (`report_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `reports_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rg_status`
--

DROP TABLE IF EXISTS `rg_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rg_status` (
  `id_status` int(11) NOT NULL AUTO_INCREMENT,
  `user_cap_gb` float DEFAULT NULL,
  `free_cap_gb` float DEFAULT NULL,
  `id_rg` int(11) NOT NULL,
  `id_strsys` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_status`),
  KEY `rg_status_ibfk_1` (`id_rg`,`id_strsys`),
  CONSTRAINT `rg_status_ibfk_1` FOREIGN KEY (`id_rg`, `id_strsys`) REFERENCES `rgs` (`id_rg`, `id_strsys`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13704 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rgs`
--

DROP TABLE IF EXISTS `rgs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rgs` (
  `id_rg` int(11) NOT NULL,
  `type` int(1) NOT NULL DEFAULT '0',
  `id_strsys` int(11) NOT NULL,
  PRIMARY KEY (`id_rg`,`id_strsys`),
  KEY `id_strsys` (`id_strsys`),
  CONSTRAINT `rgs_ibfk_1` FOREIGN KEY (`id_strsys`) REFERENCES `strsys` (`id_strsys`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `strsys`
--

DROP TABLE IF EXISTS `strsys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `strsys` (
  `id_strsys` int(11) NOT NULL AUTO_INCREMENT,
  `id_type` int(10) unsigned NOT NULL,
  `name` text,
  `site_location` text,
  `serial_number` text,
  `model` varchar(15) DEFAULT NULL,
  `spa_ip` int(4) unsigned NOT NULL,
  `spb_ip` int(4) unsigned NOT NULL,
  `cs_ip` int(4) unsigned NOT NULL,
  `firmware_ver` text,
  `system_health` int(1) unsigned NOT NULL,
  `reachable` int(1) NOT NULL DEFAULT '0',
  `user` text,
  `passwd` text,
  PRIMARY KEY (`id_strsys`),
  KEY `id_type` (`id_type`),
  CONSTRAINT `strsys_ibfk_1` FOREIGN KEY (`id_type`) REFERENCES `sys_types` (`id_type`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sys_types`
--

DROP TABLE IF EXISTS `sys_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_types` (
  `id_type` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `passwd` varchar(20) NOT NULL DEFAULT 'abc123',
  `level` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-20 11:53:41
