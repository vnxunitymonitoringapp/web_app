<?php
$files = array("free","total","subscribed","used");
$pools = array();
$first_loop = true;
$Go = 1024*1024*1024;
$j = 0;
$id_strsys = 17;
foreach($files as $file)
{
    $i = 0;
    if($stream = fopen($file,"r"))//config file parsing
    {
        while(!feof($stream))
        {
            $line = rtrim(ltrim(fgets($stream)));
            $elements = explode(" ",$line);
            if($first_loop)
            {
                $pool_stat = array(
                    "id" => $elements[3],
                    "date" => $elements[0]." ".$elements[1],
                    "free" => floatval($elements[5]) / $Go,
                    "total" => 0,
                    "used" => 0, 
                    "subscribed" => 0,
                    "per_subscribed" => 0
                );
                array_push($pools,$pool_stat);
            }
            else
            {
                $pools[$i][$files[$j]] = floatval($elements[5]) / $Go;
                if($files[$j] == "subscribed")
                    $pools[$i]["per_subscribed"] = number_format(($pools[$i]["subscribed"] / $pools[$i]["total"]) * 100,3);
                $i++;
            }
        }
    }
    $j++;
    $first_loop = false;
}

if($file = fopen('data_extract','a'))
{
    foreach($pools as $pool)
    {
        $line=$pool["total"].",".$pool["used"].",".$pool["total"].",".$pool["per_subscribed"].",".$pool["id"].",".$id_strsys.",".$pool["date"].PHP_EOL;
        fwrite($file,$line);
    }
}

?>