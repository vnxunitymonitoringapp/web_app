'use strict';
var used = process.argv[2];
var free = process.argv[3];
var used_tot = process.argv[4];
var free_tot = process.argv[5];
const ChartjsNode = require('chartjs-node');
var chartNode = new ChartjsNode(556, 278);
var donutOptions = {
        responsive: true,
        cutoutPercentage: 20, 
        legend: {position:'left', 
            labels:{pointStyle:'rectRounded',
                usePointStyle:true,
                fontSize: 16
            },
            onClick : function() {}
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    var label = data.datasets[0].data[tooltipItem.index];
                    return label;
                }
            },
            bodyFontSize: 18
        }
    };
var label1 = 'Used capacity '+used_tot+' GB ('+used.toString()+'%)';
var label2 = 'Free capacity '+free_tot+' GB ('+free.toString()+'%)';
var chDonutData1 = {
        labels: [label2,label1],
        datasets: [
        {
            backgroundColor: ["#32ff7d","#007db8"],
            borderWidth: 0,
            data: [free,used]
        }
        ]
    };
var chartOP = {
	type: 'pie',
	data: chDonutData1,
	options: donutOptions
};
var chartname = used.toString()+free.toString()+'.png';
return chartNode.drawChart(chartOP)
.then(() => {
    // chart is created
 
    // get image as png buffer
    return chartNode.getImageBuffer('image/png');
})
.then(buffer => {
    Array.isArray(buffer) // => true
    // as a stream
    return chartNode.getImageStream('image/png');
})
.then(streamResult => {
    // using the length property you can do things like
    // directly upload the image to s3 by using the
    // stream and length properties
    streamResult.stream // => Stream object
    streamResult.length // => Integer length of stream
    // write to a file
    return chartNode.writeImageToFile('image/png', '/var/www/html/fts_mon_app_db/data_harvester/'+chartname);
})
.then(() => {
    // chart is now written to the file path
    // ./testimage.png
});
