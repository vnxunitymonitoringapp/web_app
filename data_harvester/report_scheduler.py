#!/usr/bin/python
import sys
import os
import datetime
import mysql.connector
import requests
import functions
import random
import base64
from db_schema import strsys
from Naked.toolshed.shell import execute_js, muterun_js
import smtplib
from os.path import basename
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
FREQ = -1
evolution = 1
try:
    if len(sys.argv) == 2:
        if sys.argv[1] == "day":
            FREQ = 2
        elif sys.argv[1] == "week":
            FREQ = 1
            evolution = 7
        elif sys.argv[1] == "month":
            FREQ = 0
            evolution = 30
        else:
            raise SystemExit
    else:
        raise SystemExit
except SystemExit:
    print("Please enter a frequency argument: day | week | month")
    sys.exit()
#logging
#email smtp config
from_ = "bhs0082@uib.socgen"
sending_ = "FTS.monitoring@uib.com.tn"
mdp = "M0nitoring2019"
subject = "FTS Storage Monitoring: UIB "
if FREQ == 2:
    subject += "Daily Report"
elif FREQ == 1:
    subject += "Weekly Report"
elif FREQ == 0:
    subject += "Monthly Report"
content = "Automatic report for configured systems, Please don't answer. - FTS Monitoring"
smtp = "192.102.53.9"
smtp_port = 25
reports_root = "/var/www/html/fts_mon_app_db/reports/"
script_root = "/var/www/html/fts_mon_app_db/data_harvester"
#database access
hostname = "localhost"
username = "root"
password = ""
db = "FTS_mon_app"
max_disp = 30
FTS_mon_app = mysql.connector.connect(
	host=hostname,
	user=username,
	passwd=password,
	database=db
)
reports = functions.get_reports_by_Freq(FTS_mon_app,FREQ)
report_generator = "http://localhost/report_creator.php"
#0: id
#1: name
#2: email_list
#3: freq
#4:date
#5: owner
for report in reports:
    print("Creating report %s..." % report[1])
    name_rep = report[1]
    emails = report[2].split(",")
    entries = functions.get_report_entries(FTS_mon_app,report[0])
    sorting = report[6]
    number_elem = report[7]
    id_strsys = []
    reports = []
    for entry in entries:
        id_strsys.append(entry[1])
    for id_sys in id_strsys:
        strsys = functions.get_strsys_by_id(id_sys,FTS_mon_app)
        last_pools_entries = functions.load_last_pools_entry(FTS_mon_app,id_sys)
        if strsys[1] == 1:
            last_rgs_entries = functions.load_last_rgs_entry(FTS_mon_app, id_sys)
        pools_id = functions.get_pools_by_strsys_id( id_sys,FTS_mon_app)
        if strsys[1] == 1:
            rgs_id = functions.get_rgs_by_strsys_id( id_sys,FTS_mon_app)
        dates = functions.get_dates(FTS_mon_app, strsys[1],strsys[0] )
        used = 0
        free = 0
        used_tot = 0
        free_tot = 0
        for pool in last_pools_entries:
            used_tot = used_tot + pool[2]
            free_tot = free_tot + pool[3]
            used = used + (pool[2]*100)/pool[1]
            free = free + (pool[3]*100)/pool[1]
        if strsys[1] == 1:
            for rg in last_rgs_entries:
                used = used + ((rg[1]-rg[2])*100)/rg[1]
                free = free + (rg[2]*100)/rg[1]
                used_tot = used_tot + rg[1]-rg[2]
                free_tot = free_tot + rg[2]
        fraction = len(last_pools_entries)
        if strsys[1] == 1:
            fraction = fraction  + len(last_rgs_entries)
        used = used /fraction
        free = free/fraction
        used = round(used,2)
        free = round(free,2)
        print("Generating chart..")
        script = "%s/gen_chart.js %s %s %s %s" % (script_root, str(used),str(free), str(used_tot),str(free_tot))
        result = muterun_js(script)
        if result:
            name = "%s%s.png" % (str(used),str(free))
            print("chart generated under name %s" % (name))
        print("Generating graph..")
        graph_script = "const ChartjsNode = require('chartjs-node');\n"
        graph_script += "var chartNode = new ChartjsNode(700, 600);\n"
        graph_script += "var graphData1 = {\n"
        graph_script += "     labels: [\n"
        i= 0
        while len(dates) > max_disp:
            dates.pop(0)
        for date in dates:
            graph_script += "\""+str(date[0])+"\""
            if i < len(dates) - 1:
                graph_script += ", "
            i += 1
        graph_script += "], datasets : [\n"
        i = 0
        for pool in pools_id:
            color = "#"+''.join([random.choice('0123456789ABCDEF') for h in range(6)])
            graph_script += "{\n"
            graph_script += "label: \""+str(pool[1])+"\",\n"
            graph_script += "borderColor: \""+color+"\",\n"
            data = functions.load_pool_entries(FTS_mon_app,pool[0],id_sys)
            graph_script += "data : [ \n"
            m = 0
            while m < len(dates) - len(data):
                graph_script += "0, "
                m += 1
            j = 0
            while len(data) > max_disp:
                data.pop(0)
            for dt in data:
                graph_script += str(round(dt[2],3))
                if j < len(data) - 1:
                    graph_script += ", "
                j += 1
            graph_script += "], fill:false \n}"
            if i < len(pools_id) - 1 or len(rgs_id) > 0:
                graph_script += ", \n"
            i += 1
        i = 0
        if strsys[1] == 1:
            for rg in rgs_id:
                color = "#"+''.join([random.choice('0123456789ABCDEF') for h in range(6)])
                graph_script += "{"
                graph_script += "label: \"RAID GROUP "+str(rg[1])+"\",\n"
                graph_script += "borderColor: \""+color+"\",\n"
                graph_script += "backgroundColor: \""+color+"\",\n"
                graph_script += "pointBackgroundColor: \""+color+"\",\n"
                data = functions.load_rg_entries(FTS_mon_app,rg[0], id_sys)
                graph_script += "data : [ \n"
                j = 0
                m = 0
                while m < len(dates) - len(data):
                    graph_script += "0, "
                    m += 1
                while len(data) > max_disp:
                    data.pop(0)
                for dt in data:
                    graph_script += str(round(dt[1] - dt[2],3))
                    if j < len(data) - 1:
                        graph_script += ","
                    j += 1
                graph_script += "], fill:false }"
                if i < len(rgs_id) - 1:
                    graph_script += ", \n"
                i += 1
        graph_script += "]"
        graph_script += "};\n"
        graph_script += "var graphOP = {\n"
        graph_script += "type: 'line',\n"
        graph_script += "data: graphData1,\n"
        graph_script += "options: {"
        graph_script += "    scales: {"
        graph_script += "    yAxes: [{"
        graph_script += "        ticks: {"
        graph_script += "            beginAtZero: true"
        graph_script += "        }"
        graph_script += "    }]"
        graph_script += "}"
        graph_script += "}};\n"
        graph_script += "return chartNode.drawChart(graphOP)\n"
        graph_script += ".then(() => {\n"
        graph_script += "return chartNode.getImageBuffer('image/png');\n"
        graph_script += "})\n"
        graph_script += ".then(buffer => {\n"
        graph_script += "Array.isArray(buffer);\n"
        graph_script += "return chartNode.getImageStream('image/png');\n"
        graph_script += "})\n"
        graph_script += ".then(streamResult => {\n"
        graph_script += "return chartNode.writeImageToFile('image/png', '"+script_root+"/graph.png');\n"
        graph_script += "})\n"
        graph_script += ".then(() => {\n"
        graph_script += "});\n"
        script_js = open(script_root+"/graph_script.js","w")
        script_js.write(graph_script)
        script_js.close()
        result = muterun_js(script_root+"/graph_script.js")
        if result:
            print("graph generated under name graph.png")
        ############## global graph
        print("Generating global graph..")
        graph_script_glob = "const ChartjsNode = require('chartjs-node');\n"
        graph_script_glob += "var chartNode = new ChartjsNode(700, 600);\n"
        graph_script_glob += "var graphData1 = {\n"
        graph_script_glob += "     labels: [\n"
        i= 0
        while len(dates) > max_disp:
            dates.pop(0)
        for date in dates:
            graph_script_glob += "\""+str(date[0])+"\""
            if i < len(dates) - 1:
                graph_script_glob += ", "
            i += 1
        graph_script_glob += "], datasets : [\n"
        
        color = "#007db8"
        graph_script_glob += "{\n"
        graph_script_glob += "label: \"Global storage usage\",\n"
        graph_script_glob += "borderColor: \""+color+"\",\n"
        graph_script_glob += "backgroundColor: \""+color+"\",\n"
        graph_script_glob += "pointBackgroundColor: \""+color+"\",\n"
        graph_script_glob += "data : [ \n"
        glob_data = [None]*len(dates)
        for index in range(len(glob_data)):
            glob_data[index] = 0
        for pool in pools_id:
            data = functions.load_pool_entries(FTS_mon_app,pool[0],id_sys)
            m = 0
            while m < len(dates) - len(data):
                glob_data[m] = glob_data[m] + 0
                m += 1
            while len(data) > max_disp:
                data.pop(0)
            for dt in data:
                glob_data[m] = round(dt[2],3) + glob_data[m]
                m = m +1
        if strsys[1] == 1:
            for rg in rgs_id:
                data = functions.load_rg_entries(FTS_mon_app,rg[0],id_sys)
                m = 0
                while m < len(dates) - len(data):
                    glob_data[m] = glob_data[m] + 0
                    m += 1
                while len(data) > max_disp:
                    data.pop(0)
                for dt in data:
                    glob_data[m] = round(dt[2],3) + glob_data[m]
                    m = m +1
        i = 0
        for glob_dt in glob_data:
            graph_script_glob += str(glob_dt)
            if i < len(glob_data):
                graph_script_glob += ", "
            i = i +1
        graph_script_glob += "],fill:false}]"
        graph_script_glob += "};\n"
        graph_script_glob += "var graphOP = {\n"
        graph_script_glob += "type: 'line',\n"
        graph_script_glob += "data: graphData1,\n"
        graph_script_glob += "options: {"
        graph_script_glob += "    scales: {"
        graph_script_glob += "    yAxes: [{"
        graph_script_glob += "        ticks: {"
        graph_script_glob += "            beginAtZero: true"
        graph_script_glob += "        }"
        graph_script_glob += "    }]"
        graph_script_glob += "}"
        graph_script_glob += "}};\n"
        graph_script_glob += "return chartNode.drawChart(graphOP)\n"
        graph_script_glob += ".then(() => {\n"
        graph_script_glob += "return chartNode.getImageBuffer('image/png');\n"
        graph_script_glob += "})\n"
        graph_script_glob += ".then(buffer => {\n"
        graph_script_glob += "Array.isArray(buffer);\n"
        graph_script_glob += "return chartNode.getImageStream('image/png');\n"
        graph_script_glob += "})\n"
        graph_script_glob += ".then(streamResult => {\n"
        graph_script_glob += "return chartNode.writeImageToFile('image/png', '"+script_root+"/graph_glob.png');\n"
        graph_script_glob += "})\n"
        graph_script_glob += ".then(() => {\n"
        graph_script_glob += "});\n"
        script_js = open(script_root+"/graph_script_glob.js","w")
        script_js.write(graph_script_glob)
        script_js.close()
        result = muterun_js(script_root+"/graph_script_glob.js")
        if result:
            print("graph generated under name graph_glob.png")
        ############## global graph
        with open(script_root+"/"+str(used)+str(free)+".png", "rb") as chart:
            base64chart = base64.b64encode(chart.read()).decode()
            chart.close()
        with open(script_root+"/graph.png","rb") as graph:
            base64graph = base64.b64encode(graph.read()).decode()
            graph.close()
        with open(script_root+"/graph_glob.png","rb") as graph_glob:
            base64graph_glob = base64.b64encode(graph_glob.read()).decode()
            graph_glob.close()
        os.remove(script_root+"/"+str(used)+str(free)+".png")
        os.remove(script_root+"/graph.png")
        os.remove(script_root+"/graph_glob.png")
        uri_chart = 'data:image/png;base64,{}'.format(base64chart)
        uri_graph = 'data:image/png;base64,{}'.format(base64graph)
        uri_graph_glob = 'data:image/png;base64,{}'.format(base64graph_glob)
        PARAMS = {'id_strsys':id_sys,'chartPie':uri_chart,'graph':uri_graph,'graph-global':uri_graph_glob, 'report':name_rep, 'evolution':evolution, 'freq':FREQ, 'sorting': sorting, 'number_elem':number_elem}
        response = requests.post(url = report_generator, data = PARAMS)
        reports.append(reports_root+str(response.content))
    print(reports)
    print("mail will be sent to:")
    print(emails)
    msg = MIMEMultipart()
    msg['From'] = sending_
    msg['To'] = ','.join(emails)
    msg['Subject'] = subject
    body = MIMEText(content, 'plain')
    msg.attach(body)
    for f in reports:
	with open(f,'r') as con:
        	part = MIMEApplication(con.read(),Name=basename(f))
        part['Content-Disposition'] = 'attachment; filename="{}"'.format(basename(f))
        msg.attach(part)
    try:
        server = smtplib.SMTP(smtp, smtp_port)
        server.ehlo()
        server.login(from_,mdp)
    except:
        print("Login failed to SMTP server.")
    text = msg.as_string()
    try:
        server.sendmail(sending_ , emails, text)
    except:
        print("Unable to send email.")
    server.quit()
    
if len(reports) == 0:
    print("No report to execute.")
