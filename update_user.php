<?php
session_start();
if(!isset($_SESSION['user_id']))
{
    session_destroy();
    header('Location: connect.php');
}
else
{
    /**
    * start program by including core.php and run include_core_files()
    */
    include "core.php";
    //including core files
    include_core_files();
    $db = new db_object();
    $current_page = "Dashboard";
    $userId = $_SESSION['user_id'];
    $userLevel = $_SESSION['level'];
    $userName = $_SESSION['name'];
    if(isset($_POST['userID']) || isset($_POST['action']))
    {
        if($userLevel >= 0)
        {
            if($_POST['action'] == "reset")
            {
                if($db->verify_user_by_id($_POST['userID']))
                {
                    if($db->reset_user($_POST['userID']))
                    {
                        echo "User reset with success.";
                    }
                    else
                    {
                        echo "User already reset.";
                    }
                }
                else
                {
                    echo "User does not exist in database. Please choose another one.";
                }
            }
            else if($_POST['action'] == "update")
            {
                if(isset($_POST['newLvl']))
                {
                    if($db->verify_user_by_id($_POST['userID']))
                    {
                        if($db->update_user($_POST['userID'],$_POST['newLvl']))
                        {
                            echo "User updated with success.";
                        }
                        else
                        {
                            echo "User already at same level.";
                        }
                    }
                    else
                    {
                        echo "User does not exist in database. Please choose another one.";
                    }
                }
            }
            else if($_POST['action'] == "passwd")
            {
                if(isset($_POST['newPasswd']) && isset($_POST['newPasswdConfirm']) && isset($_POST['ancientPasswd']))
                {
                    if($_POST['newPasswd'] == $_POST['newPasswdConfirm'])
                    {
                        if($db->change_user_passwd($userId,$_SESSION['passwd'],$_POST['ancientPasswd'],$_POST['newPasswd']))
                        {
                            echo "Your password has been updated succesfully";
                            $connected_user = $db->verify_user_by_name($userName,$_POST['newPasswd']);
                            if(!empty($connected_user))
                            {
                                $_SESSION['user_id'] = $connected_user[0];
                                $_SESSION['name'] = $connected_user[1];
                                $_SESSION['passwd'] = $connected_user[2];
                                $_SESSION['level'] = $connected_user[3];
                            }
                        }
                        else
                            echo "Unable to update password. Maybe it's the same as the ancient one.";
                    }
                    else
                    {
                        echo "Unable to update password. Confirmation field is not the same as the new password.";
                    }
                }
            }
        }
        else
        {
            echo "Your runlevel does not allow you to do such action.";
        }
    }
    else
    {
        session_destroy();
        header('Location: connect.php');
    }
}
?>