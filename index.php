<?php
session_start();
if(!isset($_SESSION['user_id']))
{
    session_destroy();
    header('Location: connect.php');
}
else
{
    /**
    * start program by including core.php and run include_core_files()
    */
    include "core.php";
    //including core files
    include_core_files();
    $db = new db_object();
    $current_page = "Dashboard";
    $userId = $_SESSION['user_id'];
    $userLevel = $_SESSION['level'];
    $userName = $_SESSION['name'];
    $types = $db->get_strsys_types();
    $strsys = $db->load_all_strsys();


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 5.0 Strict//EN">

<html lang="en">
    <?php main_head_gen($current_page); ?>
    <body class="bg-light">
        <!-- ******************** USER content **************************** -->
        <!-- ================= Add new system modal ======================= -->
        <?php
            if($userLevel > 0)
            {
                add_new_system_mod_gen($types);
            } 
         ?>
        <!-- ================= Help modal ================================= -->
        <?php
        help_mod_gen();
        ?>
        <!-- ================ navbar beginning ============================ -->
        <?php main_navbar_gen($userName); ?>
        <!-- ================ navbar ending ============================ -->
        <!-- ================ wrapper beginning ======================== -->
        <div class="wrapper">
            <!-- ================ sidebar beginning ======================== -->
            <?php side_navbar_gen($strsys); ?>
            <!-- ================ sidebar ending ======================== -->
            <!-- ================ content beginning ======================== -->
            <div id="content">
                <div class="d-sm-flex justify-content-between">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb no-bg">
                            <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                        </ol>
                    </nav>
                    <?php if($userLevel > 0) { ?>
                    <div class="p-2" style="position:relative;">
                        <a class="stretched-link nounderline" href="#" data-toggle="modal" data-target="#addNewSystemModal">
                            <span>Add System</span>
                            <button class="btn btn-primary rounded-circle btn-sm">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                        </a>
                    </div>
                    <?php } ?>
                </div>
                <!-- ============ System general display ================ -->
                <div class="container-fluid">
                    <div class="row">
                        <?php
                        if(sizeof($strsys))
                        {
                            foreach($strsys as $system)
                            {
                                $strsys = $db->load_strsys_byID($system['id_strsys']);
                                $entries_pools = $db->load_last_pools_entry($system);
                                $pools = $db->get_system_pools_byID($system);
                                $rgs = array();
                                $entries_rgs = array();
                                if($system['id_type'] == 1)
                                {
                                    $rgs = $db->get_system_rgs_byID($system);
                                    $entries_rgs = $db->load_last_rgs_entry($system);
                                }
                                $state_pools = get_pools_global_health($pools,$rgs,$entries_pools,$entries_rgs);
                            ?>
                            <div class="col-sm-6 mb-4">
                            <?php
                            strsys_card_gen($system,$types,1,$system['id_strsys'],$state_pools);
                            $i++;
                            $rgs = array();
                            $entries_rgs = array();
                            ?>
                            </div>
                            <?php
                            }
                        }
                        ?>
                    </div>
                </div>
                <br>
            </div>
            <!-- ================ content ending ======================== -->
        </div>
        <!-- ================ wrapper ending ======================== -->
        <!-- ================ footer beginning ============================ -->
        <?php
        main_footer_gen();
        ?>
        <!-- ================ footer ending ============================ -->
        <!-- ******************** JQUERY LIBRARY ************************** -->
        <?php
        js_libraries_gen();
        add_strsys_script();
        ?>
    </body>
    <script>
        $(document).ready(function() {
            $('.card-load').each(function() {
                var card = $(this);
                var id_str = String(this.id);
                id_str = id_str.replace('label-','');
                $.ajax({
                    url: 'check_system_up.php',
                    method: 'post',
                    data: {
                        'id_strsys' : id_str
                    },
                    success:function(data) {
                        card.find('.card-spinner').remove();
                        if(data == "0")
                        {
                            card.find(".card-img-top").addClass('off');
                            card.find(".col-form-label").addClass('text-secondary');
                            card.find(".form-control-plaintext").addClass('text-secondary');
                            card.find(".systemHealth-c").removeClass('text-success').removeClass('text-warning').removeClass('text-danger').addClass('text-secondary');
                            card.find("#health-last").after('<div class=\"form-group no-mb row\"><label for=\"stat\" class=\"col-sm-5 col-form-label py-0 font-weight-bold text-right text-secondary\">Status: </label><div class=\"col-sm-7\"><input type=\"text\" readonly class=\"form-control-plaintext py-0 text-secondary\" id=\"stat\" value=\"Not Connected\"></div></div>');
                        }
                    }
                });
            });
        });
    </script>
</html>
<?php 
}
?>