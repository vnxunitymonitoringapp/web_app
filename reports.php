<?php
session_start();
if(!isset($_SESSION['user_id']))
{
    session_destroy();
    header('Location: connect.php');
}
else
{
    /**
    * start program by including core.php and run include_core_files()
    */
    include "core.php";
    //including core files
    include_core_files();
    $db = new db_object();
    $current_page = "Reports Management";
    $userId = $_SESSION['user_id'];
    $userLevel = $_SESSION['level'];
    $userName = $_SESSION['name'];
    $types = $db->get_strsys_types();
    $reports = $db->get_all_reports();
    $strsys = $db->load_all_strsys();
    if(isset($_GET['index']))
    {
        $index = $_GET['index'];
    }
    else
        $index = 0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 5.0 Strict//EN">

<html lang="en">
    <?php main_head_gen($current_page); ?>
    <body class="bg-light">
        <!-- ******************** USER content **************************** -->
        <!-- ================== Information modal ========================= -->
        <div class="modal fade" id="informationModal" tabindex="-1" role="dialog" aria-labelledby="info-modal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Information</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                </div>
            </div>
        </div>
        <!-- ================= Add new system modal ======================= -->
        <?php
            if($userLevel > 0)
            {
                add_new_system_mod_gen($types);
            } 
         ?>
         <!-- ================= Add new report modal ======================= -->
        <?php
            if($userLevel > 0)
            {
                generate_report_mod_gen($strsys);
            } 
         ?>
        <!-- ================= Help modal ================================= -->
        <?php
        help_mod_gen();
        delete_file_mod();
        ?>
        <!-- ================ navbar beginning ============================ -->
        <?php main_navbar_gen($userName); ?>
        <!-- ================ navbar ending ============================ -->
        <!-- ================ wrapper beginning ======================== -->
        <div class="wrapper">
            <!-- ================ sidebar beginning ======================== -->
            <?php side_navbar_gen($strsys); ?>
            <!-- ================ sidebar ending ======================== -->
            <!-- ================ content beginning ======================== -->
            <div id="content">
                <div class="d-sm-flex justify-content-between">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb no-bg">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item" aria-current="page">Reports Management</li>
                        </ol>
                    </nav>
                    <?php if($userLevel > 0) { ?>
                    <div class="p-2" style="position:relative;">
                        <a class="stretched-link nounderline" href="#" data-toggle="modal" data-target="#add-report">
                            <span>New Report</span>
                            <button class="btn btn-primary rounded-circle btn-sm">
                                <i class="fa fa-file" aria-hidden="true"></i>
                            </button>
                        </a>
                    </div>
                    <?php } ?>
                </div>
                <!-- ============ Reports display ================ -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" id="reports-tab" data-toggle="tab" href="#reports" role="tab" aria-controls="reports" aria-selected="true">Reports</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="reports-sched-tab" data-toggle="tab" href="#reports-sched" role="tab" aria-controls="reports-sched" aria-selected="false">Scheduled Reports</a>
                                </li>
                            </ul>
                            <div class="card col-sm-12 border-top-0 tab-content" id="report-tabs">
                                <div class="tab-pane fade show active" id="reports" role="tabpanel" aria-labelledby="reports-tab">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <h5 class="card-title col-sm-6 text-info py-3">Reports</h5>
                                        </div>
                                    </div>
                                    <div class="card-body py-0">  
                                            <?php 
                                            $display_at_once = 40;
                                            pagination_gen($index,$display_at_once);
                                            reports_files_gen($index,$userLevel, $display_at_once); ?>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="reports-sched" role="tabpanel" aria-labelledby="reports-sched-tab">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <h5 class="card-title col-sm-6 text-info py-3">Scheduled Reports</h5>
                                        </div>
                                    </div>
                                    <div class="card-body py-0">  
                                        <?php reports_table_gen($reports, $db, $userLevel, $userId); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
            <!-- ================ content ending ======================== -->
        </div>
        <!-- ================ wrapper ending ======================== -->
        <!-- ================ footer beginning ============================ -->
        <?php
        main_footer_gen();
        ?>
        <!-- ================ footer ending ============================ -->
        <!-- ******************** JQUERY LIBRARY ************************** -->
        <?php
        js_libraries_gen();
        add_strsys_script();
        delete_file_script();
        ?>
        <script type="text/javascript">
        function display_info(msg)
        {
            $("#informationModal .modal-body").html(msg);
            $("#informationModal").modal("show");
        }
        $("#report").submit(function(e) {
                e.preventDefault();
                var buttonInsert = $("#addReport");
                var reportName = $("#reportName").val();
                var emailList = $("#emails").val();
                var frequency = $("#frequency").val();
                var entries = [];
                var sorting = $('input[name="sorting"]:checked').val();
                var number_elem = $('#number-elem').val();
                $("#report .form-check-input:checkbox").each(function(index){
                    if($(this).prop("checked") == true)
                        entries.push($(this).val());
                });
                if(reportName != "" && emailList != "" && entries.length > 0)
                {
                    reportName.replace(" ","");
                    buttonInsert.addClass('disabled');
                    buttonInsert.attr('aria-disabled','true');
                    $.ajax({
                        url: "insert_report.php",
                        type:'post',
                        data: {
                            'reportName': reportName,
                            'frequency': frequency,
                            'email_list': emailList.trim(),
                            'entries': entries,
                            'sorting': sorting,
                            'number_elem': number_elem
                        },
                        success:function(response)
                        {
                           $("#add-report").modal('hide'); 
                            display_info(response);
                        }
                    });
                }
            });
            $(".delete-rep-sch").on('click',function() {
                var target_rep = $(this).attr("id");
                target_rep = target_rep.replace("rep","");
                $.ajax({
                    url: "delete_report.php",
                    type:'post',
                    data: {
                        'report_id': target_rep,
                    },
                    success:function(response)
                    {
                        display_info(response);
                    }
                });
            });
        </script>
    </body>
</html>
<?php 
}
?>
