<?php
session_start();
if(!isset($_SESSION['user_id']))
{
    session_destroy();
    header('Location: connect.php');
}
else
{
    /**
    * start program by including core.php and run include_core_files()
    */
    include "core.php";
    //including core files
    include_core_files();
    $db = new db_object();
    $current_page = "Dashboard";
    $userId = $_SESSION['user_id'];
    $userLevel = $_SESSION['level'];
    $userName = $_SESSION['name'];
    if(isset($_POST['report_id']))
    {
        if($db->delete_report_byID($_POST['report_id']))
            echo "Report deleted from schedluer database.";
        else
            echo "Error while deleting report. Maybe you are not allowed to or you are not the owner of the report";
    }
}