<?php
session_start();
if(!isset($_SESSION['user_id']))
{
    session_destroy();
    header('Location: connect.php');
}
else
{
    /**
    * start program by including core.php and run include_core_files()
    */
    if(isset($_GET['id_strsys']))
    {
        $id_strsys = $_GET['id_strsys'];
        include "core.php";
        //including core files
        include_core_files();
        $db = new db_object();
        $userId = $_SESSION['user_id'];
        $userLevel = $_SESSION['level'];
        $userName = $_SESSION['name'];
        $types = $db->get_strsys_types();
        $all_systems = $db->load_all_strsys();
        $strsys = $db->load_strsys_byID($id_strsys);
        $entries_pools = $db->load_last_pools_entry($strsys);
        $pools = $db->get_system_pools_byID($strsys);
        if(isset($_GET['evolution']))
            $evolution = $_GET['evolution'];
        else
            $evolution = 1;
        if(isset($_GET['sorting']))
            $sorting = $_GET['sorting'];
        else
            $sorting = 2;
        if(isset($_GET['number_elem']))
            $number_elem = $_GET['number_elem'];
        else
            $number_elem = 10;
        if($strsys['id_type'] == 1)
        {
            $rgs = $db->get_system_rgs_byID($strsys);
            $entries_rgs = $db->load_last_rgs_entry($strsys);
        }
        $alerts = $db->get_system_alerts_byID($strsys);
        $current_page = "System ".$strsys['name'];
        $ping_test = ping($strsys['spa_ip']);
        $state_pools = get_pools_global_health($pools,$rgs,$entries_pools,$entries_rgs);
        $state = 1;
        if(!$ping_test)
        {
            $ping_test = ping($strsys['spb_ip']);
            if(!$ping_test)
            {
                $ping_test = ping($strsys['cs_ip']);
                if(!$ping_test)
                    $state = 0;
            }
        }
    }
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 5.0 Strict//EN">

<html lang="en">
    <?php main_head_gen($current_page); ?>
    <body class="bg-light">
        <!-- ******************** USER content **************************** -->
        <!-- ================= Add new system modal ======================= -->
        <?php
            if($userLevel > 0)
            {
                delete_system_mod_gen($id_strsys);
                download_report_mod($strsys);
            } 
         ?>
        <!-- ================= Help modal ================================= -->
        <?php
        help_mod_gen();
        global_graph_mod_gen();
        ?>
        <!-- ================ navbar beginning ============================ -->
        <?php main_navbar_gen($userName); ?>
        <!-- ================ navbar ending ============================ -->
        <!-- ================ wrapper beginning ======================== -->
        <div class="wrapper">
            <!-- ================ sidebar beginning ======================== -->
            <?php side_navbar_gen($all_systems); ?>
            <!-- ================ sidebar ending ======================== -->
            <!-- ================ content beginning ======================== -->
            <div id="content">
                <div class="d-sm-flex justify-content-between">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb no-bg">
                            <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="index.php">Systems</a></li>
                            <li class="breadcrumb-item" aria-current="page"><?php echo $strsys['name']; ?></li>
                        </ol>
                    </nav>
                    <?php if($userLevel > 0) { ?>
                    <div class="d-sm-flex justify-content-around">
                        <div class="p-2" style="position:relative;">
                            <a class="stretched-link nounderline" href="#" id="exportReport">
                                <span>Export Report</span>
                                <button class="btn btn-primary rounded-circle btn-sm">
                                    <i class="fa fa-file-text" aria-hidden="true"></i>
                                </button>
                            </a>
                        </div>
                        <div class="p-2" style="position:relative;">
                            <a class="stretched-link nounderline" href="#" data-toggle="modal" data-target="#deleteSystem">
                                <span>Delete System</span>
                                <button class="btn btn-primary rounded-circle btn-sm">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>
                            </a>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <!-- ============ System general display ================ -->
                <div class="container-fluid">
                    <div class="row">
                        <div class="card-deck px-3 col-sm-12" style="margin-left:0 !important;">
                            <?php 
                            strsys_card_gen_disp($strsys, $types, $state, $state_pools);?>
                            <div class="card col-sm-12 col-md-6" id="system-capacity">
                                <div class="container-fluid">
                                    <div class="row" >
                                        <h5 class="card-title col-sm-6 text-info pt-3">System Capacity</h5>
                                    </div>
                                </div>
                                <div class="card-body p-0" style="position: relative;">
                                    <div class="load-chart">
                                        <div id="Spinner" class="spinner-border text-primary" role="status">
                                            <span class="sr-only">Loading...</span>
                                        </div>
                                    </div>
                                    <canvas id="chartPie"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <?php
                    if(sizeof($alerts) != 0)
                    {
                    ?>
                    <div class="row">
                        <div class="card-deck px-3 col-sm-12" style="margin-left:0 !important;">
                            <div class="card col-sm-12" id="system-alerts">
                                <div class="container-fluid">
                                    <div class="row">
                                        <h5 class="card-title col-sm-6 text-info py-3">System Alerts</h5>
                                    </div>
                                </div>
                                <div class="card-body py-0">
                                    <?php alerts_table_gen($alerts);?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <?php
                    }
                    ?>
                    <div class="row">
                        <div class="card-deck px-3 col-sm-12" style="margin-left:0 !important;">
                            <div class="card col-sm-12" id="system-alerts">
                                <div class="container-fluid">
                                    <div class="row">
                                        <h5 class="card-title col-sm-6 text-info py-3">Storage Pool Status</h5>
                                    </div>
                                </div>
                                <div class="card-body py-0">
                                    <?php pools_table_gen($pools, $entries_pools,$strsys); ?>
                                </div>                        
                            </div>
                        </div>
                    </div>
                    <br>
                    <?php
                    if($strsys['id_type'] == 1)
                    {
                    ?>
                    <div class="row">
                        <div class="card-deck px-3 col-sm-12" style="margin-left:0 !important;">
                            <div class="card col-sm-12" id="system-alerts">
                                <div class="container-fluid">
                                    <div class="row">
                                        <h5 class="card-title col-sm-6 text-info py-3">Raid Groups Status</h5>
                                    </div>
                                </div>
                                <div class="card-body py-0">
                                    <?php rgs_table_gen($rgs, $entries_rgs,$strsys); ?>
                                </div>                        
                            </div>
                        </div>
                    </div>
                    <br>
                    <?php
                    }
                    ?>
                    <div class="row">
                        <div class="card-deck px-3 col-sm-12" style="margin-left:0 !important;">
                            <div class="card col-sm-12" id="system-alerts">
                                <div class="container-fluid">
                                    <div class="row">
                                        <h5 class="card-title col-sm-6 text-info py-3">LUNs Evolution</h5>
                                    </div>
                                    
                                </div>
                                <div class="card-body py-0">
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="evolution" class="font-weight-bold">Allocation Evolution Period:</label>
                                            <select id="evolution" class="form-control-sm ">
                                                <option <?php echo ($evolution == 1 ? "selected" : "");?> value="1">Daily Evolution</option>
                                                <option <?php echo ($evolution == 7 ? "selected": "");?> value="7">Weekly Evolution</option>
                                                <option <?php echo ($evolution == 30 ? "selected": "");?> value="30">Monthly Evolution</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-5 border-left">
                                            <label class="font-weight-bold">Order By:</label>
                                            <div class="form-group">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="sorting" id="consumption" value="0" <?php echo ($sorting == 0 ? "checked":"")?>>
                                                    <label class="form-check-label" for="consumption">Allocation (%)</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="sorting" id="consumption-evol" value="1" <?php echo ($sorting == 1 ? "checked":"")?>>
                                                    <label class="form-check-label" for="consumption-evol">Allocation Evolution</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="sorting" id="consumption-evol-gb" value="2" <?php echo ($sorting == 2 ? "checked":"")?>>
                                                    <label class="form-check-label" for="consumption-evol-gb">Allocation Evolution GB</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-3 border-left">
                                            <label for="number-elem" class="font-weight-bold">Number of element:</label><br>
                                            <select id="number-elem" class="form-control-sm ">
                                                <option <?php echo ($number_elem == 10 ? "selected" : "");?> value="10">10</option>
                                                <option <?php echo ($number_elem == 20 ? "selected": "");?> value="20">20</option>
                                                <option <?php echo ($number_elem == 30 ? "selected": "");?> value="30">30</option>
                                            </select>
                                        </div>
                                    </div>
                                    <?php if($strsys['id_type'] == 1)
                                            gen_pool_nav_tabs($strsys, $pools, $db,$rgs, $evolution,$sorting,$number_elem);
                                          else
                                            gen_pool_nav_tabs($strsys, $pools, $db, array(), $evolution,$sorting,$number_elem); ?>
                                </div>                        
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="card-deck px-3 col-sm-12" style="margin-left:0 !important;">
                            <div class="card col-sm-12" id="system-alerts">
                                <div class="container-fluid">
                                    <div class="row d-flex justify-content-between">
                <h5 class="card-title col-sm-6 text-info py-3">Storage Usage Evolution</h5> <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#showGlobalGraph" style="margin-top: 1rem; height: 35px;">
  Show global usage
</button> 
                                    </div>
                                </div>
                                <div class="card-body py-0">
                                    <canvas id="graph" width="98%"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
            <!-- ================ content ending ======================== -->
        </div>
        <!-- ================ wrapper ending ======================== -->
        <!-- ================ footer beginning ============================ -->
        <?php
        main_footer_gen();
        ?>
        <!-- ================ footer ending ============================ -->
        <!-- ******************** JQUERY LIBRARY ************************** -->
        <?php
        js_libraries_gen();
        generate_chart_pie($db, $strsys);
        generate_graph_pie($db, $strsys);
        generate_graph_global($db,$strsys);
        ?>
        <script>
            <?php echo 'var id_strsys = '.$id_strsys.';'; ?>
            function convertChartToImg(id)
            {
                var canvas = document.getElementById(id);
                var imgb64 = canvas.toDataURL("image/png");
                return imgb64;
            }
            $(document).ready(function () {
                $('.load-chart').remove();
            });
            $('#exportReport').on('click',function(){
                var chartPie = convertChartToImg('chartPie');
                var graph = convertChartToImg('graph');
                var graph_global = convertChartToImg('graph-global');
                var downloadReport = $('#downloadRep');
                var evolution = $('#evolution').val();
                var sorting = $('input[name="sorting"]:checked').val();
                var number_elem = $('#number-elem').val();
                downloadReport.modal('show');
                downloadReport.find('#downloadMessage').html('Please wait while the server is generating a PDF report...');
                $.ajax({
                    url: 'report_creator.php',
                    type: 'post',
                    data: {
                        'id_strsys' : id_strsys,
                        'chartPie' : chartPie,
                        'graph' : graph,
                        'graph-global': graph_global,
                        'evolution': evolution,
                        'sorting': sorting,
                        'number_elem':number_elem
                    },
                    success: function(data) {
                        downloadReport.find('#Spinner').hide();
                        downloadReport.find('#downloadButton').attr('disabled',false).attr('href','reports/'+data).removeClass('disabled');
                        downloadReport.find('#downloadMessage').html('Click the download button to download report in a PDF format.');
                    },
                    error: function() {
                        downloadReport.find('#Spinner').hide();
                        downloadReport.find('#downloadMessage').html('Failed to generate report. Please check with the bug report.');
                    }
                });
            });
            $('#downloadRep').on('hidden.bs.modal hide.bs.modal', function (e) {
                $(this).find('#Spinner').show();
                $(this).find('#downloadButton').attr('disabled',true).attr('href','#').addClass('disabled');
                $(this).find('#downloadMessage').html('');
            });
            $('#downloadButton').on('click',function() {
            });
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
            $('.confirm-delete-system').on('click',function() {
                var sys = $(this).attr('sys-to-del');
                $.ajax({
                    url: 'delete_strsys.php',
                    type: 'post',
                    data: {
                        'id_strsys' : sys
                    },
                    success: function(data) {
                        window.location.href("index.php");
                    },
                    error: function() {
                        $('#deleteSystem').find('.modal-message').html('Failed to delete Storage system from database. Check with software provider support.');
                    }
                });
            });
            $('#evolution').on('change',function() {
                var evolution = $(this).val();
                var sorting = $('input[name="sorting"]:checked').val();
                var number_elem = $('#number-elem').val();
                var url = "&evolution="+evolution+"&sorting="+sorting+"&number_elem="+number_elem;
                var url_full = String(window.location.href).replace(/#/,"");
                var index = (url_full.search('&evolution') == -1 ? url_full.length : url_full.search('&evolution'));
                url_full = url_full.substring(0,index);
                window.location.href = url_full+url;

            });
            $('input[name="sorting"]').on('change',function() {
                var evolution = $('#evolution').val();
                var sorting = $(this).val();
                var number_elem = $('#number-elem').val();
                var url = "&evolution="+evolution+"&sorting="+sorting+"&number_elem="+number_elem;
                var url_full = String(window.location.href).replace(/#/,"");
                var index = (url_full.search('&evolution') == -1 ? url_full.length : url_full.search('&evolution'));
                url_full = url_full.substring(0,index);
                window.location.href = url_full+url;

            });
            $('#number-elem').on('change',function() {
                var evolution = $('#evolution').val();
                var sorting = $('input[name="sorting"]:checked').val();
                var number_elem = $(this).val();
                var url = "&evolution="+evolution+"&sorting="+sorting+"&number_elem="+number_elem;
                var url_full = String(window.location.href).replace(/#/,"");
                var index = (url_full.search('&evolution') == -1 ? url_full.length : url_full.search('&evolution'));
                url_full = url_full.substring(0,index);
                window.location.href = url_full+url;
            });
        </script>
    </body>
</html>