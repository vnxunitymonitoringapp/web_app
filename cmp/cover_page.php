<?php
switch($freq)
{
    case 2:
        $freq_txt = "Daily Report";
        break;
    case 1:
        $freq_txt = "Weekly Report";
        break;
    case 0:
        $freq_txt = "Monthly Report";
        break;
    default:
        $freq_txt = "";
}
?>
<div class="main-page">
    <br>
    <br>
    <br>
    <br>
    <br>
    <img src="img/logo.png" class="cover-logo">
    <br>
    <br>
    <br>
    <br>
    <h1>Automatic report for<br>Storage Array Monitoring</h1>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    
    <img src="img/Logo_client.png" class="client-logo" height="100px">
    <br>
    <br>
    <br>
    <br>
    <br>
    <h1>Storage Array</h1>
    <h2><?php echo $strsys_name; ?></h2>
    <br>
    <br>
    <br>
    <table class="cover-tab">
        <tr>
            <td>Creation Date:</td>
            <td><?php echo $date; ?></td>
        </tr>
        <tr>
            <td colspan = "2"><?php echo $freq_txt; ?></td>
        </tr>
    </table>
    <br>
    
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    
    <p class="copyright">Copyright &copy; <?php echo date("Y"); ?> Focus Technology Solutions<br><br>All Rights Reserved</p>
</div>