<nav id="sidebar" class="bg-dark">
    <div class="list-group" style="min-height:400px;max-height:400px;">
        <a href="index.php" class="row list-group-item list-group-item-action wider text-light bg-transparent pr-0 no-border no-border-radius active">
            <i class="col-1 fa fa-cubes ml-0" aria-hidden="true"></i>
            <span class="col-11 larger-text">
                Dashboard
            </span>  
        </a>
        <a href="reports.php" class="row list-group-item list-group-item-action wider text-light bg-transparent pr-0 no-border no-border-radius">
            <i class="col-1 fa fa-files-o ml-0" aria-hidden="true"></i>
            <span class="col-11 larger-text">
                Reports
            </span>  
        </a>
        <a href="#systemsSubmenu" class="row list-group-item list-group-item-action wider text-light bg-transparent pr-0 no-border no-border-radius dropdown-toggle" aria-expanded="false" data-toggle="collapse">
            <i class="col-1 fa fa-server ml-0" aria-hidden="true"></i>
            <span class="col-11 larger-text">
                Systems
            </span>
        </a>
        <div class="list-group collapse" id="systemsSubmenu">
        <?php
        if(sizeof($strsys) > 0)
        {
            foreach($strsys as $system)
            {
                echo "<a href=\"system_display.php?id_strsys=".$system['id_strsys']."\" class=\"row list-group-item list-group-item-action wider text-light bg-transparent pr-0 no-border no-border-radius\">";
                echo "  <i class=\"col-1 fa fa-hdd-o ml-2\" aria-hidden=\"true\"></i>";
                echo "<span class=\"col-11\">";
                echo $system['name'];
                echo "</span></a>";
            }
        }
        ?>
        </div>
    </div>
    <div class="list-group border-top">
        <a href="settings.php" class="row list-group-item list-group-item-action wider text-light bg-transparent pr-0 no-border no-border-radius">
            <i class="col-1 fa fa-cog ml-0" aria-hidden="true"></i>
            <span class="col-11 larger-text">
                Settings
            </span>  
        </a>
        <a href="#" class="row list-group-item list-group-item-action wider text-light bg-transparent pr-0 no-border no-border-radius" data-toggle="modal" data-target="#helpModal">
            <i class="col-1 fa fa-question-circle ml-0" aria-hidden="true"></i>
            <span class="col-11 larger-text">
                Help
            </span>  
        </a>
    </div>
</nav>