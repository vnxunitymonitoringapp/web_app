<?php
$pools = $db->load_last_pools_entry($strsys);
$rgs = $db->load_last_rgs_entry($strsys);
$used = 0;
$free = 0;
$used_tot = 0;
$free_tot = 0;
$fraction = sizeof($pools) + sizeof($rgs);
foreach($pools as $pool)
{
    $used_tot += $pool['used_cap_gb'];
    $free_tot += $pool['free_cap_gb'];
    $used += ($pool['used_cap_gb'] * 100)/$pool['total_cap_gb'];
    $free += ($pool['free_cap_gb'] * 100)/$pool['total_cap_gb'];
}
foreach($rgs as $rg)
{
    $used_tot += $rg['user_cap_gb'];
    $free_tot += $rg['free_cap_gb'];
    $used += (($rg['user_cap_gb'] - $rg['free_cap_gb']) * 100)/$rg['user_cap_gb'];
    $free += ($rg['free_cap_gb'] * 100)/$rg['user_cap_gb'];
}
$used = number_format((float)$used / $fraction, 2,'.','');
$free = number_format((float)$free / $fraction, 2,'.','');
echo "<script type=\"text/javascript\" src=\"js/Chart.js\"></script>
<script>
    var chartPie = document.getElementById(\"chartPie\");
    var chDonutData1 = {
        labels: [ 'Free Capacity $free_tot GB ($free%)','Used capacity $used_tot GB ($used%)'],
        datasets: [
        {
            backgroundColor: [\"#32ff7d\",\"#007db8\"],
            borderWidth: 0,
            data: [".$free.",".$used."]
        }
        ]
    };
    var donutOptions = {
        responsive: true,
        cutoutPercentage: 20, 
        legend: {position:'bottom', 
            labels:{pointStyle:'rectRounded',
                usePointStyle:true,
                fontSize: 13
            },
            onClick : function() {}
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    var label = data.datasets[0].data[tooltipItem.index];
                    return label+\"%\";
                }
            },
            bodyFontSize: 18
        }
    };
    if (chartPie) {
        new Chart(chartPie, {
            type: 'pie',
            data: chDonutData1,
            options: donutOptions
        });
    }
</script>";