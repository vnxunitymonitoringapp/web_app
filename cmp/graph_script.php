<?php
include_once "core/misc_functions.php";
$max_disp = 30;
$pools = $db->get_system_pools_byID($strsys);
$rgs = $db->get_system_rgs_byID($strsys);
$dates = $db->get_dates($strsys);
while(sizeof($dates) > $max_disp)
{
    array_shift($dates);
}
echo "
<script>
    function convertGraphToImg()
    {
        var canvas = document.getElementById('graph');
        var img = new Image();
        img.src = canvas.toDataURL(\"image/png\");
        canvas.insertAdjacentElement(\"afterend\",img);
        canvas.parentNode.removeChild(canvas);
    }
    var ctx = document.getElementById('graph').getContext('2d');
    var chart = new Chart(ctx, { type: 'line',data: { labels: [";
    $i = 0;
    
    foreach($dates as $date)
    {
        echo "\"".$date['day']."\"";
        if($i < sizeof($dates))
            echo ", ";
        $i++;
    }
    echo "], datasets: [";
    foreach($pools as $pool)
    {
        $color = generate_random_color();
        echo "{label: \"".$pool['name']."\",";
        echo "borderColor: \"".$color."\",";
        echo "backgroundColor: \"".$color."\",";
        echo "pointBackgroundColor : \"".$color."\",";
        echo "data : [";
        $dt = $db->load_pool_entries($pool['id_pool'],$strsys);
        $i = 0;
        for($j = 0; $j < sizeof($dates) - sizeof($dt); $j++)
        {
            echo "0, ";
        }
        while(sizeof($dt) > $max_disp)
        {
            array_shift($dt);
        }
        foreach($dt as $data)
        {
            echo number_format((float)$data['AVG(used_cap_gb)'], 3,'.','');
            if($i < sizeof($dt))
                echo ", ";
            $i++;
        }
        echo "],fill:false},";
    }
    $j = 0;
    if($strsys['id_type'] == 1)
    {
        foreach($rgs as $rg)
        {
            $color = generate_random_color();
            echo "{label: \"RG ".$rg['id_rg']."\",";
            echo "borderColor: \"".$color."\",";
            echo "backgroundColor: \"".$color."\",";
            echo "pointBackgroundColor : \"".$color."\",";
            echo "data : [";
            $dt = $db->load_rg_entries($rg['id_rg'], $strsys);
            $i = 0;
            for($j = 0; $j < sizeof($dates) - sizeof($dt); $j++)
            {
                echo "0, ";
            }
            while(sizeof($dt) > $max_disp)
            {
                array_shift($dt);
            }
            foreach($dt as $data)
            {
                $used = $data['AVG(user_cap_gb)'] - $data['AVG(free_cap_gb)'];
                echo number_format((float)$used, 3,'.','');
                if($i < sizeof($dt))
                    echo ", ";
                $i++;
            }
            echo "],fill:false}";
            if($j < sizeof($rgs))
                echo ", ";
            $j++;
        }
    }
    echo "]}, options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    } });";
echo "</script>";