<?php
include_once "core/misc_functions.php";
$max_disp = 30;
$pools = $db->get_system_pools_byID($strsys);
$rgs = $db->get_system_rgs_byID($strsys);
$dates = $db->get_dates($strsys);
$ts_day = 86400;
$weeks_to_predict = 5;
while(sizeof($dates) > $max_disp)
{
    array_shift($dates);
}
$date_numbers = array();
$date_ref = strtotime($dates[0]['day']);
for($j = 0; $j < sizeof($dates); $j++)
{
    $date_numbers[$j] = strtotime($dates[$j]['day']) - $date_ref;
}
$glob_data = array();
$glob_data = array_fill(0, sizeof($dates), 0);
foreach($pools as $pool)
{
    
    $dt = $db->load_pool_entries($pool['id_pool'],$strsys);
    $j = 0;
    for($j = 0; $j < sizeof($dates) - sizeof($dt); $j++)
    {
        $glob_data[$j] += 0;
    }
    while(sizeof($dt) > $max_disp)
    {
        array_shift($dt);
    }
    foreach($dt as $data)
    {
        $glob_data[$j] += number_format((float)$data['AVG(used_cap_gb)'], 3,'.','');
        $j++;
    }
}
if($strsys['id_type'] == 1)
{
    foreach($rgs as $rg)
    {
        $dt = $db->load_rg_entries($rg['id_rg'], $strsys);
        $j = 0;
        for($j = 0; $j < sizeof($dates) - sizeof($dt); $j++)
        {
            $glob_data[$j] += 0;
        }
        while(sizeof($dt) > $max_disp)
        {
            array_shift($dt);
        }
        foreach($dt as $data)
        {
            $used = $data['AVG(user_cap_gb)'] - $data['AVG(free_cap_gb)'];
            $glob_data[$j] += number_format((float)$used, 3,'.','');
            $j++;
        }
    }
}
for($j = 0; $j < sizeof($glob_data); $j++)
{
    $glob_data[$j] = $glob_data[$j] / (sizeof($pools) + sizeof($rgs));
}
$date_to_predict = array();
$added_labels = array();
for($j = 0; $j < $weeks_to_predict; $j++)
{
    $date_to_predict[$j] = $date_numbers[sizeof($date_numbers)-1] + ($ts_day * 7) * ($j+1);
    $added_labels[$j]['day'] = date("Y-m-d",$date_to_predict[$j] + $date_ref);
}
$reduce = sizeof($dates) - ($max_disp - $weeks_to_predict);
for($j = 0; $j < $reduce; $j++)
{
    array_shift($dates);
}
$dates = array_merge($dates, $added_labels);
echo "
<script>
    var learning_rate = 0.0001
    var b = tf.variable(tf.scalar(Math.random()));
    var m = tf.variable(tf.scalar(Math.random()));
    var date_to_predict = [";
    for($j = 0; $j < sizeof($date_to_predict); $j++)
    {
        echo $date_to_predict[$j];
        if($j < sizeof($date_to_predict)-1)
            echo ", ";
    }
    echo "];";
    echo "var x_array = [";
    $j = 0;
    foreach($date_numbers as $d_n)
    {
        echo $d_n;
        if($j < sizeof($date_numbers) - 1)
            echo ", ";
        $j++;
    }
    echo "];";
    echo "var y_array = [";
    $j = 0;
    foreach($glob_data as $g_d)
    {
        echo $g_d;
        if($j < sizeof($glob_data) - 1)
            echo ", ";
        $j++;
    }
    echo "];";
echo "
    function predict(n) {     
        return tf.tidy(function() {
            return m.mul(n).add(b);
        });
    }
    function loss(prediction,actualValues) {
        const error = prediction.sub(actualValues).square().mean();
        return error;
    }
    const predictionsBefore = predict(tf.tensor1d(x_array));
    const optimizer = tf.train.sgd(learning_rate);
    for(var i = 0; i < 100; i++)
    {
        optimizer.minimize(function()  {
            const predsYs = predict(tf.tensor1d(x_array));
            stepLoss = loss(predsYs, tf.tensor1d(y_array))
            console.log(stepLoss.dataSync()[0])
            return stepLoss;
        });
    }
    var predicted = new Array();
    var i = 0;
    var result = 0;
    for(i = 0; i < date_to_predict.length; i++)
    {
        result = date_to_predict[i] * m.dataSync()[0] + b.dataSync()[0];
        predicted.push(result);
        
    }
    var data_to_use = [";
    for($j = 0; $j < $reduce; $j++)
    {
        array_shift($glob_data);
    }
    for($j = 0; $j < sizeof($glob_data); $j++)
    {
        echo $glob_data[$j];
        if($j < sizeof($glob_data)-1)
            echo ", ";
    }
    echo "
    ];
    var merge = data_to_use.concat(predicted);
    var ctx_global = document.getElementById('graph-global').getContext('2d');
    var chart_global = new Chart(ctx_global, { type: 'line',data: { labels: [";
    $i = 0;
    foreach($dates as $date)
    {
        echo "\"".$date['day']."\"";
        if($i < sizeof($dates)-1)
            echo ", ";
        $i++;
    }
    echo "], datasets: [";
    $color = "#007db8";
    echo "{label: \"Global storage usage\",";
    echo "borderColor: \"".$color."\",";
    echo "backgroundColor: \"".$color."\",";
    echo "pointBackgroundColor : \"".$color."\",";
    echo "data : merge";
    
    echo ",fill:false}";
    echo "]}, options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    } });";
echo "</script>";