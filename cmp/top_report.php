<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Strict//EN">

<html lang="en">
    <head>
        <title>Report <?php echo $date; ?></title>
        <meta name="description" content="This page contain the report">
        <meta charset="utf-8">
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- *****************************  User CSS Sheets ************************* -->
        <link rel="stylesheet" href="css/reporting_css/main.css">
        <link rel="stylesheet" href="css/reporting_css/font-awesome.min.css">
    </head>

    <!-- Body part -->
    <body>