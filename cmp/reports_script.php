<script>
$('.delete-rep').on('click',function() {
    var file = $(this).attr('id');
    $('#deleteFile').modal('show');
    $('#deleteFile').find('.confirm-delete-file').attr('file-to-del',file);
});
$('#deleteFile').on('hide.bs.modal hidden.bs.modal', function() {
    $(this).find('.confirm-delete-file').attr('file-to-del','');
});
$('.confirm-delete-file').on('click',function() {
    var myfile = $(this).attr('file-to-del');
    $.ajax({
        url: 'delete_file.php',
        method: 'post',
        data: {
            'file-to-delete': myfile
        },
        success: function(data) {
            window.location.reload(true);
        },
        error: function(data) {
            $('#deleteFile').find('.modal-body').innerHtml('').innerHtml(data);
        }
    });
});
</script>