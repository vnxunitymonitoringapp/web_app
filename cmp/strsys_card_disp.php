<?php
include_once "core/misc_functions.php";
echo "<div class=\"card col-sm-12 col-md-6\" id=\"system-information\">"; 
$type_key = array_search($system['id_type'], array_column($types,'id_type'));
if($state == 0)
{
    $tooltip = "System Unreachable";
    $state_icon = "<i class=\"fa fa-chain-broken fa-lg text-secondary\" aria-hidden=\"true\"></i>";
    $reachable = "";
    $color = "text-secondary";
    $off = "off";
}
else
{
    $tooltip = "System Reachable";
    $state_icon = "<i class=\"fa fa-link fa-lg text-success\" aria-hidden=\"true\"></i>";
    $reachable = "reachable";
    $color = "";
    $off =  "";
}
?>
<div class="container-fluid">
    <div class="row">
        <h5 class="card-title col-sm-6 text-info py-3">System Information</h5>
        <?php echo "<div class=\"card-img-top ".$types[$type_key]['name']." $off custom-card-img smaller col-sm-6\" alt=\"".strtoupper($types[$type_key]['name'])." System Pic\">
            <div class=\"connection-state $reachable small\"  data-toggle=\"tooltip\" data-placement=\"top\" title=\"$tooltip\" >
                $state_icon
            </div>
        </div>"; ?>
    </div>
</div>
<div class="card-body">
        <form>
            <div class="form-group no-mb row">
                <label for="systemName-c" class="col-sm-5 col-form-label py-0 font-weight-bold text-right <?php echo $color; ?>">Name: </label>
                <div class="col-sm-7">
                    <?php echo "<input type=\"text\" readonly class=\"form-control-plaintext py-0 $color\" id=\"systemName-c\" value=\"".$system['name']."\">"; ?>
                </div>
            </div>
            <div class="form-group no-mb row">
                <label for="siteLocation-c" class="col-sm-5 col-form-label py-0 font-weight-bold text-right <?php echo $color; ?>">Site Location: </label>
                <div class="col-sm-7">
                    <?php echo "<input type=\"text\" readonly class=\"form-control-plaintext py-0 $color\" id=\"siteLocation-c\" value=\"".$system['site_location']."\">"; ?>
                </div>
            </div>
            <div class="form-group no-mb row">
                <label for="model-c" class="col-sm-5 col-form-label py-0 font-weight-bold text-right <?php echo $color; ?>">Model: </label>
                <div class="col-sm-7">
                    <?php echo "<input type=\"text\" readonly class=\"form-control-plaintext py-0 $color\" id=\"model-c\" value=\"".$system['model']."\">"; ?>
                    
                </div>
            </div>
            <div class="form-group no-mb row">
                <label for="serialNumber-c" class="col-sm-5 col-form-label py-0 font-weight-bold text-right <?php echo $color; ?>">Serial Number: </label>
                <div class="col-sm-7">
                    <?php echo "<input type=\"text\" readonly class=\"form-control-plaintext py-0 $color\" id=\"serialNumber-c\" value=\"".$system['serial_number']."\">"; ?>
                </div>
            </div>
<?php if($types[$type_key]['name'] == "vnx") { ?>
            <div class="form-group no-mb row">
                <label for="spaIpAddress-c" class="col-sm-5 col-form-label py-0 font-weight-bold text-right <?php echo $color; ?>">SPA IP Adress: </label>
                <div class="col-sm-7">
                    <?php echo "<input type=\"text\" readonly class=\"form-control-plaintext py-0 $color\" id=\"spaIpAddress-c\" value=\"".$system['spa_ip']."\">"; ?>
                </div>
            </div>
            <div class="form-group no-mb row">
                <label for="spbIpAddress-c" class="col-sm-5 col-form-label py-0 font-weight-bold text-right <?php echo $color; ?>">SPB IP Adress: </label>
                <div class="col-sm-7">
                    <?php echo "<input type=\"text\" readonly class=\"form-control-plaintext py-0 $color\" id=\"spbIpAddress-c\" value=\"".$system['spb_ip']."\">"; ?>
                </div>
            </div>
<?php }else if($types[$type_key]['name'] == "unity") { ?>
    <div class="form-group no-mb row">
                <label for="spaIpAddress-c" class="col-sm-5 col-form-label py-0 font-weight-bold text-right <?php echo $color; ?>">Management Adress: </label>
                <div class="col-sm-7">
                    <?php echo "<input type=\"text\" readonly class=\"form-control-plaintext py-0 $color\" id=\"spaIpAddress-c\" value=\"".$system['spa_ip']."\">"; ?>
                </div>
            </div>
<?php } ?>
            <div class="form-group no-mb row">
                <label for="firmwareVersion-c" class="col-sm-5 col-form-label py-0 font-weight-bold text-right <?php echo $color; ?>">Firmware Version: </label>
                <div class="col-sm-7">
                    <?php echo "<input type=\"text\" readonly class=\"form-control-plaintext py-0 $color\" id=\"firmwareVersion-c\" value=\"".$system['firmware_ver']."\">"; ?>
                </div>
            </div>
            <div class="form-group no-mb row">
                <label for="systemHealth-c" class="col-sm-5 col-form-label py-0 font-weight-bold text-right <?php echo $color; ?>">System Hardware Health: </label>
                <div class="col-sm-7">
                    <?php system_health($system['system_health'], $state); ?>
                </div>
            </div>
            <div class="form-group no-mb row" id="health">
                <label for="systemPoolsHealth-c" class="col-sm-5 col-form-label py-0 font-weight-bold text-right <?php echo $color; ?>">System Storage Health: </label>
                <div class="col-sm-7">
                    <?php system_health($state_pools, $state); ?>
                </div>
            </div>
        </form>
    </div>
</div>