<div class="modal fade" id="addNewSystemModal" tabindex="-1" role="dialog" aria-labelledby="addNewSystemModal" aria-hidden="true">
            <div class="modal-dialog" role="addNewSystem">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add a New Storage System</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group row">
                                <label for="systemName" class="col-sm-4 col-form-label text-right"><span class="font-weight-bold">System Name:</span></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="systemName">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="siteLocation" class="col-sm-4 col-form-label text-right"><span class="font-weight-bold">Site Location:</span></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="siteLocation">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="systemType" class="col-sm-4 col-form-label text-right"><span class="font-weight-bold">System Type</span></label>
                                <div class="col-sm-6">
                                    <select id="systemType" class="form-control">
                                    <?php
                                    if(sizeof($types) > 0)
                                    {
                                        $i = 0;
                                        foreach($types as $type)
                                        {
                                            if($i == 0)
                                                echo "<option value=\"".$type['id_type']."\" selected>".strtoupper($type['name'])."</option>";
                                            else
                                                echo "<option value=\"".$type['id_type']."\">".strtoupper($type['name'])."</option>";
                                            $i++;
                                        }
                                    }
                                    else
                                    {
                                        echo "<option value=\"-1\" selected>NO TYPE SUPPORTED</option>";
                                    }
                                    ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="spaIpAddress" class="col-sm-4 col-form-label text-right"><span class="font-weight-bold" id="toChange" >SPA IP Adress:</span></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="spaIpAddress">
                                </div>
                            </div>
                            <div class="form-group row" id="toHide">
                                <label for="spbIpAddress" class="col-sm-4 col-form-label text-right"><span class="font-weight-bold">SPB IP Adress:</span></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="spbIpAddress">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="userName" class="col-sm-4 col-form-label text-right"><span class="font-weight-bold">User Name:</span></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="userName">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="userPassword" class="col-sm-4 col-form-label text-right"><span class="font-weight-bold">Password:</span></label>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control" id="userPassword">
                                </div>
                            </div>
                        </form>
                        <div class="mx-auto" style="width:34px;">
                            <div id="Spinner-add" class="d-none mx-auto spinner-border text-primary" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                        <p id="addMessage"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="addStrsys">Add Storage System</button>
                    </div>
                </div>
            </div>
          </div>
