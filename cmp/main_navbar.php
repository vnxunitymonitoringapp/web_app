<nav class="navbar navbar-expand-lg navbar-dark bg-dark bg-dark-custom">
            <div class="navbar-brand pr-lg-5 pl-lg-3 pr-sm-3 border-right">
                <img class="navbar-logo" src="img/logo.png" width="154" height="38" />
            </div>
            <h4 class="navbar-text text-light font-weight-light">
                FTS Storage Monitoring
            </h4>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item py-2">
                    <button type="button" class="btn btn-info rounded-circle btn-sm bg-gray" style="width:39px !important;" data-toggle="modal" data-target="#helpModal">
                        <i class="fa fa-question fa-2x" aria-hidden="true"></i>
                    </button>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <button type="button" class="btn btn-info rounded-circle btn-sm mr-1 bg-gray">
                            <i class="fa fa-user fa-2x" aria-hidden="true"></i>
                        </button>
                        Hi, <?php echo ucfirst($userName); ?>
                    </a> 
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="settings.php"><i class="fa fa-cog" aria-hidden="true"></i> Profile Settings</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="disconnect.php"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>
                    </div>
                </li>
            </ul>
        </nav>