<div class="modal fade" id="showGlobalGraph" tabindex="-1" role="dialog" aria-labelledby="showGlobalGraph" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xlg" role="showGlobalGraph">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Global storage usage</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <canvas id="graph-global" width="98%"></canvas>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>