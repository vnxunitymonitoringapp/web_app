<script>
    $('#addNewSystemModal').on('hidden.bs.modal hide.bs.modal', function (e) {
        $(this).find('#Spinner-add').addClass('d-none');
        $(this).find('#addMessage').html('');
	    window.location.reload(true);
    });
    $('#systemType').on('change',function() {
        var selected = $(this).val();
        if(selected == "2")
        {
            $('#toHide').addClass('d-none');
            $('#toChange').html("Management IP:");
        }
        else if(selected == "1")
        {
            $('#toHide').removeClass('d-none');
            $('#toChange').html("SPA IP Adress:");
        }
    });
    $('#addStrsys').on('click',function(){
        var spinner = $('#Spinner-add');
        var selected = $('#systemType').val();
        if(selected == "1")
        {
            var strsys = {
                'systemName' : $('#systemName').val(),
                'siteLocation': $('#siteLocation').val(),
                'systemType' : $('#systemType').val(),
                'spaIpAddress' : $('#spaIpAddress').val(),
                'spbIpAddress' : $('#spbIpAddress').val(),
                'userName' : $('#userName').val(),
                'userPassword' : $('#userPassword').val()
            };
        }
        else if(selected == "2")
        {
            var strsys = {
                'systemName' : $('#systemName').val(),
                'siteLocation': $('#siteLocation').val(),
                'systemType' : $('#systemType').val(),
                'spaIpAddress' : $('#spaIpAddress').val(),
                'spbIpAddress' : $('#spaIpAddress').val(),
                'userName' : $('#userName').val(),
                'userPassword' : $('#userPassword').val()
            };
        }
        var message = $('#addMessage');
        message.html('');
        spinner.removeClass('d-none');
        $.ajax({
            url: 'add_strsys.php',
            type: 'post',
            data: strsys,
            success: function(data) {
                spinner.addClass('d-none');
                message.html(data);
            }
        });
    });
</script>
