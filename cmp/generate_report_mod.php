<div class="modal fade" tabindex="-1" role="dialog" id="add-report">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form id="report">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Report Settings</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="form-group">
                            <input type="text" maxlength="20" class="form-control" id="reportName" name="reportName" required placeholder="Report Name">
                        </div>
                        <div class="form-group">
                        <?php
                        systems_checkbox($strsys); 
                        ?>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="emails" name="emails" required placeholder="Notification emails (Separated by ,)">
                        </div>
                        <div class="form-group">
                            <select class="custom-select form-control" id="frequency" name="frequency" required>
                                <option value="0" selected>Every Month</option>
                                <option value="1">Every Week</option>
                                <option value="2">Every Day</option>
                            </select>
                        </div>
                        <div class="form-group row">
                            <label class="font-weight-bold col-sm-5">Lun Ordered By:</label>
                            <div class="form-group col-sm-7">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="sorting" id="consumption" value="0" checked >
                                    <label class="form-check-label" for="consumption">Allocation (%)</label>
                                </div><br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="sorting" id="consumption-evol" value="1">
                                    <label class="form-check-label" for="consumption-evol">Allocation Evolution</label>
                                </div><br>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="sorting" id="consumption-evol-gb" value="2">
                                    <label class="form-check-label" for="consumption-evol-gb">Allocation Evolution GB</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="number-elem" class="font-weight-bold">Number of Lun displayed:</label>
                            <select id="number-elem" class="form-control-sm ">
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="addReport" class="btn btn-primary">Add Report</button>
                </div>
            </div>
        </form>
    </div>
</div>