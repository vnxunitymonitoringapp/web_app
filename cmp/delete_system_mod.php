<div class="modal fade" id="deleteSystem" tabindex="-1" role="dialog" aria-labelledby="deleteSystem" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Confirm delete</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p class="modal-message">Are you sure you want to delete this System?</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-primary confirm-delete-system" sys-to-del ="<?php echo $id_strsys; ?>">Confirm</button>
            </div>
          </div>
        </div>
      </div>
    