<head>
        <title>FTS Storage Monitoring - <?php echo $current_page; ?></title>
        <meta name="description" content="Main dashboard page ">
        <meta charset="utf-8">
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=1.0, user-scalable=no">
        <link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png">
        <!-- ******************** BOOTSTRAP CSS SHEETS ******************** -->
        <link rel="stylesheet" href="css/bootstrap.css">
        <!-- ******************** FONTAWESOME CSS SHEETS ****************** -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <!-- ******************** USER CSS SHEETS ************************* -->
        <link rel="stylesheet" href="css/main.css">
        
    </head>