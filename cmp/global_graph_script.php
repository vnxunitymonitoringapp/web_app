<?php
include_once "core/misc_functions.php";
$max_disp = 30;
$pools = $db->get_system_pools_byID($strsys);
$rgs = $db->get_system_rgs_byID($strsys);
$dates = $db->get_dates($strsys);
while(sizeof($dates) > $max_disp)
{
    array_shift($dates);
}
$glob_data = array();
$glob_data = array_fill(0, sizeof($dates), 0);
foreach($pools as $pool)
{
    
    $dt = $db->load_pool_entries($pool['id_pool'],$strsys);
    $j = 0;
    for($j = 0; $j < sizeof($dates) - sizeof($dt); $j++)
    {
        $glob_data[$j] += 0;
    }
    while(sizeof($dt) > $max_disp)
    {
        array_shift($dt);
    }
    foreach($dt as $data)
    {
        $glob_data[$j] += number_format((float)$data['AVG(used_cap_gb)'], 3,'.','');
        $j++;
    }
}
if($strsys['id_type'] == 1)
{
    foreach($rgs as $rg)
    {
        $dt = $db->load_rg_entries($rg['id_rg'], $strsys);
        $j = 0;
        for($j = 0; $j < sizeof($dates) - sizeof($dt); $j++)
        {
            $glob_data[$j] += 0;
        }
        while(sizeof($dt) > $max_disp)
        {
            array_shift($dt);
        }
        foreach($dt as $data)
        {
            $used = $data['AVG(user_cap_gb)'] - $data['AVG(free_cap_gb)'];
            $glob_data[$j] += number_format((float)$used, 3,'.','');
            $j++;
        }
    }
}
for($j = 0; $j < sizeof($glob_data); $j++)
{
    $glob_data[$j] = $glob_data[$j] / (sizeof($pools) + sizeof($rgs));
}
echo "
<script>
    
    var data_to_use = [";
    for($j = 0; $j < sizeof($glob_data); $j++)
    {
        echo $glob_data[$j];
        if($j < sizeof($glob_data)-1)
            echo ", ";
    }
    echo "
    ];
    var ctx_global = document.getElementById('graph-global').getContext('2d');
    var chart_global = new Chart(ctx_global, { type: 'line',data: { labels: [";
    $i = 0;
    foreach($dates as $date)
    {
        echo "\"".$date['day']."\"";
        if($i < sizeof($dates)-1)
            echo ", ";
        $i++;
    }
    echo "], datasets: [";
    $color = "#007db8";
    echo "{label: \"Global storage usage\",";
    echo "borderColor: \"".$color."\",";
    echo "backgroundColor: \"".$color."\",";
    echo "pointBackgroundColor : \"".$color."\",";
    echo "data : data_to_use";
    
    echo ",fill:false}";
    echo "]}, options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    } });";
echo "</script>";