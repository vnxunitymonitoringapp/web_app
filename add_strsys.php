<?php
session_start();
if(!isset($_SESSION['user_id']))
{
    session_destroy();
    header('Location: connect.php');
}
else
{
    /**
    * start program by including core.php and run include_core_files()
    */
    include "core.php";
    //including core files
    include_core_files();
    $db = new db_object();
    $userId = $_SESSION['user_id'];
    $userLevel = $_SESSION['level'];
    $userName = $_SESSION['name'];
    if(isset($_POST['systemName']) && isset($_POST['siteLocation']) && isset($_POST['systemType']) && isset($_POST['spaIpAddress']) && isset($_POST['spbIpAddress']) && isset($_POST['userName']) && isset($_POST['spbIpAddress']) && isset($_POST['userName']) && isset($_POST['userPassword']))
    {
        $strsys = array(
            'id_type' => $_POST['systemType'],
            'name' => $_POST['systemName'],
            'site_location' => $_POST['siteLocation'],
            'serial_number' => "",
            'model' => "",
            'spa_ip' => $_POST['spaIpAddress'],
            'spb_ip' => $_POST['spbIpAddress'],
            'cs_ip' => $_POST['spaIpAddress'],
            'firmware_ver' => "",
            'system_health' => 0,
            'user' => $_POST['userName'],
            'passwd' => $_POST['userPassword']
        );
        $db->add_strsys($strsys);
        $output = shell_exec("/usr/bin/python /var/www/html/fts_mon_app_db/data_harvester/add_one_system.py");
    }
    else
    {
        echo "<span class=\"text-danger\">Please fullfil all the fields.</span>";
    }
}
?>