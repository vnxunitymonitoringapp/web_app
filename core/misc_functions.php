<?php
function system_health($level, $state)
{
    if($state == 0)
    {
        $color = "text-secondary";
        $success = "";
        $warn = "";
        $dan = "";
        
    }
    else
    {
        $color = "";
        $success = "text-success";
        $warn = "text-warning";
        $dan = "text-danger";
    }
    $good = "<i class=\"fa fa-check-circle-o $success fa-2x align-middle systemHealth-c $color\" id=\"\" aria-hidden=\"true\"></i>";
    $warning = "<i class=\"fa fa-exclamation-triangle $warn fa-2x align-middle systemHealth-c $color\" id=\"\" aria-hidden=\"true\"></i>";
    $critical = "<i class=\"fa fa-times-circle $dan fa-2x align-middle systemHealth-c $color\" id=\"\" aria-hidden=\"true\"></i>";
    switch($level)
    {
        case 0:
            echo $good;
            break;
        case 1:
            echo $warning;
            break;
        case 2:
            echo $critical;
            break;
    }
}
function alert_level($level)
{
    $warning = "<i class=\"fa fa-exclamation-triangle fa-2x align-bottom\" aria-hidden=\"true\"></i>";
    $info = "<i class=\"fa fa-info-circle fa-2x align-bottom\" aria-hidden=\"true\"></i>";
    $danger = "<i class=\"fa fa-times-circle fa-2x align-bottom\" aria-hidden=\"true\"></i>";
    switch($level)
    {
        case 0:
            echo $info;
            break;
        case 1:
            echo $warning;
            break;
        case 2:
            echo $critical;
            break;
    }
}
function users_list($users)
{
    $i = 0;
    $active = "";
    echo "<div class=\"list-group\" id=\"users-list\">";
    foreach($users as $user)
    {
        switch($user['level'])
        {
            case 0:
                $badge_cont = "Normal";
                break;
            case 1:
                $badge_cont = "Admin";
                break;
        }
        if($i == 0)
        {
            $active = "active";
            $badge = "<span class=\"float-right badge badge-light\">".$badge_cont."</span>";
        }
        else
        {
            $active = "";
            if($user['level'] == 0)
                $badge = "<span class=\"float-right badge badge-success\">".$badge_cont."</span>";
            else
                $badge = "<span class=\"float-right badge badge-primary\">".$badge_cont."</span>";
        }
        echo "<a class=\"list-group-item list-group-item-action ".$active."\" id=\"user-".$user['user_id']."\" href=\"#\" role=\"tab\" aria-controls=\"user-".$user['user_id']."\">".$user['name'].$badge."</a>";
        $i++;
    }
    echo "</div>";
}
function systems_checkbox($strsys)
{
    if(sizeof($strsys))
    {
        foreach($strsys as $system)
        {
            echo "<div class=\"form-check form-check-inline\">";
            echo "<input class=\"form-check-input\" type=\"checkbox\" id=\"system-".$system['id_strsys']."\" value=\"".$system['id_strsys']."\">";
            echo "<label class=\"form-check-label\" for=\"system-".$system['id_strsys']."\">".$system['name']."</label>";
            echo "</div>";
        }
    }
}
function reports_table_gen($reports, $db, $userLvl, $current_id)
{
    echo "<table class=\"table table-bordered table-hover\">
            <thead class=\"thead-dark\">
                <tr>
                    <th scope=\"col\">Report Name</th>
                    <th scope=\"col\">Created</th>
                    <th scope=\"col\">Owner</th>
                    <th scope=\"col\">Email list</th>
                    <th scope=\"col\">Frequency</th>
                    <th scope=\"col\">Storage system</th>
                    <th scope=\"col\">Action</th>
                </tr>
            </thead>";
    echo "  <tbody>";
    if(sizeof($reports) > 0)
    {
        foreach($reports as $report)
        {
            echo "<tr>";
            echo "<td>".$report['name']."</td>";
            echo "<td>".$report['date']."</td>";
            $owner = $db->get_user_by_id($report['user_id']);
            echo "<td>".$owner['name']."</td>";
            echo "<td class=\"text-break\">".$report['email_list']."</td>";
            switch($report['frequency'])
            {
                case 0:
                    echo "<td>Every Month</td>";
                    break;
                case 1:
                    echo "<td>Every Week</td>";
                    break;
                case 2:
                    echo "<td>Every Day</td>";
                    break;
            }
            echo "<td>";
            foreach($report['strsys_list'] as $strsys)
            {
                echo "<a href=\"system_display.php?id_strsys=".$strsys['id_strsys']."\" class=\"btn btn-link\">".$strsys['name']."</a>";
            }
            echo "</td>";
            if($userLvl > 0)
            {
                if($current_id == $report['user_id'] || $userLvl == 2)
                {
                    echo "<td><a href=\"#\" class=\"btn btn-link delete-rep-sch\" id=\"rep".$report['report_id']."\">Delete</a>"; 
                }
                else
                {
                    echo "<td></td>";
                }
            }
            else
            {
                echo "<td></td>";
            }
            echo "</tr>";
        }
    }
    else
    {
        echo "<td class=\"text-center\" colspan=\"7\">No current records found.</td>";
    }
    echo "  </tbody></table>";
}
function alerts_table_gen($alerts)
{
    echo "<table class=\"table table-bordered table-hover\">
            <thead class=\"thead-dark\">
                <tr>
                    <th class=\"text-center\" scope=\"col\">Status</th>
                    <th scope=\"col\">Created At</th>
                    <th scope=\"col\">Event Description</th>
                    <th scope=\"col\">Event Code</th>
                </tr>
            </thead>
            <tbody>";
    if(sizeof($alerts))
    {
        foreach($alerts as $alert)
        {
            echo "<tr>
                <td class=\"text-warning text-center\">".alert_level($alert['status'])."</td>
                <td>".$alert['date']."</td>
                <td>".$alert['description']."</td>
                <td>".$alert['code']."</td>
            </tr>";
        }
    }
    else
    {
        echo "<td class=\"text-center\" colspan=\"7\">No current records found.</td>";
    }
    echo "</tbody></table>";
}
function pools_table_gen($pools, $entries,$strsys,$report = true)
{
    echo "<table class=\"table table-bordered table-hover\">
                <thead class=\"thead-dark\">
                    <tr>
                        <th scope=\"col\">Pool ID</th>
                        <th scope=\"col\">Pool Name</th>
                        <th scope=\"col\">Total Capacity (GB)</th>
                        <th scope=\"col\">Used Capacity (GB)</th>
                        <th scope=\"col\">Free Capacity (GB)</th>
                        <th scope=\"col\">% Subscription</th>
                        <th scope=\"col\">Status</th>
                    </tr>
                </thead>
                <tbody>";
    if(sizeof($pools))
    {
        $warnings = array();
        $dangers = array();
        foreach($pools as $pool)
        {
            $key_index = array_search($pool['id_pool'],array_column($entries, 'id_pool'));
            $alert_value = ($entries[$key_index]['used_cap_gb'] / $entries[$key_index]['total_cap_gb'])*100;
            echo "<tr>
                    <td class=\"text-center\">".$pool['id_pool']."</td>
                    <td class=\"text-center\">".$pool['name']."</td>
                    <td class=\"text-center\">".$entries[$key_index]['total_cap_gb']."</td>
                    <td class=\"text-center\">".$entries[$key_index]['used_cap_gb']."</td>
                    <td class=\"text-center\">".$entries[$key_index]['free_cap_gb']."</td>
                    <td class=\"text-center\">".$entries[$key_index]['per_subscription'] ."</td>";
                    if(($alert_value >= $pool['threshold']) && ($alert_value < 80))
                    {
                        echo "<td class=\"text-warning text-center\">Warning</td>";
                        array_push($warnings,$pool['name']);
                    }
                    else if(($alert_value > $pool['threshold']) && ($alert_value >= 80))
                    {
                        echo "<td class=\"text-danger text-center\">Critical</td>";
                        array_push($dangers,$pool['name']);
                    }
                    else
                        echo "<td class=\" text-center\">Normal</td>";
            echo  "</tr>";
        }
    }
    else
    {
        echo "<td class=\"text-center\" colspan=\"7\">No current records found.</td>";
    }
    echo "</tbody></table>";
    if(!empty($warnings))
    {
        echo "<p class=\"text-left alert alert-warning font-weight-bold\">";
        if($report)
            system_health(1,1);
        echo " ";
        if(sizeof($warnings) == 1)
            echo "Pool ";
        else
            echo "Pools ";
        $nbr = 0;
        foreach($warnings as $p)
        {
            if($nbr < sizeof($warnings))
                echo "&#60;$p".(sizeof($warnings) > 1 ? "&#62;, " : "&#62; ");
            else
                echo "and &#60;$p&#62; ";
            $nbr++; 
        }
        echo (sizeof($warnings) == 1 ? "has " : "have ");
        echo "trespassed ".(sizeof($warnings) == 1 ? "its " : "their ")." threshold (70%): Consider some future capacity extensions.";
        echo "</p>";
    }
    if(!empty($dangers))
    {
        echo "<p class=\"text-left alert alert-danger font-weight-bold\">";
        if($report)
            system_health(2,1);
        echo " ";
        if(sizeof($dangers) == 1)
            echo "Pool ";
        else
            echo "Pools ";
        $nbr = 0;
        foreach($dangers as $p)
        {
            if($nbr < sizeof($dangers))
                echo "&#60;$p".(sizeof($dangers) > 1 ? "&#62;, " : "&#62; ");
            else
                echo "and &#60;$p&#62; ";
            $nbr++; 
        }
        echo (sizeof($dangers) == 1 ? "has " : "have ");
        echo "trespassed ".(sizeof($dangers) == 1 ? "its " : "their ")." threshold in a critical way: Data loss may occure if you don't consider immediate capacity extensions or remove some useless data.";
        echo "</p>";
    }
}
function rgs_table_gen($rgs, $entries,$strsys,$report = true)
{
    echo "<table class=\"table table-bordered table-hover\">
                <thead class=\"thead-dark\">
                    <tr>
                        <th scope=\"col\">RG ID#</th>
                        <th scope=\"col\">RAID Type</th>
                        <th scope=\"col\">User Capacity (GB)</th>
                        <th scope=\"col\">Free Capacity (GB)</th>
                        <th scope=\"col\">% Usage</th>
                        <th scope=\"col\">Status</th>
                    </tr>
                </thead>
                <tbody>";
    $warnings = array();
    $dangers = array();            
    if(sizeof($rgs))
    {
        foreach($rgs as $rg)
        {
            $key_index = array_search($rg['id_rg'],array_column($entries, 'id_rg'));
            $used = ($entries[$key_index]['user_cap_gb'] - $entries[$key_index]['free_cap_gb']) * 100 / $entries[$key_index]['user_cap_gb'];
            $used = number_format((float)$used, 3,'.','');
            echo "<tr>
                    <td class=\"text-center\">".$rg['id_rg']."</td>
                    <td class=\"text-center\">RAID ".$rg['type']."</td>
                    <td class=\"text-center\">".$entries[$key_index]['user_cap_gb']."</td>
                    <td class=\"text-center\">".$entries[$key_index]['free_cap_gb']."</td>
                    <td class=\"text-center\">".$used ."</td>";
                    if(($used >= 70) && ($used < 80))
                    {
                        echo "<td class=\"text-warning text-center\">Warning</td>";
                        array_push($warnings,$rg['id_rg']);
                    }
                    else if(($used > 70) && ($used >= 80))
                    {
                        echo "<td class=\"text-danger text-center\">Critical</td>";
                        array_push($dangers,$rg['id_rg']);
                    }
                    else
                        echo "<td class=\" text-center\">Normal</td>";
            echo  "</tr>";
        }
    }
    else
    {
        echo "<td class=\"text-center\" colspan=\"7\">No current records found.</td>";
    }
    echo "</tbody></table>";
    if(!empty($warnings))
    {
        echo "<p class=\"text-left alert alert-warning font-weight-bold\">";
        if($report)
            system_health(1,1);
        echo " ";
        if(sizeof($warnings) == 1)
            echo "RG ";
        else
            echo "RGs ";
        $nbr = 0;
        foreach($warnings as $p)
        {
            if($nbr < sizeof($warnings))
                echo "&#60;$p".(sizeof($warnings) > 1 ? "&#62;, " : "&#62; ");
            else
                echo "and &#60;$p&#62; ";
            $nbr++; 
        }
        echo (sizeof($warnings) == 1 ? "has " : "have ");
        echo "trespassed 50% of usage: Consider some future capacity extensions.";
        echo "</p>";
    }
    if(!empty($dangers))
    {
        echo "<p class=\"text-left alert alert-danger font-weight-bold\">";
        if($report)
            system_health(2,1);
        echo " ";
        if(sizeof($dangers) == 1)
            echo "RG ";
        else
            echo "RGs ";
        $nbr = 0;
        foreach($dangers as $p)
        {
            if($nbr < sizeof($dangers))
                echo "&#60;$p".(sizeof($dangers) > 1 ? "&#62;, " : "&#62; ");
            else
                echo "and &#60;$p&#62; ";
            $nbr++; 
        }
        echo (sizeof($dangers) == 1 ? "has " : "have ");
        echo "reached ".(sizeof($dangers) == 1 ? "its " : "their ")." limit in a critical way: Limit will be reached if you don't consider immediate capacity extensions or remove some useless data.";
        echo "</p>";
    }
}
function generate_random_color()
{
    $hex = '#';
 
//Create a loop.
    foreach(array('r', 'g', 'b') as $color){
        //Random number between 0 and 255.
        $val = mt_rand(0, 255);
        //Convert the random number into a Hex value.
        $dechex = dechex($val);
        //Pad with a 0 if length is less than 2.
        if(strlen($dechex) < 2){
            $dechex = "0" . $dechex;
        }
        //Concatenate
        $hex .= $dechex;
    }
    
    //Print out our random hex color.
    return $hex;
}
function top_report($date)
{
    include "cmp/top_report.php";
}
function header_report($date, $strsys_name)
{
    include "cmp/header_report.php";
}
function bottom_report()
{
    include "cmp/bottom_report.html";
}
function strsys_info_report($strsys,$db)
{
    $entries_pools = $db->load_last_pools_entry($strsys);
    $pools = $db->get_system_pools_byID($strsys);
    if($strsys['id_type'] == 1)
    {
        $rgs = $db->get_system_rgs_byID($strsys);
        $entries_rgs = $db->load_last_rgs_entry($strsys);
    }
    $state_pools = get_pools_global_health($pools,$rgs,$entries_pools,$entries_rgs);
    if($strsys['system_health'] == 0)
        $health = "GOOD";
    else if($strsys['system_health'] == 1)
        $health = "WARNING";
    else if($strsys['system_health'] == 2)
        $health = "CRITICAL";
    else
        $health = "UNKNOWN";
    if($state_pools == 0)
        $health_s = "GOOD";
    else if($state_pools == 1)
        $health_s = "WARNING";
    else if($state_pools == 2)
        $health_s = "CRITICAL";
    else
        $health_s = "UNKNOWN";
    if($strsys['id_type'] == 1)
        $tp = "VNX";
    else if($strsys['id_type'] == 2)
        $tp = "Unity";
    $sys_table ="<div class=\"content-title\">Storage System</div><table class=\"info-table\">";
    $sys_table .="<tr><td>NAME</td>";
    $sys_table .="<td>".$strsys['name']."</td>";
    $sys_table .="<td rowspan=\"7\"><img src=\"img/$tp.png\" style=\"width:200px;\"></td></tr>";
    $sys_table .="<tr><td>SITE LOCATION</td>";
    $sys_table .="<td>".$strsys['site_location']."</td></tr>";
    $sys_table .="<tr><td>MODEL</td>";
    $sys_table .="<td>".$strsys['model']."</td></tr>";
    $sys_table .="<tr><td>SERIAL NUMBER</td>";
    $sys_table .="<td>".$strsys['serial_number']."</td></tr>";
    $sys_table .="<tr><td>SPA IP</td>";
    $sys_table .="<td>".$strsys['spa_ip']."</td></tr>";
    $sys_table .="<tr><td>SPB IP</td>";
    $sys_table .="<td>".$strsys['spb_ip']."</td></tr>";
    $sys_table .="<tr><td>FIRMWARE VERSION</td>";
    $sys_table .="<td>".$strsys['firmware_ver']."</td></tr>";
    $sys_table .="<tr><td>HARDWARE STATUS</td>";
    $sys_table .="<td>".$health."</td></tr>";
    $sys_table .="<tr><td>STORAGE STATUS</td>";
    $sys_table .="<td>".$health_s."</td></tr></table>";
    echo $sys_table;
}
function convert_canvas_img($id)
{
    echo "<script>
        function convertCanvasToImg(id)
        {
            var canvas = document.getElementById(id);
            var img = new Image();
            img.src = canvas.toDataURL(\"image/png\");
            canvas.insertAdjacentElement(\"afterend\",img);
            canvas.parentNode.removeChild(canvas);
        }
        ";
    foreach($id as $element)
        echo "convertCanvasToImg('$element');
        ";
    echo "
        </script>
    ";
}
function ping($host) 
{
    $os = (strtoupper(substr(PHP_OS, 0, 3)) === (chr(87).chr(73).chr(78)));
    if ($os){
        if (!exec("ping -n 1 -w 1 ".$host." 2>NUL > NUL && (echo 0) || (echo 1)"))
            return true;
    } else {
        if (!exec("ping -q -c1 ".$host." >/dev/null 2>&1 ; echo $?"))
            return true;
    }

    return false;
}
function cover_page($strsys_name,$date,$freq=-1)
{
    include "cmp/cover_page.php";
}
function _scan_dir($dir) {
    $ignored = array('.', '..', '.svn', '.htaccess');

    $files = array();    
    foreach (scandir($dir) as $file) {
        if (in_array($file, $ignored)) continue;
        $files[$file] = filemtime($dir . '/' . $file);
    }

    arsort($files);
    $files = array_keys($files);

    return ($files) ? $files : array();
}
function reports_files_gen($index,$lvl = 0,$display_at_once = 10)
{
    $report_dir = $_SERVER["DOCUMENT_ROOT"]."/reports";
    $files = _scan_dir($report_dir);
    echo "<table class=\"table table-bordered table-hover\">
            <thead class=\"thead-dark\">
                <tr>
                    <th class=\"text-center\" scope=\"col\">File Name</th>
                    <th scope=\"col\">Created At</th>
                    <th scope=\"col\">Concerned System</th>
                    <th scope=\"col\">Actions</th>
                </tr>
            </thead>
            <tbody>";
    if(sizeof($files))
    {
        $start = $index * $display_at_once;
        $i = $start ; 
        while(($i < $start + $display_at_once) && ($i < sizeof($files))) 
        {
            $file = $files[$i];
            echo "<tr>";
            echo "<td scope=\"col\"><i class=\"fa fa-file-pdf-o\" aria-hidden=\"true\"></i> ".$file."</td>";
            $elements = explode("-",$file);
            
            echo "<td scope=\"col\">".substr($elements[1],0,2)."/".substr($elements[1],2,2)."/".substr($elements[1],4,4)." ".substr($elements[1],8,2).":".substr($elements[1],10,2)."</td>";
            echo "<td scope=\"col\">".str_replace(".pdf","",$elements[3])."</td>";
            echo "<td scope=\"col\" style=\"text-align: center;\"><a href=\"reports/$file\" class=\"btn btn-link download-rep\" download>Download</a>"; 
            if($lvl >0)
            {
                echo "<a href=\"#\" class=\"btn btn-link delete-rep\" id=\"$file\">Delete</a> ";
            }
            echo "</td>";
            echo "</tr>";
            $i++; 
        }
    }
    else
    {
        echo "<td class=\"text-center\" colspan=\"4\">No current report found.</td>";
    }
    echo "</tbody></table>";
}
function pagination_gen($index,$display_at_once = 10)
{
    $report_dir = $_SERVER["DOCUMENT_ROOT"]."/reports";
    $files = array_reverse(array_diff(scandir($report_dir), array('.', '..')));
    $number = ceil(sizeof($files)/$display_at_once);
    echo "<nav aria-label=\"Page navigation\">
  <ul class=\"pagination\">";
    if($index == 0)
    {
        echo "<li class=\"page-item disabled\">
        <a class=\"page-link\" href=\"#\" aria-disabled=\"true\" aria-label=\"Previous\">
          <span aria-hidden=\"true\">&laquo;</span>
        </a>
      </li>";
    }
    else
    {
        echo "<li class=\"page-item\">
        <a class=\"page-link\" href=\"reports.php?index=".($index-1)."\" aria-label=\"Previous\">
          <span aria-hidden=\"true\">&laquo;</span>
        </a>
      </li>";
    }
    for($i = 0; $i < $number; $i++)
    {
        if($i == $index)
        {
            echo "<li class=\"page-item active\" aria-current=\"page\">
            <span class=\"page-link\">
              ".($i+1)."<span class=\"sr-only\">(current)</span></span></li>";
        }
        else
        {
            echo "<li class=\"page-item\" aria-current=\"page\">
            <a class=\"page-link\" href=\"reports.php?index=$i\">".($i+1)."</a></li>"; 
        }
    }
    if($index == $number-1)
    {
        echo "<li class=\"page-item disabled\">
        <a class=\"page-link\" href=\"#\" aria-disabled=\"true\" aria-label=\"Next\">
          <span aria-hidden=\"true\">&raquo;</span>
        </a>
      </li>";
    }
    else
    {
        echo "<li class=\"page-item\">
        <a class=\"page-link\" href=\"reports.php?index=".($index+1)."\" aria-label=\"Next\">
          <span aria-hidden=\"true\">&raquo;</span>
        </a>
      </li>";
    }
    echo "  </ul></nav>";
}
function get_pools_global_health($pools,$rgs,$entries_pools, $entries_rgs)
{
    $glob_pool_health = 0;
    $curr_pool_health = 0;
    foreach($pools as $pool)
    {
        $curr_pool_health = 0;
        $key_index = array_search($pool['id_pool'],array_column($entries_pools, 'id_pool'));
        $alert_value = ($entries_pools[$key_index]['used_cap_gb'] / $entries_pools[$key_index]['total_cap_gb'])*100;
        if(($alert_value >= $pool['threshold']) && ($alert_value < 80))
        {
            $curr_pool_health = 1;
        }
        else if(($alert_value > $pool['threshold']) && ($alert_value >= 80))
        {
            $curr_pool_health = 2;
        }    
        if($curr_pool_health > $glob_pool_health)
            $glob_pool_health = $curr_pool_health;
    }
    if(isset($rgs))
    {
        if(sizeof($rgs) != 0)
        {
            foreach($rgs as $rg)
            {
                $curr_pool_health = 0;
                $key_index = array_search($rg['id_rg'],array_column($entries_rgs, 'id_rg'));
                $used = ($entries_rgs[$key_index]['user_cap_gb'] - $entries_rgs[$key_index]['free_cap_gb']) * 100 / $entries_rgs[$key_index]['user_cap_gb'];
                $used = number_format((float)$used, 3,'.','');
                if(($used >= 70) && ($used < 80))
                {
                    $curr_pool_health = 1;
                }
                else if(($used >= 80))
                {
                    $curr_pool_health = 2;
                }
                if($curr_pool_health > $glob_pool_health)
                    $glob_pool_health = $curr_pool_health;
            }
        }
    }
    return $glob_pool_health;
}
function compare_luns($a,$b)
{
    if($a['consumption'] == $b['consumption'])
        return 0;
    return ($a['consumption'] < $b['consumption'] ? 1 : -1);
}

function compare_luns_alt($a,$b)
{
    if($a['consumption_evolution'] == $b['consumption_evolution'])
        return 0;
    return ($a['consumption_evolution'] < $b['consumption_evolution'] ? 1 : -1);
}
function compare_luns_alt_2($a, $b)
{
    if($a['consumption_evolution_gb'] == $b['consumption_evolution_gb'])
        return 0;
    return ($a['consumption_evolution_gb'] < $b['consumption_evolution_gb'] ? 1 : -1);
}

function get_top_ten_luns($luns, $lun_status,$lun_status_offset,$sorting,$number_elem)
{
    $change = true;
    $top_ten_list = array();
    foreach($luns as $lun)
    {
        $key_index = array_search($lun['id_lun'],array_column($lun_status, 'id_lun'));
        if(!empty($lun_status_offset))
            $key_index_2 = array_search($lun['id_lun'],array_column($lun_status_offset, 'id_lun'));
        else
            $key_index_2 = -1;
        $consumption = ($lun_status[$key_index]['consumed_cap_gb'] / $lun_status[$key_index]['total_cap_gb']) * 100;
        $consumption_evolution = ($key_index_2 != -1 ? round((($lun_status[$key_index]['consumed_cap_gb'] - $lun_status_offset[$key_index_2]['consumed_cap_gb']) /  $lun_status[$key_index]['total_cap_gb'])*100,3) : "UNKNOWN");
        $consumption_evolution_gb = ($key_index_2 != -1 ? round($lun_status[$key_index]['consumed_cap_gb'] - $lun_status_offset[$key_index_2]['consumed_cap_gb'],3) : "UNKNOWN");
        $lun_pres = array(
            "id_lun" => $lun['id_lun'],
            "name" => $lun['name'],
            "total_cap_gb" => $lun_status[$key_index]['total_cap_gb'],
            "user_cap_gb" => $lun_status[$key_index]['user_cap_gb'],
            "consumed_cap_gb" => $lun_status[$key_index]['consumed_cap_gb'],
            "consumption" => number_format((float) $consumption,3,'.',','),
            "consumption_evolution" => $consumption_evolution,
            "consumption_evolution_gb" => $consumption_evolution_gb
        );
        array_push($top_ten_list,$lun_pres);
    }
    if($sorting == 0)
        usort($top_ten_list,"compare_luns");
    else if($sorting == 1)
        usort($top_ten_list,"compare_luns_alt");
    else if($sorting == 2)
        usort($top_ten_list,"compare_luns_alt_2");
    return array_slice($top_ten_list,0,$number_elem);
}
function lun_table_gen($luns, $lun_status,$lun_status_offset, $sorting,$number_elem, $report,$strsys)
{
    echo "<table class=\"table table-bordered table-hover table-lun my-2\">
            <thead class=\"thead-dark\">
                <tr>
                    <th scope=\"col\" class=\"text-center align-middle lun-order\">#</th>
                    <th scope=\"col\" class=\"text-center align-middle lun-id\">LUN ID</th>
                    <th scope=\"col\" class=\"text-center align-middle lun-name\">Name</th>
                    <th scope=\"col\" class=\"text-center align-middle lun-size\">Size (GB)</th>
                    <th scope=\"col\" class=\"text-center align-middle lun-size\">Allocated (GB)</th>
                    <th scope=\"col\" class=\"text-center align-middle lun-size\">Allocated(%)</th>
                    <th scope=\"col\" class=\"text-center align-middle lun-size\">".($report ? "Alloc Ev. (%)" : "Allocation Evolution (%)")."</th>
                    <th scope=\"col\" class=\"text-center align-middle lun-size\">".($report ? "Alloc Ev. (GB)" : "Allocation Evolution (GB)")."</th>
                </tr>
            </thead>";
    echo "  <tbody>";
    if(!empty($luns))
    {
        $lun_list = get_top_ten_luns($luns, $lun_status,$lun_status_offset,$sorting,$number_elem);
        $i = 0;
        foreach($lun_list as $lun)
        {
            if($lun['consumption'] > 80)
                $cons_color = "text-danger";
            else if($lun['consumption'] <= 80 && $lun['consumption'] > 70)
                $cons_color = "text-warning";
            else
                $cons_color = "";
            if($lun['consumption_evolution'] > 0)
            {
                $ev_color = "text-danger";
                if(!$report)
                    $arrow = "<i class=\"fa fa-caret-up\" aria-hidden=\"true\"></i>";
                else
                    $arrow = "+";
            }
            else if($lun['consumption_evolution'] < 0)
            {
                $ev_color = "text-success";
                if(!$report)
                    $arrow = "<i class=\"fa fa-caret-down\" aria-hidden=\"true\"></i>"; 
                else
                    $arrow = "-";
            }
            else
            {
                $ev_color ="";
                $arrow = "";
            }
            /*if($report)
                $name = substr($lun['name'],0,20);
            else*/
                $name = $lun['name'];
            if($strsys['id_type'] == 2)
                $id = "sv_".$lun['id_lun'];
            else
                $id = $lun['id_lun'];
            echo "<tr>
                    <td class=\"text-center\">".($i+1)."</td>
                    <td class=\"text-center\">".$id."</td>
                    <td class=\"text-center\">".$name."</td>
                    <td class=\"text-center\">".$lun['total_cap_gb']."</td>
                    <td class=\"text-center\">".$lun['consumed_cap_gb']."</td>
                    <td class=\"text-center $cons_color\">".$lun['consumption']."</td>
                    <td class=\"text-center $ev_color\">$arrow ".$lun['consumption_evolution']."</td>
                    <td class=\"text-center $ev_color\">$arrow ".$lun['consumption_evolution_gb']."</td>
                </tr>
            ";
            $i++;
        }
    }
    else
    {
        echo "<td class=\"text-center\" colspan=\"8\" style=\"text-align:center;\">No current records found.</td>";
    }
    echo "</tbody></table>";
}
function gen_pool_nav_tabs($strsys, $pools,  $db,$rgs = array(),$evolution = 1, $sorting = 0,$number_elem = 10, $report = false)
{
    echo "<ul class=\"nav nav-tabs\" id=\"tab-list\" role=\"tablist\">";
    $first = true;
    foreach($pools as $pool)
    {
        echo "<li class=\"nav-item\">";
        echo "   <a class=\"nav-link ".($first ? "active" : "" )."\" id=\"pool-".$pool['id_pool']."\" data-toggle=\"tab\" href=\"#pool-".$pool['id_pool']."-tab\" role=\"tab\" aria-controls=\"pool-".$pool['id_pool']."-tab\" aria-selected=\"true\">".$pool['name']."</a>";
        echo "</li>";
        $first = false;
    }
    /*
    if(!empty($rgs))
    {
        foreach($rgs as $rg)
        {
            echo "<li class=\"nav-item\">";
            echo "   <a class=\"nav-link\" id=\"rg-".$rg['id_rg']."\" data-toggle=\"tab\" href=\"#rg-".$rg['id_rg']."-tab\" role=\"tab\" aria-controls=\"rg-".$rg['id_rg']."-tab\" aria-selected=\"true\">RAID GROUP #".$rg['id_rg']."</a>";
            echo "</li>";
        }
    }*/
    echo "</ul>";
    echo "<div class=\"tab-content\" id=\"pool-tab-content\">";
    $first = true;
    $date = date("Y-m-d");
    foreach($pools as $pool)
    {
        $date_offset = get_lun_date_offset($evolution);
        $evol = $evolution;
        $luns = $db->load_luns_by_pool($pool,$strsys);
        $luns_status = $db->load_luns_entries_by_date($pool,$strsys,$date);
        $luns_status_offset = $db->load_luns_entries_by_date($pool,$strsys,$date_offset);
        while(empty($luns_status_offset) && $evol > 0)
        {
            $evol--;
            $date_offset = get_lun_date_offset($evol);
            $luns_status_offset = $db->load_luns_entries_by_date($pool,$strsys,$date_offset);
        }
        echo "<div class=\"tab-pane fade ".($first ? "show active" : " " )."\" id=\"pool-".$pool['id_pool']."-tab\" role=\"tabpanel\" aria-labelledby=\"pool-".$pool['id_pool']."\">";
        lun_table_gen($luns, $luns_status,$luns_status_offset,$sorting,$number_elem,$report,$strsys);
        echo "</div>";
        $first = false;
    }
    /*
    foreach($rgs as $rg)
    {
        echo "<div class=\"tab-pane fade\" id=\"rg-".$rg['id_rg']."-tab\" role=\"tabpanel\" aria-labelledby=\"rg-".$rg['id_rg']."\">";
        //lun_table_gen(array());
        echo "</div>";
    }*/
    echo "</div>";
}
function get_lun_date_offset($evolution)
{
    $date = date("Y-m-d");
    $offset_day = $evolution * 86400;
    $day_past = strtotime($date) - $offset_day;
    $date_offset = date("Y-m-d", $day_past);
    return $date_offset;
}
?>
