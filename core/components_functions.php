<?php
function side_navbar_gen($strsys)
{
    include "cmp/side_navbar.php";
}
function add_new_system_mod_gen($types)
{
    include "cmp/add_new_system_mod.php";
}
function help_mod_gen()
{
    include "cmp/help_mod.html";
}
function main_navbar_gen($userName)
{
    include "cmp/main_navbar.php";
}
function strsys_card_gen($system, $types, $state,$i,$state_pools)
{
    include "cmp/strsys_card.php";
}
function strsys_card_gen_disp($system, $types, $state, $state_pools)
{
    include "cmp/strsys_card_disp.php";
}
function main_footer_gen()
{
    include "cmp/main_footer.html";
}
function js_libraries_gen()
{
    include "cmp/js_libraries.html";
}
function main_head_gen($current_page)
{
    include "cmp/main_head.php";
}
function generate_chart_pie($db, $strsys)
{
    include "cmp/chart_script.php";
}
function generate_graph_pie($db,$strsys)
{
    include "cmp/graph_script.php";
}
function generate_graph_global($db,$strsys)
{
    include "cmp/global_graph_script.php";
}
function global_graph_mod_gen()
{
    include "cmp/global_graph_mod.php";
}
function download_report_mod($strsys)
{
    include "cmp/download_report_mod.php";
}
function add_strsys_script()
{
    include "cmp/add_strsys_script.php";
}
function delete_file_mod()
{
    include "cmp/confirm_delete_mod.html";
}
function delete_file_script()
{
    include "cmp/reports_script.php";
}
function delete_system_mod_gen($id_strsys)
{
    include "cmp/delete_system_mod.php";
}
function generate_report_mod_gen($strsys)
{
    include "cmp/generate_report_mod.php";
}
?>