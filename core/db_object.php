<?php
/**
 * class: db_object
 * function: overlay to access the application database
 * requires:requires a file for configuration named "config_file.conf"
 */
include_once "misc_functions.php";
const CONFIG_FILE = "configdb_file.conf";
const NBR_PROP = 4;
class db_object
{
    private $config_file;
    private $hostname;
    private $databasename;
    private $login;
    private $password;
    private $isWin;
    private $pdo_intern;
    private $pdo_status;
    private $strsys_fields = "id_strsys, id_type, `name`, site_location, serial_number, model, INET_NTOA(`spa_ip`) AS spa_ip, INET_NTOA(`spb_ip`) AS spb_ip, INET_NTOA(`cs_ip`) AS cs_ip, firmware_ver, system_health";
    function __construct()
    {
        $this->isWin = false;
        if(strcasecmp(substr(PHP_OS,0,3), 'WIN') == 0)
            $this->isWin = true;
        $config_file = CONFIG_FILE;
        $action= "opening config file";
        if($file = fopen($config_file,"r"))//config file parsing
        {
            $prop_set = 0;
            $action= "parsing config file";
            /*while(!feof($file))
            {
                $line = rtrim(ltrim(fgets($file)));
                $line = str_replace(" ","",$line);
                if($line[0] != '#')
                {
                    $elementsParsed = explode(":",$line);
                    switch($elementsParsed[0])
                    {
                        case "hostname":
                            $this->hostname = $elementsParsed[1];
                            $prop_set++;
                            break;
                        case "databasename":
                            $this->databasename = $elementsParsed[1];
                            $prop_set++;
                            break;
                        case "login":
                            $this->login = $elementsParsed[1];
                            $prop_set++;
                            break;
                        case "password":
                            $this->password = $elementsParsed[1];
                            $prop_set++;
                            break;
                        default:
                            $this->pdo_status = false;
                            die("Error while $action: Please check syntax\n");
                            break; 
                    }
                }
            }*/
	    $this->hostname="localhost";
	    $this->databasename="FTS_mon_app";
	    $this->login="root";
	    $this->password="";
	    $prop_set=4;
            $action = "configuring database access";
            if($prop_set == NBR_PROP)
            {
                $pdo_connexion_line = "mysql:host=$this->hostname;dbname=$this->databasename;charset=utf8";
                try
                {
                    $this->pdo_status = true;
                    $this->pdo_intern = new PDO($pdo_connexion_line, $this->login, $this->password);
                }
                catch(Exception $e)
                {
                    $this->pdo_status = false;
                    die('Error while accessing database: '. $e->getMessage());
                }
            }
            else
            {
                $this->pdo_status = false;
                die("Error while $action: please check for missing parameters\n");
            }
        }
        else
        {
	    ini_set('auto_detect_line_endings', false);
            $this->pdo_status = false;
            die("Error while $action\n");
        }
	fclose($file);
    }
    public function show_db_access_config()
    {
        echo "hostname: $this->hostname ** \ndatabasename: $this->databasename ** \nlogin: $this->login ** \npassword: $this->password ** \n";
        echo "PDO status: $this->pdo_status";
    }
    public function get_strsys_types()
    {
        if($this->pdo_status)
        {
            $qry = $this->pdo_intern->prepare("SELECT id_type,`name` FROM sys_types ORDER BY id_type");
            $exec_query = $qry->execute();
            if($exec_query)
            {
                $types = $qry->fetchAll(\PDO::FETCH_ASSOC);
                $qry->closeCursor();
                return $types;
            }
        }
    }
    public function verify_user_by_name_only($name)
    {
        if($this->pdo_status)
        {
            $qry = $this->pdo_intern->prepare("SELECT * FROM users WHERE `name` = ?");
            $exec_qry = $qry->execute(array($name));
            if($exec_qry)
            {
                if($qry->rowCount() == 0)
                {
                    return false;
                }
                else
                {
                    $qry->closeCursor();
                    return true;
                }
            }
        }
    }
    public function verify_user_by_name($name, $passwd)
    {
        if($this->pdo_status)
        {
            $qry = $this->pdo_intern->prepare("SELECT * FROM users WHERE `name` = ?");
            $exec_qry = $qry->execute(array($name));
            if($exec_qry)
            {
                if($user = $qry->fetch(\PDO::FETCH_ASSOC))
                {
                    $qry->closeCursor();
                    if(password_verify($passwd,$user['passwd']))
                        return array($user['user_id'],$user['name'],$user['passwd'],$user['level']);
                    else
                        return array();
                }
                else
                {
                    $qry->closeCursor();
                    return array();
                }
            }
        }
    }
    public function verify_user_by_id($id)
    {
        if($this->pdo_status)
        {
            $qry = $this->pdo_intern->prepare("SELECT * FROM users WHERE `user_id` = ?");
            $exec_qry = $qry->execute(array($id));
            if($exec_qry)
            {
                if($user = $qry->fetch())
                {
                    $qry->closeCursor();
                    return true;
                }
                else
                {
                    $qry->closeCursor();
                    return false;
                }
            }
        }
    }
    public function get_user_by_id($id)
    {
        if($this->pdo_status)
        {
            $qry = $this->pdo_intern->prepare("SELECT * FROM users WHERE `user_id` = ?");
            $exec_qry = $qry->execute(array($id));
            if($exec_qry)
            {
                if($user = $qry->fetchAll(\PDO::FETCH_ASSOC))
                {
                    $qry->closeCursor();
                    return $user[0];
                }
                else
                {
                    $qry->closeCursor();
                    return null;
                }
            }
        }
    }
    public function get_all_users()
    {
        if($this->pdo_status)
        {
            $qry = $this->pdo_intern->prepare("SELECT * FROM users WHERE `user_id` != 1 ");
            $exec_qry = $qry->execute();
            if($exec_qry)
            {
                if($users = $qry->fetchAll(\PDO::FETCH_ASSOC))
                {
                    $qry->closeCursor();
                    return $users;
                }
                else
                {
                    $qry->closeCursor();
                    return array();
                }
            }
        }
    }
    public function add_new_user($name, $lvl = 0)
    {
        $qry = $this->pdo_intern->prepare("INSERT INTO users (`name`,`passwd`,`level`) VALUES (?,?,?)");
        $exec_qry = $qry->execute(array($name, password_hash("abc123", PASSWORD_DEFAULT),$lvl));
        if($qry->rowCount() > 0)
            return true;
        else
            return false;
    }
    public function update_user($id,$lvl)
    {
        $qry = $this->pdo_intern->prepare("UPDATE users SET `level` = ? WHERE `user_id` = ?");
        $exec_qry = $qry->execute(array($lvl, $id));
        if($qry->rowCount() > 0)
            return true;
        else
            return false;
    }
    public function change_user_passwd($id, $sessionPasswd, $ancient, $new)
    {
        if(password_verify($ancient, $sessionPasswd))
        {
            $qry = $this->pdo_intern->prepare("UPDATE users SET `passwd` = ? WHERE `user_id` = ?");
            $exec_qry = $qry->execute(array(password_hash($new,PASSWORD_DEFAULT), $id));
            if($qry->rowCount() > 0)
                return true;
            else
                return false;
        }
    }
    public function reset_user($id) 
    {
        $default_ = "abc123";
        $qry = $this->pdo_intern->prepare("UPDATE users SET `passwd` = ? WHERE `user_id` = ?");
        $exec_qry = $qry->execute(array(password_hash($default_,PASSWORD_DEFAULT), $id));
        if($qry->rowCount() > 0)
            return true;
        else
            return false;
    }
    public function delete_user_by_id($id)
    {
        if($this->pdo_status)
        {
            $qry = $this->pdo_intern->prepare("DELETE FROM users WHERE `user_id` = ?");
            $exec_qry = $qry->execute(array($id));
            return $exec_qry;
        }
    }
    public function load_strsys_bySPIP($ipaddress)
    {
        if($this->pdo_status)
        {
            $qry = $this->pdo_intern->prepare("SELECT id_strsys, id_type, `name`, site_location, serial_number, model, INET_NTOA(`spa_ip`) AS spa_ip, INET_NTOA(`spb_ip`) AS spb_ip, INET_NTOA(`cs_ip`) AS cs_ip, firmware_ver, system_health FROM strsys WHERE spa_ip = INET_ATON(?) OR spb_ip = INET_ATON(?) OR cs_ip = INET_ATON(?)");
            $exec_qry = $qry->execute(array($ipaddress,$ipaddress,$ipaddress));
            if($strsys = $qry->fetchAll(\PDO::FETCH_ASSOC))
            {
                $qry->closeCursor();
                return $strsys;
            }
            else
            {
                echo "No entry found in database havin $ipaddress as an IP ADDRESS\n";
                $qry->closeCursor();
                return array();
            }
        }
    }
    public function load_strsys_byNAME($name)
    {
        if($this->pdo_status)
        {
            $qry = $this->pdo_intern->prepare("SELECT id_strsys, id_type, `name`, site_location, serial_number, model, INET_NTOA(`spa_ip`) AS spa_ip, INET_NTOA(`spb_ip`) AS spb_ip, INET_NTOA(`cs_ip`) AS cs_ip, firmware_ver, system_health FROM strsys WHERE `name` = ?");
            $exec_qry = $qry->execute(array($name));
            if($strsys = $qry->fetchAll(\PDO::FETCH_ASSOC))
            {
                $qry->closeCursor();
                return $strsys;
            }
            else
            {
                echo "No entry found in database having $name as NAME_\n";
                $qry->closeCursor();
                return NULL;
            }
        }
    }
    public function load_strsys_byID($id)
    {
        if($this->pdo_status)
        {
            $qry = $this->pdo_intern->prepare("SELECT id_strsys, id_type, `name`, site_location, serial_number, model, INET_NTOA(`spa_ip`) AS spa_ip, INET_NTOA(`spb_ip`) AS spb_ip, INET_NTOA(`cs_ip`) AS cs_ip, firmware_ver, system_health FROM strsys WHERE `id_strsys` = ?");
            $exec_qry = $qry->execute(array($id));
            if($strsys = $qry->fetchAll(\PDO::FETCH_ASSOC))
            {
                $qry->closeCursor();
                return $strsys[0];
            }
            else
            {
                echo "No entry found in database havin $id as ID\n";
                $qry->closeCursor();
                return NULL;
            }
        }
    }
    public function load_all_strsys()
    {
        if($this->pdo_status)
        {
            $qry = $this->pdo_intern->prepare("SELECT id_strsys, id_type, `name`, site_location, serial_number, model, INET_NTOA(`spa_ip`) AS spa_ip, INET_NTOA(`spb_ip`) AS spb_ip, INET_NTOA(`cs_ip`) AS cs_ip, firmware_ver, system_health FROM strsys");
            $exec_qry = $qry->execute();
            if($exec_qry)
            {
                $resp = $qry->fetchAll(\PDO::FETCH_ASSOC);
                $qry->closeCursor();    
                return $resp;
            }
            else
            {
                echo "No entry found in database.\n";
                $qry->closeCursor();
                return NULL;
            }
        }
    }
    public function update_strsys($strsys)
    {
        if($this->pdo_status)
        {
            $result = $this->pdo_intern->prepare("UPDATE strsys SET id_type = ? , `name` = ? , site_location = ? , serial_number = ?, model = ?, spa_ip = INET_ATON(?), spb_ip = INET_ATON(?), cs_ip = INET_NTOA(?), firmware_ver = ?, system_health = ?, user = ?, passwd = ? WHERE id_strsys = ?");
            $exec_result = $result->execute(array(
                $strsys['id_type'],
                $strsys['name'],
                $strsys['site_location'],
                $strsys['serial_number'],
                $strsys['model'],
                $strsys['spa_ip'],
                $strsys['spb_ip'],
                $strsys['cs_ip'],
                $strsys['firmware_ver'],
                $strsys['system_health'],
                $strsys['user'],
                $strsys['passwd'],
                $strsys['id_strsys']
            ));
            if($exec_result > 0)
            {
                echo "Entry updated in database.";
            }
        }
    }
    public function add_strsys($strsys)
    {
        if($this->pdo_status)
        {
            $ping_test = ping($strsys['spa_ip']);
            if(!$ping_test)
            {
                $ping_test = ping($strsys['spb_ip']);
                if(!$ping_test)
                {
                    $ping_test = ping($strsys['cs_ip']);
                    if(!$ping_test)
                        echo "<span class=\"text-danger\">Cannot Reach storage system. Please check whether you have the right network configuration or the system is up.</span>";
                }
            }
            if($ping_test)
            {
                $result = $this->pdo_intern->prepare("INSERT INTO strsys(id_type, `name`, site_location, serial_number, model, spa_ip, spb_ip, cs_ip, firmware_ver, system_health,user,passwd) VALUES(?,?,?,?,?,INET_ATON(?),INET_ATON(?),INET_ATON(?),?,?,?,?)");
                $exec_result = $result->execute(array(
                    $strsys['id_type'],
                    $strsys['name'],
                    $strsys['site_location'],
                    $strsys['serial_number'],
                    $strsys['model'],
                    $strsys['spa_ip'],
                    $strsys['spb_ip'],
                    $strsys['cs_ip'],
                    $strsys['firmware_ver'],
                    $strsys['system_health'],
                    $strsys['user'],
                    $strsys['passwd']
                ));
                if($exec_result > 0)
                {
                    shell_exec("/usr/bin/python /var/www/html/fts_mon_app_db/data_harvester/data_harvester.py");
                    echo "<span class=\"text-success\">Entry inserted in database.</span>";

                }
            }
        }
    }
    public function delete_strsys_byID($id_strsys)
    {
        if($this->pdo_status)
        {
            $exists = $this->load_strsys_byID($id_strsys);
            if($exists)
            {
                $qry = "DELETE FROM strsys WHERE id_strsys = ?";
                $qry_prepare = $this->pdo_intern->prepare($qry);
                $exec_qry = $qry_prepare->execute(array($id_strsys));
                if($exec_qry)
                    echo "Storage system deleted from known database.";
            }
            else
            {
                echo "Unable to find this Storage System in database.";
            }
        }
    }
    public function add_report($report,$who)
    {
        if($this->pdo_status)
        {
            $date_now = date("Y-m-d H:i:s");
            $qry = $this->pdo_intern->prepare("INSERT INTO reports(`name`,`frequency`,email_list,`date`,`user_id`,sorting,number_elem) VALUES(?,?,?,?,?,?,?)");
            $exec_qry = $qry->execute(array(
                $report['name'],
                $report['frequency'],
                trim($report['email_list']),
                $date_now,
                $who,
                $report['sorting'],
                $report['number_elem']
            ));
            if($exec_qry)
            {
                $qry2 = $this->pdo_intern->prepare("SELECT LAST_INSERT_ID();");
                $qry2->execute();
                $last_id = $qry2->fetchAll(\PDO::FETCH_ASSOC);
                $report_id = $last_id[0]['LAST_INSERT_ID()'];
                $question_marks = "";
                $insert_values = array();
                for($x=0; $x < sizeof($report['entries']);$x++)
                {
                    $array_value = array($report['entries'][$x],$report_id);
                    $insert_values = array_merge($insert_values, $array_value);
                    $question_marks .= "(?,?)";
                    if($x < sizeof($report['entries']) - 1)
                        $question_marks .=", ";
                }
                $qry3 = "INSERT INTO report_entries (id_strsys,report_id) VALUES".$question_marks;
                $statement = $this->pdo_intern->prepare($qry3);
                try
                {
                    $exec_qry = $statement->execute($insert_values);
                }
                catch(PDOException $e)
                {
                    die($e->getMessage());
                }
                
                if($exec_qry)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
    public function get_all_reports()
    {
        if($this->pdo_status)
        {
            $qry = $this->pdo_intern->prepare("SELECT * FROM reports");
            $qry->execute();
            $reports = $qry->fetchAll(\PDO::FETCH_ASSOC);
            $index = 0;
            foreach($reports as $report)
            {
                $entries_names = array();
                $get_entries = $this->pdo_intern->prepare("SELECT * from report_entries WHERE report_id = ?");
                $get_entries->execute(array($report['report_id']));
                $entries = $get_entries->fetchAll(\PDO::FETCH_ASSOC);
                foreach($entries as $entry)
                {
                    $get_strsys = $this->load_strsys_byID($entry['id_strsys']);
                    array_push($entries_names,array("name" => $get_strsys['name'], "id_strsys" => $get_strsys['id_strsys']));
                }
                $appending_list = array("strsys_list" => $entries_names);
                $reports[$index] = array_merge($reports[$index],$appending_list);
                $index++;
            }
            return $reports;
        }
    }
    public function delete_report_byID($ID)
    {
        if($this->pdo_status)
        {
            $qry = $this->pdo_intern->prepare("DELETE FROM reports WHERE report_id = ?");
            try
            {
                $exec_qry = $qry->execute(array($ID));
            }
            catch(PDOException $e)
            {
                die( $e->getMessage());
            }
            return $exec_qry;
        }
    }
    public function load_last_pools_entry($strsys)
    {
        if($this->pdo_status)
        {
            $qry = $this->pdo_intern->prepare("SELECT * FROM (SELECT * FROM pool_status WHERE id_pool in (SELECT id_pool FROM pools WHERE id_strsys = ?) AND id_strsys = ? ORDER BY id_pool, date DESC) as T1 GROUP BY id_pool");
            $qry->execute(array($strsys['id_strsys'],$strsys['id_strsys']));
            $entries = $qry->fetchAll(\PDO::FETCH_ASSOC);
            return $entries;
        }
    }
    public function load_last_rgs_entry($strsys)
    {
        if($this->pdo_status)
        {
            $qry = $this->pdo_intern->prepare("SELECT * FROM (SELECT * FROM rg_status WHERE id_rg in (SELECT id_rg FROM rgs WHERE id_strsys = ?) AND id_strsys = ? ORDER BY id_rg, date DESC) as T1 GROUP BY id_rg");
            $qry->execute(array($strsys['id_strsys'], $strsys['id_strsys']));
            $entries = $qry->fetchAll(\PDO::FETCH_ASSOC);
            return $entries;
        }
    }
    public function load_all_pools_entry($strsys)
    {
        if($this->pdo_status)
        {
            $qry = $this->pdo_intern->prepare("SELECT * FROM pool_status WHERE id_pool in (SELECT id_pool FROM pools WHERE id_strsys = ?) AND id_strsys = ? ORDER BY `date` ASC");
            $qry->execute(array($strsys['id_strsys'],$strsys['id_strsys']));
            $entries = $qry->fetchAll(\PDO::FETCH_ASSOC);
            return $entries;
        }
    }
    public function load_pool_entries($id_pool,$strsys)
    {
        if($this->pdo_status)
        {
            $qry = $this->pdo_intern->prepare("SELECT id_status, AVG(total_cap_gb),AVG(used_cap_gb),AVG(free_cap_gb),AVG(per_subscription),id_pool, DATE(`date`) FROM pool_status WHERE id_pool = ? AND id_strsys = ? GROUP BY DATE(`date`) ASC");
            $qry->execute(array($id_pool,$strsys['id_strsys']));
            $entries = $qry->fetchAll(\PDO::FETCH_ASSOC);
            return $entries;
        }
    }
    public function load_all_rgs_entry($strsys)
    {
        if($this->pdo_status)
        {
            $qry = $this->pdo_intern->prepare("SELECT * FROM rg_status WHERE id_rg in (SELECT id_rg FROM rgs WHERE id_strsys = ?) AND id_strsys = ?  ORDER BY `date` ASC");
            $qry->execute(array($strsys['id_strsys'],$strsys['id_strsys']));
            $entries = $qry->fetchAll(\PDO::FETCH_ASSOC);
            return $entries;
        }
    }
    public function load_rg_entries($id_rg,$strsys)
    {
        if($this->pdo_status)
        {
            $qry = $this->pdo_intern->prepare("SELECT id_status, AVG(user_cap_gb), AVG(free_cap_gb),id_rg,DATE(`date`) FROM rg_status WHERE id_rg = ? AND id_strsys = ? GROUP BY DATE(`date`) ASC");
            $qry->execute(array($id_rg,$strsys['id_strsys']));
            $entries = $qry->fetchAll(\PDO::FETCH_ASSOC);
            return $entries;
        }
    }
    public function get_system_alerts_byID($strsys)
    {
        if($this->pdo_status)
        {
            $qry = $this->pdo_intern->prepare("SELECT * FROM alerts WHERE id_strsys = ? ORDER BY `date` DESC");
            $qry->execute(array($strsys['id_strsys']));
            $alerts = $qry->fetchAll(\PDO::FETCH_ASSOC);
            return $alerts;
        }
    }
    public function get_system_pools_byID($strsys)
    {
        if($this->pdo_status)
        {
            $qry = $this->pdo_intern->prepare("SELECT * FROM pools WHERE id_strsys = ?");
            $qry->execute(array($strsys['id_strsys']));
            $pools = $qry->fetchAll(\PDO::FETCH_ASSOC);
            return $pools;
        }
    }
    public function get_system_rgs_byID($strsys)
    {
        if($this->pdo_status)
        {
            $qry = $this->pdo_intern->prepare("SELECT * FROM rgs WHERE id_strsys = ?");
            $qry->execute(array($strsys['id_strsys']));
            $rgs = $qry->fetchAll(\PDO::FETCH_ASSOC);
            return $rgs;
        }
    }
    public function get_dates($strsys)
    {
        if($this->pdo_status)
        {
            if($strsys['id_type'] == 1)
                $qry_txt = "SELECT DISTINCT DATE(pool_status.date) AS day FROM pool_status INNER JOIN rg_status ON pool_status.date = rg_status.date WHERE pool_status.id_strsys = ? ORDER BY pool_status.date ASC";
            else if($strsys['id_type'] == 2)
                $qry_txt = "SELECT DISTINCT DATE(pool_status.date) AS day FROM pool_status WHERE pool_status.id_strsys = ? ORDER BY pool_status.date ASC";
            $qry = $this->pdo_intern->prepare($qry_txt);
            $qry->execute(array($strsys['id_strsys']));
            $dates = $qry->fetchAll(\PDO::FETCH_ASSOC);
            return $dates;
        }
    }
    public function get_strsys_reachable($id_strsys)
    {
        if($this->pdo_status)
        {
            $qry_txt = "SELECT reachable FROM strsys WHERE id_strsys = ?";
            $qry = $this->pdo_intern->prepare($qry_txt);
            $qry->execute(array($id_strsys));
            $reachable = $qry->fetch(\PDO::FETCH_ASSOC);
            return $reachable;
        }
    }
    public function load_luns_by_pool($pool,$strsys)
    {
        if($this->pdo_status)
        {
            $qry_txt = "SELECT * FROM luns WHERE id_pool = ? AND id_strsys = ?";
            $qry = $this->pdo_intern->prepare($qry_txt);
            $qry->execute(array($pool['id_pool'],$strsys['id_strsys']));
            $luns = $qry->fetchAll(\PDO::FETCH_ASSOC);
            return $luns;
        }
    }
    public function load_luns_entries_by_date($pool,$strsys,$date)
    {
        if($this->pdo_status)
        {
            $qry_txt = "SELECT * FROM lun_status WHERE id_pool = ? AND id_strsys = ? AND `date` = ?";
            $qry = $this->pdo_intern->prepare($qry_txt);
            $qry->execute(array($pool['id_pool'],$strsys['id_strsys'],$date));
            $entries = $qry->fetchAll(\PDO::FETCH_ASSOC);
            return $entries;
        }
    }
}

?>
