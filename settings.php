<?php
session_start();
if(!isset($_SESSION['user_id']))
{
    session_destroy();
    header('Location: connect.php');
}
else
{
    /**
    * start program by including core.php and run include_core_files()
    */
    include "core.php";
    //including core files
    include_core_files();
    $db = new db_object();
    $current_page = "Settings";
    $userId = $_SESSION['user_id'];
    $userLevel = $_SESSION['level'];
    $userName = $_SESSION['name'];
    $userPwd = $_SESSION['passwd'];
    $users = $db->get_all_users();
    $types = $db->get_strsys_types();
    $reports = $db->get_all_reports();
    $strsys = $db->load_all_strsys();
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 5.0 Strict//EN">

<html lang="en">
    <?php main_head_gen($current_page); ?>
    <body class="bg-light">
        <!-- ******************** USER content **************************** -->
        <!-- ================== Information modal ========================= -->
        <div class="modal fade" id="informationModal" tabindex="-1" role="dialog" aria-labelledby="info-modal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Information</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                </div>
            </div>
        </div>
        <?php
        if($userLevel > 1)
        {
        ?>
        <!-- ================== Change USer level modal ========================= --->
        <div class="modal fade" tabindex="-1" role="dialog" id="confirm-update">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Change <span class="whichUser font-weight-bolder">User</span>'s level</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="updateForm">
                        <div class="form-group">
                            <select class="custom-select form-control" id="userLvlUp" name="userLvlUp" required>
                                <option value="0" selected>Normal User</option>
                                <option value="1">Administrator</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="confirm-update-button">Update</button>
                </div>
                </div>
            </div>
        </div>
        <!-- ================== Delete USer modal ========================= --->
        <div class="modal fade" tabindex="-1" role="dialog" id="confirm-delete">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Confirmation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure you you want to delete <span class="whichUser font-weight-bolder">User</span>? </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button type="button" class="btn btn-primary" id="confirm-delete-button">Yes</button>
                </div>
                </div>
            </div>
        </div>
        <?php
        }
        ?>
        <!-- ================= Add new system modal ======================= -->
        <?php
            if($userLevel > 0)
            {
                add_new_system_mod_gen($types);
            } 
         ?>
        <!-- ================= Help modal ================================= -->
        <?php
        help_mod_gen();
        ?>
        <!-- ================ navbar beginning ============================ -->
        <?php main_navbar_gen($userName); ?>
        <!-- ================ navbar ending ============================ -->
        <!-- ================ wrapper beginning ======================== -->
        <div class="wrapper">
            <!-- ================ sidebar beginning ======================== -->
            <?php side_navbar_gen($strsys); ?>
            <!-- ================ sidebar ending ======================== -->
            <!-- ================ content beginning ======================== -->
            <div id="content">
                <div class="d-sm-flex justify-content-between">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb no-bg">
                            <li class="breadcrumb-item " aria-current="page"><a href="index.php">Dashboard</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Settings</li>
                        </ol>
                    </nav>
                    <?php if($userLevel > 1) { ?>
                    <div class="p-2" style="position:relative;">
                        <a class="stretched-link nounderline" href="#" data-toggle="modal" data-target="#addNewSystemModal">
                            <span>Add System</span>
                            <button class="btn btn-primary rounded-circle btn-sm">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                        </a>
                    </div>
                    <?php } ?>
                </div>
                <!-- ============ System general display ================ -->
                <div class="container-fluid">
                    <?php
                    if($userLevel == 2)
                    {
                    ?>
                    <div class="row">
                        <div class="card-deck px-3 col-sm-12" style="margin-left:0 !important;">
                            <div class="card col-sm-12" id="users">
                                <div class="container-fluid">
                                    <div class="row">
                                        <h5 class="card-title col-sm-6 text-info py-3">Users Management</h5>
                                    </div>
                                </div>
                                <div class="card-body pt-1">
                                    <div class="container-fluid">
                                        <div class="row justify-content-between">
                                            <div class="col-sm-12 col-md-6 border-right">
                                            <?php
                                            users_list($users);
                                            ?>
                                                <br>
                                                <button type="button" id="delete-user" class="btn btn-sm btn-danger">Delete</button>
                                                <button type="button" id="reset-user" class="btn btn-sm btn-primary">Reset</button>
                                                <button type="button" id="update-user" class="btn btn-sm btn-primary">Update</button>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <form id="insertForm">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" id="newUserName" name="newUserName" required placeholder="User Name">
                                                    </div>
                                                    <div class="form-group">
                                                        <select class="custom-select form-control" id="userLvl" name="userLvl" required>
                                                            <option value="0" selected>Normal User</option>
                                                            <option value="1">Administrator</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>A new user will be created with default password as following: <span class="font-italic font-weight-bold">abc123</span></label>
                                                    </div> 
                                                    <div class="form-group">
                                                        <button id="buttonInsert" class="btn btn-primary ">Insert new user</button>
                                                    </div>
                                                </form>
                                                <!--- <form id="changePicture">

                                                </form> --->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <?php
                    }
                    ?>
                    
                    <div class="row">
                        <div class="card-deck px-3 col-sm-12" style="margin-left:0 !important;">
                            <div class="card col-md-12 col-lg-6" id="settings">
                                <div class="container-fluid">
                                    <div class="row">
                                        <h5 class="card-title col-sm-6 text-info py-3">Profile Settings</h5>
                                    </div>
                                </div>
                                <div class="card-body pt-1">
                                    <form id="updatePassword">
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="ancientPasswd" name="ancientPasswd" required placeholder="Ancient Password">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="newPasswd" name="newPasswd" required placeholder="New Password">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="newPasswdConfirm" name="newPasswdConfirm" required placeholder="Confirm New Password">
                                        </div>
                                        <div class="form-group">
                                            <button id="updatePasswd" class="btn btn-primary">Update Password</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <?php
                            if($userLevel > 0)
                            {
                            ?>
                            <!--
                            <div class="card col-md-12 col-lg-6" id="report">
                                <div class="container-fluid">
                                    <div class="row">
                                        <h5 class="card-title col-sm-6 text-info py-3">Report Settings</h5>
                                    </div>
                                </div>
                                <div class="card-body pt-1">
                                    <form id="report">
                                        <div class="form-group">
                                            <input type="text" maxlength="20" class="form-control" id="reportName" name="reportName" required placeholder="Report Name">
                                        </div>
                                        <div class="form-group">
                                        <?php
                                           //systems_checkbox($strsys); 
                                        ?>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="emails" name="emails" required placeholder="Notification emails (Separated by ,)">
                                        </div>
                                        <div class="form-group">
                                            <select class="custom-select form-control" id="frequency" name="frequency" required>
                                                <option value="0" selected>Every Month</option>
                                                <option value="1">Every Week</option>
                                                <option value="2">Every Day</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <button id="addReport" class="btn btn-primary">Add Report</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            -->
                            <?php
                            }    
                            ?>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
        <!-- ================ wrapper ending ======================== -->
        <!-- ================ footer beginning ============================ -->
        <?php
        main_footer_gen();
        ?>
        <!-- ================ footer ending ============================ -->
        <!-- ******************** JQUERY LIBRARY ************************** -->
        <?php
        js_libraries_gen();
        add_strsys_script();
        ?>
        <script>
        function display_info(msg)
        {
            $("#informationModal .modal-body").html(msg);
            $("#informationModal").modal("show");
        }
        $(document).ready(function() {
            $("#informationModal").on('hidden.bs.modal', function (e) {
                window.location.reload(true);
            });
            $(".whichUser").html($("#users-list a:first-child").text().replace("Normal","").replace("Admin",""));
            $("#users-list a").on('click',function(e) {
                e.preventDefault();
                $("#users-list a").removeClass("active");
                $("#users-list a .badge.badge-light").addClass("badge-primary");
                $("#users-list a .badge").removeClass("badge-light");
                $(this).addClass("active");
                $(this).children(".badge").addClass("badge-light");
                $(".whichUser").text($(this).text().replace("Normal","").replace("Admin",""));
            });
            $("#delete-user").on('click', function(e) {
                $("#confirm-delete").modal('show');
            });
            $("#update-user").on('click',function(e) {
                $("#confirm-update").modal('show');
            });
            $("#confirm-delete-button").on('click',function(e){
                $("#confirm-delete").modal('hide');
                var id_user = $("#users-list a.active").attr('id');
                id_user = id_user.replace("user-","");
                $.ajax({
                    url: "delete_user.php",
                    type:'post',
                    data: {
                        'userID':id_user
                    },
                    success:function(response)
                    {
                        if(response == "1")
                        {
                            $("#users-list a.active#user-"+id_user).remove();
                            $(".whichUser").html($("#users-list a:first-child").text().replace("Normal","").replace("Admin",""));
                            $("#users-list a:first-child").addClass("active");
                            $("#users-list a:first-child .badge").removeClass("badge-primary");
                            $("#users-list a:first-child .badge").addClass("badge-light");
                        }
                        else
                        {
                            display_info(response);
                        }
                    }
                });
            });
            $("#confirm-update-button").on('click',function(e) {
                $("#confirm-update").modal('hide');
                var id_user = $("#users-list a.active").attr('id');
                id_user = id_user.replace("user-","");
                var new_lvl = $("#confirm-update #userLvlUp").val();
                $.ajax({
                    url: "update_user.php",
                    type:'post',
                    data: {
                        'userID':id_user,
                        'newLvl':new_lvl,
                        'action':'update'
                    },
                    success:function(response)
                    {
                        display_info(response);
                    }
                });
            });
            $("#reset-user").on('click',function(e){
                var id_user = $("#users-list a.active").attr('id');
                id_user = id_user.replace("user-","");
                $.ajax({
                    url: "update_user.php",
                    type:'post',
                    data: {
                        'userID':id_user,
                        'action':'reset'
                    },
                    success:function(response)
                    {
                        display_info(response);
                    }
                });
            });
            $("#insertForm").submit(function(e) {
                e.preventDefault();
                var buttonInsert = $("#buttonInsert");
                var userName = $("#newUserName").val();
                var userLvl = $("#userLvl").val();
                if(userName != "")
                {
                    userName.replace(" ","");
                    buttonInsert.addClass('disabled');
                    buttonInsert.attr('aria-disabled','true');
                    $.ajax({
                        url: "insert_user.php",
                        type:'post',
                        data: {
                            'userName': userName,
                            'userLvl': userLvl
                        },
                        success:function(response)
                        {
                            display_info(response);
                        }
                    });
                }
            });
            $("#report").submit(function(e) {
                e.preventDefault();
                var buttonInsert = $("#addReport");
                var reportName = $("#reportName").val();
                var emailList = $("#emails").val();
                var frequency = $("#frequency").val();
                var entries = [];
                $("#report .form-check-input:checkbox").each(function(index){
                    if($(this).prop("checked") == true)
                        entries.push($(this).val());
                });
                if(reportName != "" && emailList != "" && entries.length > 0)
                {
                    reportName.replace(" ","");
                    buttonInsert.addClass('disabled');
                    buttonInsert.attr('aria-disabled','true');
                    $.ajax({
                        url: "insert_report.php",
                        type:'post',
                        data: {
                            'reportName': reportName,
                            'frequency': frequency,
                            'email_list': emailList.trim(),
                            'entries': entries,
                            'sorting': 2,
                            'number_elem': 10
                        },
                        success:function(response)
                        {
                            display_info(response);
                        }
                    });
                }
            });
            $("#updatePassword").submit(function(e) {
                e.preventDefault();
                var buttonUpdate = $("#buttonPasswd");
                var ancientPasswd = $("#ancientPasswd").val();
                var newPasswd = $("#newPasswd").val();
                var newPasswdConfirm = $("#newPasswdConfirm").val();
                if(ancientPasswd != "" && newPasswd != "" && newPasswdConfirm != "")
                {
                    if(newPasswd == newPasswdConfirm)
                    {
                        buttonUpdate.addClass('disabled');
                        buttonUpdate.attr('aria-disabled','true');
                        $.ajax({
                            url: "update_user.php",
                            type:'post',
                            data: {
                                'ancientPasswd': ancientPasswd,
                                'newPasswd' : newPasswd,
                                'newPasswdConfirm': newPasswdConfirm,
                                'action': 'passwd'
                            },
                            success:function(response)
                            {
                                display_info(response);
                            }
                        });
                    }
                    else
                    {
                        display_info("Please check your password.");
                    }
                }
            });
            $(".delete-rep").on('click',function() {
                var target_rep = $(this).attr("id");
                target_rep = target_rep.replace("rep","");
                $.ajax({
                    url: "delete_report.php",
                    type:'post',
                    data: {
                        'report_id': target_rep,
                    },
                    success:function(response)
                    {
                        display_info(response);
                    }
                });
            });
        });
        </script>
    </body>
</html>